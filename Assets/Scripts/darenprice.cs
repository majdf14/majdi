using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class darenprice : MonoBehaviour
{
    private string irmaball;
    public Texture[] BGImage;
    public GameObject gildakirkpatrick;

    private static darenprice _instance = null;
    public static darenprice Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    void Update()
    {
        irmaball = SceneManager.GetActiveScene().name;
        if (irmaball == "Game")
        {
            gildakirkpatrick.GetComponent<RawImage>().texture = BGImage[Data.LevelNow];
        }
    }
}
