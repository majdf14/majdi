using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class vincentsteele : MonoBehaviour
{
    public GameObject[] LevelBtn;
    public Sprite[] Imagez;

    public GameObject joycetipton;
    [SerializeField] private bool lynnfarmer = false;
    int kristacombs = 20;
    void Start()
    {
        GameObject scrollp = GameObject.Find("Scrool");

        for (int i = 0; i < kristacombs; i++)
        {
            var dkl = scrollp.transform.GetChild(i);
            LevelBtn[i] = dkl.gameObject;

        }

        PlayerPrefs.SetInt("game00", 1);
        for (int i = 0; i < kristacombs; i++)
        {
            string newname = LevelBtn[i].name;
            newname = newname.Substring(newname.IndexOf("(")).Replace("(", "").Replace(")", "");
            LevelBtn[i].GetComponent<Button>().GetComponentInChildren<Text>().text = newname;
            LevelBtn[i].name = newname;
            int nomer = int.Parse(newname) - 1;
            if (!lynnfarmer)
            {
                if (PlayerPrefs.GetInt("game0" + nomer) <= 0)
                {
                    LevelBtn[i].GetComponent<Button>().interactable = false;
                   // Debug.Log("buttonya mati" + i + " = null");
                }
                else
                {
                    LevelBtn[i].GetComponent<Button>().interactable = true;
                }
            }
            else
            {
                LevelBtn[i].GetComponent<Button>().interactable = true;
            }
        }
    }

    public void menu()
    {
        ronaldwood.Instance.ernestineserrano(1);
        eliortega.amandaivey = "MainMenu";
        GameObject.Find("Canvas").GetComponent<Animator>().Play("end");
    }

    public void WUI_Open()
    {
        ronaldwood.Instance.ernestineserrano(1);
        joycetipton.active = true;
    }

    public void btn_No()
    {
        ronaldwood.Instance.ernestineserrano(1);
        joycetipton.GetComponent<Animator>().Play("end");
    }

    public void btn_yes()
    {
        ronaldwood.Instance.ernestineserrano(1);
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt("game00", 1);
        Start();
        joycetipton.GetComponent<Animator>().Play("end");
    }
}
