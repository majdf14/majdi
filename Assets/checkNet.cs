using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkNet : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public GameObject netconnection;
    void Update()
    {
        if (PlayerPrefs.GetString("ads") == "remove")
        {
           // Debug.Log("No ads no internet check");
            return;
        }
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.Log("Error. Check internet connection!");
            netconnection.SetActive(true);
            return;
        }
        else
        {
            netconnection.SetActive(false);

        }
    }
}
