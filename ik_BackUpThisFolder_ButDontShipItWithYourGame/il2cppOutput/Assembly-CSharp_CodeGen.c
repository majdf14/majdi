﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AdViewScene::OnDestroy()
extern void AdViewScene_OnDestroy_mFDE4D1F8CB0ADB1CC6CB53A5DFCD825C8CFC0C93 (void);
// 0x00000002 System.Void AdViewScene::Awake()
extern void AdViewScene_Awake_mDDB0BDCEC4EE214804CAB81785B2F371E1AECF1E (void);
// 0x00000003 System.Void AdViewScene::SetLoadAddButtonText()
extern void AdViewScene_SetLoadAddButtonText_m10EBCAD8E76B326AA5CEE0B40641F4CF65C96D14 (void);
// 0x00000004 System.Void AdViewScene::LoadBanner()
extern void AdViewScene_LoadBanner_m7D94454CE74145472E2861E9D0A5320C441CDFA7 (void);
// 0x00000005 System.Void AdViewScene::ChangeBannerSize()
extern void AdViewScene_ChangeBannerSize_m2D5D1F243F1DEB1E5EA03F721E828B7FA28A9966 (void);
// 0x00000006 System.Void AdViewScene::NextScene()
extern void AdViewScene_NextScene_m761AF4305EEFEE3C7A87F26ADF31A772D9A24972 (void);
// 0x00000007 System.Void AdViewScene::ChangePosition()
extern void AdViewScene_ChangePosition_mD5EF1589A80F3FC7C68641A474F5FA54C214BADC (void);
// 0x00000008 System.Void AdViewScene::OnRectTransformDimensionsChange()
extern void AdViewScene_OnRectTransformDimensionsChange_m797CBD3BA29C7A92983F493B3F239DD5DE67CD86 (void);
// 0x00000009 System.Void AdViewScene::SetAdViewPosition(AudienceNetwork.AdPosition)
extern void AdViewScene_SetAdViewPosition_m29CF9A2B17ACE1461C7053D7C66A282CCBB9B86C (void);
// 0x0000000A System.Void AdViewScene::.ctor()
extern void AdViewScene__ctor_m14C350DCAFD8B4217322E71A1FE3B9B9F5DA3AC0 (void);
// 0x0000000B System.Void AdViewScene::<LoadBanner>b__10_0()
extern void AdViewScene_U3CLoadBannerU3Eb__10_0_mA83AD65C2C2144D86D8885E07036347B2E0C1F53 (void);
// 0x0000000C System.Void AdViewScene::<LoadBanner>b__10_1(System.String)
extern void AdViewScene_U3CLoadBannerU3Eb__10_1_mA4370BABEFDB30C9A2EA915EDFC3EEBE6902D425 (void);
// 0x0000000D System.Void AdViewScene::<LoadBanner>b__10_2()
extern void AdViewScene_U3CLoadBannerU3Eb__10_2_m7B1A498B76D0942B988C8BAC7AC3FA6DB8C9D64F (void);
// 0x0000000E System.Void AdViewScene::<LoadBanner>b__10_3()
extern void AdViewScene_U3CLoadBannerU3Eb__10_3_m55C31A233C5126E8F5C3073B5E1E3EB0DEEC8297 (void);
// 0x0000000F System.Void BaseScene::Update()
extern void BaseScene_Update_m1C3CCC3B5989D9087CE03C85F27CD68E710B60AA (void);
// 0x00000010 System.Void BaseScene::LoadSettingsScene()
extern void BaseScene_LoadSettingsScene_m2A1E3C6C7DB676236BB911123B3F67FEAAE8CFB5 (void);
// 0x00000011 System.Void BaseScene::.ctor()
extern void BaseScene__ctor_m0DA175FBEECCE8B0400ABC2B11946C430ABD33C8 (void);
// 0x00000012 System.Void InterstitialAdScene::Awake()
extern void InterstitialAdScene_Awake_m6850AD09E9EA945D26568E0CF2A698787C36DC6E (void);
// 0x00000013 System.Void InterstitialAdScene::LoadInterstitial()
extern void InterstitialAdScene_LoadInterstitial_mB899FE1C6F5FF014C68BFC6A8FB2D39C42838EA9 (void);
// 0x00000014 System.Void InterstitialAdScene::ShowInterstitial()
extern void InterstitialAdScene_ShowInterstitial_m909152BBA6FC6DE38AE6E7BC4EA32B8C054562F1 (void);
// 0x00000015 System.Void InterstitialAdScene::OnDestroy()
extern void InterstitialAdScene_OnDestroy_mEAF7361B8C02C19B7B0A27423D96E6759C3F280E (void);
// 0x00000016 System.Void InterstitialAdScene::NextScene()
extern void InterstitialAdScene_NextScene_m521266A0DF5FC2A2750A798C6879AAE64FF5D7D9 (void);
// 0x00000017 System.Void InterstitialAdScene::.ctor()
extern void InterstitialAdScene__ctor_m2ADB34A047E3CAEE213C1B98A323078971BA9B64 (void);
// 0x00000018 System.Void InterstitialAdScene::<LoadInterstitial>b__5_0()
extern void InterstitialAdScene_U3CLoadInterstitialU3Eb__5_0_mC11D6CAD99B51A5BBBF05C4088ADEB4EF1A86656 (void);
// 0x00000019 System.Void InterstitialAdScene::<LoadInterstitial>b__5_1(System.String)
extern void InterstitialAdScene_U3CLoadInterstitialU3Eb__5_1_m36C94989FAF7352364A7207325456FB47539137E (void);
// 0x0000001A System.Void InterstitialAdScene::<LoadInterstitial>b__5_4()
extern void InterstitialAdScene_U3CLoadInterstitialU3Eb__5_4_m134A19820032496839350C04344EBE9EC288F927 (void);
// 0x0000001B System.Void InterstitialAdScene::<LoadInterstitial>b__5_5()
extern void InterstitialAdScene_U3CLoadInterstitialU3Eb__5_5_mC4D72386836014E3063A67FB5DC1E9CC2B99DEAB (void);
// 0x0000001C System.Void InterstitialAdScene/<>c::.cctor()
extern void U3CU3Ec__cctor_m202176C53DA0551139774A1E7928FB3BCB9BCAC7 (void);
// 0x0000001D System.Void InterstitialAdScene/<>c::.ctor()
extern void U3CU3Ec__ctor_mBDFC2D25849802054C8D07A549F066BB067E3399 (void);
// 0x0000001E System.Void InterstitialAdScene/<>c::<LoadInterstitial>b__5_2()
extern void U3CU3Ec_U3CLoadInterstitialU3Eb__5_2_mABECD1E4EBA4B0BA97193A3A50FB985767A03F82 (void);
// 0x0000001F System.Void InterstitialAdScene/<>c::<LoadInterstitial>b__5_3()
extern void U3CU3Ec_U3CLoadInterstitialU3Eb__5_3_m5A158F650EC2717B7DDFDCB4552A66E62BFCD667 (void);
// 0x00000020 System.Void RewardedVideoAdScene::Awake()
extern void RewardedVideoAdScene_Awake_m21E541319C8B5A972111B5905E6A2922363BAF84 (void);
// 0x00000021 System.Void RewardedVideoAdScene::LoadRewardedVideo()
extern void RewardedVideoAdScene_LoadRewardedVideo_mC8DD3F08F1C3886ED769889158145BCA71DA0143 (void);
// 0x00000022 System.Void RewardedVideoAdScene::ShowRewardedVideo()
extern void RewardedVideoAdScene_ShowRewardedVideo_mF6FAFB4F03B477B1D98857037E96533860188926 (void);
// 0x00000023 System.Void RewardedVideoAdScene::OnDestroy()
extern void RewardedVideoAdScene_OnDestroy_m6BA0043E387F39B485D204C3FAD798D615A1AAA9 (void);
// 0x00000024 System.Void RewardedVideoAdScene::NextScene()
extern void RewardedVideoAdScene_NextScene_m1E9DC5EF6A25C93C60B08837785ADF2494C5A2EF (void);
// 0x00000025 System.Void RewardedVideoAdScene::.ctor()
extern void RewardedVideoAdScene__ctor_m866B77407BC24A40D4F03F8243ABBA41145166B5 (void);
// 0x00000026 System.Void RewardedVideoAdScene::<LoadRewardedVideo>b__5_0()
extern void RewardedVideoAdScene_U3CLoadRewardedVideoU3Eb__5_0_mBD89716729D68AE731B29BDC4980848D97CC959A (void);
// 0x00000027 System.Void RewardedVideoAdScene::<LoadRewardedVideo>b__5_1(System.String)
extern void RewardedVideoAdScene_U3CLoadRewardedVideoU3Eb__5_1_mBD72EA520B885A0784CB8774E0BA771F11EE5962 (void);
// 0x00000028 System.Void RewardedVideoAdScene::<LoadRewardedVideo>b__5_6()
extern void RewardedVideoAdScene_U3CLoadRewardedVideoU3Eb__5_6_mD48FA2B95BFB085FC1833F08DD49BDC2968986D2 (void);
// 0x00000029 System.Void RewardedVideoAdScene::<LoadRewardedVideo>b__5_7()
extern void RewardedVideoAdScene_U3CLoadRewardedVideoU3Eb__5_7_m448537BA94EA1EFD65B6BC5AB90826D12AB5A279 (void);
// 0x0000002A System.Void RewardedVideoAdScene/<>c::.cctor()
extern void U3CU3Ec__cctor_mF3A1661195C2C8FA97BB7B2E21DC71D17961B8AA (void);
// 0x0000002B System.Void RewardedVideoAdScene/<>c::.ctor()
extern void U3CU3Ec__ctor_mB6AB83FA5860B334F0EDF19AF9941AA5875B0ACB (void);
// 0x0000002C System.Void RewardedVideoAdScene/<>c::<LoadRewardedVideo>b__5_2()
extern void U3CU3Ec_U3CLoadRewardedVideoU3Eb__5_2_mB80AF20222FED26CA1E672DE5D7CE4C9CE130767 (void);
// 0x0000002D System.Void RewardedVideoAdScene/<>c::<LoadRewardedVideo>b__5_3()
extern void U3CU3Ec_U3CLoadRewardedVideoU3Eb__5_3_mFA35B8C3D1E4383262CA7F88F8A0F5615A701292 (void);
// 0x0000002E System.Void RewardedVideoAdScene/<>c::<LoadRewardedVideo>b__5_4()
extern void U3CU3Ec_U3CLoadRewardedVideoU3Eb__5_4_m149D60C443F55ABD4C875C64AF4548B665C4CD49 (void);
// 0x0000002F System.Void RewardedVideoAdScene/<>c::<LoadRewardedVideo>b__5_5()
extern void U3CU3Ec_U3CLoadRewardedVideoU3Eb__5_5_mEFA33F75A9C6684B114EC7AD23CB91C5C9320033 (void);
// 0x00000030 System.Void SettingsScene::InitializeSettings()
extern void SettingsScene_InitializeSettings_m23FB96519EA306C061CF13CE68D4480B63BAB4B3 (void);
// 0x00000031 System.Void SettingsScene::Start()
extern void SettingsScene_Start_m5FE2B554EC58551D11E2E2CA8828368B3162C80A (void);
// 0x00000032 System.Void SettingsScene::OnEditEnd(System.String)
extern void SettingsScene_OnEditEnd_m0E5A66610CA3427ABB5EB8D25B024B61F2474D94 (void);
// 0x00000033 System.Void SettingsScene::SaveSettings()
extern void SettingsScene_SaveSettings_mCCB083C36CD2FD0B8B51D964301D8D995FD355F3 (void);
// 0x00000034 System.Void SettingsScene::AdViewScene()
extern void SettingsScene_AdViewScene_mBEB858300F74422353E2C16A3E3AAD2076F283B8 (void);
// 0x00000035 System.Void SettingsScene::InterstitialAdScene()
extern void SettingsScene_InterstitialAdScene_m24DE1E5107978C28D2ABCC173AA87BAF5719CA35 (void);
// 0x00000036 System.Void SettingsScene::RewardedVideoAdScene()
extern void SettingsScene_RewardedVideoAdScene_mF08541790DEC73CE0289BC883B79C2D571077F41 (void);
// 0x00000037 System.Void SettingsScene::.ctor()
extern void SettingsScene__ctor_mA86D6D212F86C534FB12555AA7AB3794C976902A (void);
// 0x00000038 System.Void SettingsScene::.cctor()
extern void SettingsScene__cctor_m4ACEA42A7778BB09CE3F9743214A670BE683E184 (void);
// 0x00000039 System.Void checkNet::Update()
extern void checkNet_Update_mD69F5038CCF13227F6CAC9DDA036EBC8617E4002 (void);
// 0x0000003A System.Void checkNet::.ctor()
extern void checkNet__ctor_mBEAC12478D70251D28CB97AF66B00676EC7C4A6C (void);
// 0x0000003B System.Void Implementation::Awake()
extern void Implementation_Awake_mE7092E8EC7973F025E4345AE29E04E11A6791E80 (void);
// 0x0000003C System.Void Implementation::Start()
extern void Implementation_Start_m872A0AABC891DB188C41ACB3588D593743943833 (void);
// 0x0000003D System.Void Implementation::ShawBanner()
extern void Implementation_ShawBanner_mA5FF1B6846CBE987D380374222790DDC3D1F9B43 (void);
// 0x0000003E System.Void Implementation::HideBanner()
extern void Implementation_HideBanner_mECE627CA15F4CD25E0F63887B55AE3CCE0F4C69D (void);
// 0x0000003F System.Void Implementation::ShowInterstitial()
extern void Implementation_ShowInterstitial_m5C3A8D90C9B75A04F6B9338AAEF7055A17CABFF3 (void);
// 0x00000040 System.Void Implementation::ShowRewardedVideo()
extern void Implementation_ShowRewardedVideo_m0E19095814347ED91ADA369B2F9EE8DE745A74D1 (void);
// 0x00000041 System.Void Implementation::Update()
extern void Implementation_Update_m61C084FC0932343F5C4A40813D13827423FDD52B (void);
// 0x00000042 System.Void Implementation::CompleteMethod(System.Boolean)
extern void Implementation_CompleteMethod_mF633B329527BE164584B4AC33F61757346CE2832 (void);
// 0x00000043 System.Void Implementation::.ctor()
extern void Implementation__ctor_m9DEA8F8E497AD0F1D71927071A55F44AADDA5D08 (void);
// 0x00000044 System.Void TestAds::Start()
extern void TestAds_Start_mBFC5EFB231F100D4150439A360B738A2071F38EB (void);
// 0x00000045 System.Void TestAds::OnGUI()
extern void TestAds_OnGUI_m8F75FFEE6D481E5D3E474E8CAA1637F42FE9EFA2 (void);
// 0x00000046 System.Void TestAds::InterstitialClosed(System.String)
extern void TestAds_InterstitialClosed_m02D36B5311B6410E242EEFADBE3DE50160D5192B (void);
// 0x00000047 System.Void TestAds::CompleteMethod(System.Boolean,System.String)
extern void TestAds_CompleteMethod_m627910A8EE161F3AFEDDC4322C76E7F1DFBF114F (void);
// 0x00000048 System.Void TestAds::.ctor()
extern void TestAds__ctor_m320DAE171DF5ACB5C267183C19864BDF1AFDD2F0 (void);
// 0x00000049 System.Void Advertiser::.ctor(GleyMobileAds.ICustomAds,GleyMobileAds.MediationSettings,System.Collections.Generic.List`1<GleyMobileAds.PlatformSettings>)
extern void Advertiser__ctor_mCB085210C61BEB5C605B44D76A52C9E325F15FD6 (void);
// 0x0000004A Advertisements Advertisements::get_Instance()
extern void Advertisements_get_Instance_m0593407095ED68645A100F756BDCBE8362BFF975 (void);
// 0x0000004B System.Void Advertisements::SetUserConsent(System.Boolean)
extern void Advertisements_SetUserConsent_m04862C0D945D3FFBDC589A32C0FD6E4E1974276D (void);
// 0x0000004C System.Void Advertisements::SetCCPAConsent(System.Boolean)
extern void Advertisements_SetCCPAConsent_m8EB5BC876CA39339B3A7413613C222BC54429E4E (void);
// 0x0000004D UserConsent Advertisements::GetConsent(System.String)
extern void Advertisements_GetConsent_m9729254344D396A779788AB128CBCE09B3DACC34 (void);
// 0x0000004E System.Boolean Advertisements::ConsentWasSet(System.String)
extern void Advertisements_ConsentWasSet_m2AB9EA7FA4C406BD38631E35D00F3CDBAB92BD2A (void);
// 0x0000004F UserConsent Advertisements::GetUserConsent()
extern void Advertisements_GetUserConsent_m1A409021B05385D24A4E5169ED24906287EE30CD (void);
// 0x00000050 UserConsent Advertisements::GetCCPAConsent()
extern void Advertisements_GetCCPAConsent_m80C0C3F355C31726641AFFCA6E8B9BCEC81A70FE (void);
// 0x00000051 System.Boolean Advertisements::UserConsentWasSet()
extern void Advertisements_UserConsentWasSet_m3164AD181A066E604B2B15848541E97E4752F58C (void);
// 0x00000052 System.Boolean Advertisements::CCPAConsentWasSet()
extern void Advertisements_CCPAConsentWasSet_mF1B145AEB60B9B47F45F11382F7F24BFDAC99CC8 (void);
// 0x00000053 System.Void Advertisements::RemoveAds(System.Boolean)
extern void Advertisements_RemoveAds_mA11908EA558AAB8B158CE87650523034B261C0A2 (void);
// 0x00000054 System.Boolean Advertisements::CanShowAds()
extern void Advertisements_CanShowAds_m04BE533F9F4541D18D8DA1BF67CBEA68662AE3AD (void);
// 0x00000055 System.Void Advertisements::Initialize()
extern void Advertisements_Initialize_m2C47C13B5808B687C06EA56F3EF9B389058FB25D (void);
// 0x00000056 System.Collections.IEnumerator Advertisements::WaitForConsent(UnityEngine.Events.UnityAction)
extern void Advertisements_WaitForConsent_mFF2DFDF08DE97E3A70F448A84EDEEBAAEF1D5482 (void);
// 0x00000057 System.Void Advertisements::ContinueInitialization()
extern void Advertisements_ContinueInitialization_m0CEA001408002747E388CB9097C90B57E91BEFFD (void);
// 0x00000058 System.Void Advertisements::UpdateUserConsent()
extern void Advertisements_UpdateUserConsent_mC25FA5D05C21380B62238E4E63AFDC447CEA2D4E (void);
// 0x00000059 System.Void Advertisements::ShowInterstitial(UnityEngine.Events.UnityAction)
extern void Advertisements_ShowInterstitial_m8C1F65815E3C3CBDC70C66B4A8B0F7144025C7F7 (void);
// 0x0000005A System.Void Advertisements::ShowInterstitial(UnityEngine.Events.UnityAction`1<System.String>)
extern void Advertisements_ShowInterstitial_m0418A9FF2661E94BF2362CC8312A830304638320 (void);
// 0x0000005B System.Void Advertisements::ShowInterstitial(GleyMobileAds.SupportedAdvertisers,UnityEngine.Events.UnityAction)
extern void Advertisements_ShowInterstitial_m7D11FB14B15B1090B2C8BAF7C60EDCFA3364C195 (void);
// 0x0000005C GleyMobileAds.ICustomAds Advertisements::GetInterstitialAdvertiser()
extern void Advertisements_GetInterstitialAdvertiser_m86D3F0FE83387F1122219EFE5D6C9B9197E9216B (void);
// 0x0000005D System.Void Advertisements::ShowRewardedVideo(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void Advertisements_ShowRewardedVideo_mFC1BA0197C4B026B04842C6AE23D5087BF0D0FC3 (void);
// 0x0000005E System.Void Advertisements::ShowRewardedVideo(UnityEngine.Events.UnityAction`2<System.Boolean,System.String>)
extern void Advertisements_ShowRewardedVideo_mD70F23B82E40CA215812646937EB016A0498D22C (void);
// 0x0000005F System.Void Advertisements::ShowRewardedVideo(GleyMobileAds.SupportedAdvertisers,UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void Advertisements_ShowRewardedVideo_mA726AD68698919D965D9B6C596EBDB271BE59FDB (void);
// 0x00000060 System.Void Advertisements::ShowBanner(BannerPosition,BannerType)
extern void Advertisements_ShowBanner_m830434ABEF4F35E97B2BF37E01A17C8B1F374BC0 (void);
// 0x00000061 System.Void Advertisements::ShowBanner(GleyMobileAds.SupportedAdvertisers,BannerPosition,BannerType)
extern void Advertisements_ShowBanner_m02D3E282D5E8784BDD720CBB17B5408FA74F8209 (void);
// 0x00000062 System.Void Advertisements::LoadBanner(BannerPosition,BannerType,System.Boolean,GleyMobileAds.SupportedAdvertisers)
extern void Advertisements_LoadBanner_m4671085C5860F9E361D6D43036BF23D031182731 (void);
// 0x00000063 System.Void Advertisements::BannerDisplayedResult(System.Boolean,BannerPosition,BannerType)
extern void Advertisements_BannerDisplayedResult_mBC146C2CD24754C50C7F272E7582D1D7914E4CE4 (void);
// 0x00000064 System.Void Advertisements::HideBanner()
extern void Advertisements_HideBanner_m7357FDDAC20A4D2D004DDA7C15E214CAE3595567 (void);
// 0x00000065 GleyMobileAds.ICustomAds Advertisements::UsePercent(System.Collections.Generic.List`1<Advertiser>,SupportedAdTypes)
extern void Advertisements_UsePercent_m3E3F9DA3D62D351CCFE7C5196DF0E95EC2010663 (void);
// 0x00000066 GleyMobileAds.ICustomAds Advertisements::UseOrder(System.Collections.Generic.List`1<Advertiser>,SupportedAdTypes)
extern void Advertisements_UseOrder_mD28D4E9C7A0E2358F4C69504FE9F1F67683F9BEB (void);
// 0x00000067 System.Void Advertisements::LoadFile()
extern void Advertisements_LoadFile_mFA58B4C896E325346DBFB9EB61F08C2A9C09CF99 (void);
// 0x00000068 System.Collections.IEnumerator Advertisements::LoadFile(System.String)
extern void Advertisements_LoadFile_mA9B7B1205BDDDE223DA9F8F056AAC4349FF39736 (void);
// 0x00000069 System.Void Advertisements::UpdateSettings(GleyMobileAds.AdOrder)
extern void Advertisements_UpdateSettings_m0409C3AD7D2458361FB656C4749BFDF2DF70C0B8 (void);
// 0x0000006A System.Void Advertisements::ApplySettings()
extern void Advertisements_ApplySettings_mDC7E3C27FB48A8F29029D97C6031BFF908CD1C5A (void);
// 0x0000006B System.Boolean Advertisements::IsRewardVideoAvailable()
extern void Advertisements_IsRewardVideoAvailable_mBADE08BD1AC5EEDF6766D507553B4F2FA6E919DD (void);
// 0x0000006C System.Boolean Advertisements::IsInterstitialAvailable()
extern void Advertisements_IsInterstitialAvailable_m524FF7F2F7F00BB7D4A50BDC4B990538AAA87A54 (void);
// 0x0000006D System.Boolean Advertisements::IsBannerAvailable()
extern void Advertisements_IsBannerAvailable_m8091A9EE50201CB5B0C20252D50559E41E5E6AA5 (void);
// 0x0000006E System.Boolean Advertisements::IsBannerOnScreen()
extern void Advertisements_IsBannerOnScreen_m29F7588BC172A6C1720C707B3BEC27DFE1E742F2 (void);
// 0x0000006F System.Void Advertisements::DisplayAdvertisers(System.Collections.Generic.List`1<Advertiser>)
extern void Advertisements_DisplayAdvertisers_m123FAD8F505756B700A0BB2609F2F2318C37B0FE (void);
// 0x00000070 System.Collections.Generic.List`1<Advertiser> Advertisements::GetAllAdvertisers()
extern void Advertisements_GetAllAdvertisers_m298B08D6502964358FDB677CFC43BD1CDB2844DE (void);
// 0x00000071 System.Collections.Generic.List`1<Advertiser> Advertisements::GetBannerAdvertisers()
extern void Advertisements_GetBannerAdvertisers_m53E7BF45B1D4C82835DD63924441E0F6FC569E65 (void);
// 0x00000072 System.Collections.Generic.List`1<Advertiser> Advertisements::GetInterstitialAdvertisers()
extern void Advertisements_GetInterstitialAdvertisers_m643B4FE8382767A54A6FC6C0BB333D09CCC5BB8D (void);
// 0x00000073 System.Collections.Generic.List`1<Advertiser> Advertisements::GetRewardedAdvertisers()
extern void Advertisements_GetRewardedAdvertisers_m25AE7047899E8F6FB55030BE6AEDBE8BBE2D38CB (void);
// 0x00000074 System.Void Advertisements::.ctor()
extern void Advertisements__ctor_m196FAB4C079F6174FF0FCAAA517DFD0B52535E9C (void);
// 0x00000075 System.Void Advertisements/<WaitForConsent>d__30::.ctor(System.Int32)
extern void U3CWaitForConsentU3Ed__30__ctor_mEF3F558CC844D495511A3EE729299BAA7EA33661 (void);
// 0x00000076 System.Void Advertisements/<WaitForConsent>d__30::System.IDisposable.Dispose()
extern void U3CWaitForConsentU3Ed__30_System_IDisposable_Dispose_m1507FFF762E488F54036D4107F166B844750FCC6 (void);
// 0x00000077 System.Boolean Advertisements/<WaitForConsent>d__30::MoveNext()
extern void U3CWaitForConsentU3Ed__30_MoveNext_m15A78578B7532EB5B8B88EFAAA88C83ED2AD4194 (void);
// 0x00000078 System.Object Advertisements/<WaitForConsent>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForConsentU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m461801BB3E622DFFA270CC42435ACCF242FCD8B5 (void);
// 0x00000079 System.Void Advertisements/<WaitForConsent>d__30::System.Collections.IEnumerator.Reset()
extern void U3CWaitForConsentU3Ed__30_System_Collections_IEnumerator_Reset_m23CF3803C5066B335D1E8A7F4354D0D4B83C6896 (void);
// 0x0000007A System.Object Advertisements/<WaitForConsent>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForConsentU3Ed__30_System_Collections_IEnumerator_get_Current_m147B4ED3C07166B678D1EE61F40677DC60DFF73B (void);
// 0x0000007B System.Void Advertisements/<>c::.cctor()
extern void U3CU3Ec__cctor_mD34C1990F3788A3D0EC714DA46314F714A729016 (void);
// 0x0000007C System.Void Advertisements/<>c::.ctor()
extern void U3CU3Ec__ctor_m031DEFFEF2B73091CB187A978DDC232AE37EC98E (void);
// 0x0000007D System.Boolean Advertisements/<>c::<ContinueInitialization>b__31_0(GleyMobileAds.AdvertiserSettings)
extern void U3CU3Ec_U3CContinueInitializationU3Eb__31_0_mCB89CAEC2A808C5232A26CF7EC17AF0A541FEA8A (void);
// 0x0000007E System.Boolean Advertisements/<>c::<ContinueInitialization>b__31_1(GleyMobileAds.AdvertiserSettings)
extern void U3CU3Ec_U3CContinueInitializationU3Eb__31_1_m9477EE09452D2ECB262A580962A86DE1F2E1C3B5 (void);
// 0x0000007F System.Boolean Advertisements/<>c::<ContinueInitialization>b__31_2(GleyMobileAds.AdvertiserSettings)
extern void U3CU3Ec_U3CContinueInitializationU3Eb__31_2_mA4E69678C0B8C1226FC05F85D873082F8D3CAF01 (void);
// 0x00000080 System.Boolean Advertisements/<>c::<ContinueInitialization>b__31_3(GleyMobileAds.AdvertiserSettings)
extern void U3CU3Ec_U3CContinueInitializationU3Eb__31_3_mFA2DC3D2C04CBDBF16DBCB84ECEFEB6B2F2EEEE5 (void);
// 0x00000081 System.Boolean Advertisements/<>c::<ContinueInitialization>b__31_4(GleyMobileAds.AdvertiserSettings)
extern void U3CU3Ec_U3CContinueInitializationU3Eb__31_4_m444DAEE6A32D52D7CB7D610EB209A1B900772443 (void);
// 0x00000082 System.Boolean Advertisements/<>c::<ContinueInitialization>b__31_5(GleyMobileAds.AdvertiserSettings)
extern void U3CU3Ec_U3CContinueInitializationU3Eb__31_5_mC34D7D0FBF69A124EC430BF245DDF69860BE659C (void);
// 0x00000083 System.Boolean Advertisements/<>c::<ContinueInitialization>b__31_6(GleyMobileAds.AdvertiserSettings)
extern void U3CU3Ec_U3CContinueInitializationU3Eb__31_6_m292AED839A8536F19D7BEFFF1DB95FB219E37872 (void);
// 0x00000084 System.Boolean Advertisements/<>c::<ContinueInitialization>b__31_7(GleyMobileAds.AdvertiserSettings)
extern void U3CU3Ec_U3CContinueInitializationU3Eb__31_7_m0AB7482887AC5B8C7FFDF6E295C64FCB5EDB2105 (void);
// 0x00000085 System.Boolean Advertisements/<>c::<ContinueInitialization>b__31_8(GleyMobileAds.AdvertiserSettings)
extern void U3CU3Ec_U3CContinueInitializationU3Eb__31_8_mB7FA94A31346B422EE17CD0E33117054D271B437 (void);
// 0x00000086 System.Boolean Advertisements/<>c::<ContinueInitialization>b__31_9(GleyMobileAds.AdvertiserSettings)
extern void U3CU3Ec_U3CContinueInitializationU3Eb__31_9_mE0582532A64498CBC93AC5BD74286C433A8C84D6 (void);
// 0x00000087 System.Int32 Advertisements/<>c::<ApplySettings>b__50_0(Advertiser)
extern void U3CU3Ec_U3CApplySettingsU3Eb__50_0_mAB00A38D6691A5020FDAE55671901C8E7BFB5F55 (void);
// 0x00000088 System.Int32 Advertisements/<>c::<ApplySettings>b__50_1(Advertiser)
extern void U3CU3Ec_U3CApplySettingsU3Eb__50_1_m6977D10B4F7EF2CB997836E5CF1A274A31465FF1 (void);
// 0x00000089 System.Int32 Advertisements/<>c::<ApplySettings>b__50_2(Advertiser)
extern void U3CU3Ec_U3CApplySettingsU3Eb__50_2_m8C1A288F56B381B1A76BA9A5A7DEE91783B1FD07 (void);
// 0x0000008A System.Int32 Advertisements/<>c::<ApplySettings>b__50_3(Advertiser)
extern void U3CU3Ec_U3CApplySettingsU3Eb__50_3_m48C572C6DBF8A65F5146E9FB1C7C349D9281A696 (void);
// 0x0000008B System.Int32 Advertisements/<>c::<ApplySettings>b__50_4(Advertiser)
extern void U3CU3Ec_U3CApplySettingsU3Eb__50_4_mE046FA8D25580F975F8B8B93A50F9B3E5FEBCBEF (void);
// 0x0000008C System.Int32 Advertisements/<>c::<ApplySettings>b__50_5(Advertiser)
extern void U3CU3Ec_U3CApplySettingsU3Eb__50_5_m57BF3B032049434278BE031727C4DC0555F8A24F (void);
// 0x0000008D System.Void Advertisements/<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m5C77E13AAE022FDB6975B468A694DFF0D5801CFF (void);
// 0x0000008E System.Boolean Advertisements/<>c__DisplayClass35_0::<ShowInterstitial>b__0(Advertiser)
extern void U3CU3Ec__DisplayClass35_0_U3CShowInterstitialU3Eb__0_m139D1121622D19575AF806B2DE399BC09DDD7DB8 (void);
// 0x0000008F System.Void Advertisements/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m71B1282F80EAB07BF4F93CD386DF2974426CC9A5 (void);
// 0x00000090 System.Boolean Advertisements/<>c__DisplayClass39_0::<ShowRewardedVideo>b__0(Advertiser)
extern void U3CU3Ec__DisplayClass39_0_U3CShowRewardedVideoU3Eb__0_m8C9E1FE69A461431CA0DCE7DA0F3E2E67015D6AD (void);
// 0x00000091 System.Void Advertisements/<>c__DisplayClass42_0::.ctor()
extern void U3CU3Ec__DisplayClass42_0__ctor_m9EB0EB779CDAA398FEA78300E97257108BD55FAD (void);
// 0x00000092 System.Boolean Advertisements/<>c__DisplayClass42_0::<LoadBanner>b__0(Advertiser)
extern void U3CU3Ec__DisplayClass42_0_U3CLoadBannerU3Eb__0_m70A8730CBA7F3E6A25442E9535FD9202ECA45800 (void);
// 0x00000093 System.Void Advertisements/<LoadFile>d__48::.ctor(System.Int32)
extern void U3CLoadFileU3Ed__48__ctor_m9A70F7BB3CDF96EEA4CE0622FBD014203735A684 (void);
// 0x00000094 System.Void Advertisements/<LoadFile>d__48::System.IDisposable.Dispose()
extern void U3CLoadFileU3Ed__48_System_IDisposable_Dispose_m0A6B5CBCEADF845B7B982EE5015B047CE05DA483 (void);
// 0x00000095 System.Boolean Advertisements/<LoadFile>d__48::MoveNext()
extern void U3CLoadFileU3Ed__48_MoveNext_m67F0D5B49BAD292E27179406E05B02714FC5F94C (void);
// 0x00000096 System.Object Advertisements/<LoadFile>d__48::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadFileU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC95F5BF2DB84451A1D5FE72C65C5D526ECCD3771 (void);
// 0x00000097 System.Void Advertisements/<LoadFile>d__48::System.Collections.IEnumerator.Reset()
extern void U3CLoadFileU3Ed__48_System_Collections_IEnumerator_Reset_m3F921F33B9753B579FD9B51D27717DF200AD0F66 (void);
// 0x00000098 System.Object Advertisements/<LoadFile>d__48::System.Collections.IEnumerator.get_Current()
extern void U3CLoadFileU3Ed__48_System_Collections_IEnumerator_get_Current_m83EA6195A84C1BBF72F40406CCDC5509C43E0374 (void);
// 0x00000099 System.Void InitializePlaymakerAds::.ctor()
extern void InitializePlaymakerAds__ctor_mD653B3674DD735ED852B909508C9AC9EE9C93E19 (void);
// 0x0000009A System.Void removeads::Start()
extern void removeads_Start_m9DBC53EB280B692D317CC158B6C7DBC79FFDA535 (void);
// 0x0000009B System.Void removeads::openme()
extern void removeads_openme_mDB2BEC05E9F009A65850BADDD2FB1C46FC47B5D6 (void);
// 0x0000009C System.Void removeads::closeme()
extern void removeads_closeme_m52DABD0C747CC2C8B87EEC7A7948F9265825B865 (void);
// 0x0000009D System.Void removeads::removeadsfn()
extern void removeads_removeadsfn_m58E85A2F4746574FEF4EA0E1C1927A280CA0046A (void);
// 0x0000009E System.Void removeads::.ctor()
extern void removeads__ctor_mE08F00E8D152188F9EB9D211B75907F22E9EA4B3 (void);
// 0x0000009F System.Void ellisrusso::Start()
extern void ellisrusso_Start_mAA19486AF210433EDEAF04DCC2EF23BACCC363A8 (void);
// 0x000000A0 System.Void ellisrusso::Update()
extern void ellisrusso_Update_mD5D3CB8AF0F7AF2B5E12AF46E0914B697DE4A94A (void);
// 0x000000A1 System.Void ellisrusso::chelseysamuel()
extern void ellisrusso_chelseysamuel_m167984E96189DC33540E09D482C76F9042079BD0 (void);
// 0x000000A2 System.Void ellisrusso::laurelhester()
extern void ellisrusso_laurelhester_mB953CB0758D1840D4E94076619E9960FF4A84F79 (void);
// 0x000000A3 System.Collections.IEnumerator ellisrusso::katinahiggins(System.String,UnityEngine.UI.Image)
extern void ellisrusso_katinahiggins_mB70F985B9890427FFF4DE57531677868DCA04809 (void);
// 0x000000A4 System.Void ellisrusso::.ctor()
extern void ellisrusso__ctor_mC2504CFCAF8B3965DAC3616CB51C7B832625E9DB (void);
// 0x000000A5 System.Void ellisrusso::<chelseysamuel>b__9_0(System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>)
extern void ellisrusso_U3CchelseysamuelU3Eb__9_0_mEB99490EB2C595B764E23E699B10FF57D74164B6 (void);
// 0x000000A6 System.Void ellisrusso/<katinahiggins>d__12::.ctor(System.Int32)
extern void U3CkatinahigginsU3Ed__12__ctor_m221F3CC206C15B2BC10D8A08B0895FB66B77B795 (void);
// 0x000000A7 System.Void ellisrusso/<katinahiggins>d__12::System.IDisposable.Dispose()
extern void U3CkatinahigginsU3Ed__12_System_IDisposable_Dispose_mFA634121E45EF868EBC2199C52FA92A58B65004C (void);
// 0x000000A8 System.Boolean ellisrusso/<katinahiggins>d__12::MoveNext()
extern void U3CkatinahigginsU3Ed__12_MoveNext_mC051514349F400B8307966B9130DCE2AC514957F (void);
// 0x000000A9 System.Void ellisrusso/<katinahiggins>d__12::<>m__Finally1()
extern void U3CkatinahigginsU3Ed__12_U3CU3Em__Finally1_m9A6F28CA1D430D0CA31EE9DA588B7DDE99F623E4 (void);
// 0x000000AA System.Object ellisrusso/<katinahiggins>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CkatinahigginsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8C92D0F61EED7AB9F884626C64167B813043FDC (void);
// 0x000000AB System.Void ellisrusso/<katinahiggins>d__12::System.Collections.IEnumerator.Reset()
extern void U3CkatinahigginsU3Ed__12_System_Collections_IEnumerator_Reset_mD4E6345F3CFEDE8C4296771D67A3ACAA38113659 (void);
// 0x000000AC System.Object ellisrusso/<katinahiggins>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CkatinahigginsU3Ed__12_System_Collections_IEnumerator_get_Current_mABD1E6DADEB8AEF39AD1F62010EA24785906D2DB (void);
// 0x000000AD System.Void juniorrhoades::Start()
extern void juniorrhoades_Start_mDA62011582D8C596421A780FCAC3FE7451D32E87 (void);
// 0x000000AE System.Void juniorrhoades::OnTokenReceived(System.Object,Firebase.Messaging.TokenReceivedEventArgs)
extern void juniorrhoades_OnTokenReceived_m45F91ADF67328210C364B3B6A977C171539F61B9 (void);
// 0x000000AF System.Void juniorrhoades::OnMessageReceived(System.Object,Firebase.Messaging.MessageReceivedEventArgs)
extern void juniorrhoades_OnMessageReceived_m5F9ED1B78E4FAE77618737957DD3ACC18E9A2B15 (void);
// 0x000000B0 System.Void juniorrhoades::.ctor()
extern void juniorrhoades__ctor_m74967CBF172466397AA576564705DDAA35B1BAFD (void);
// 0x000000B1 landonfoley landonfoley::get_Instance()
extern void landonfoley_get_Instance_m65988A568853FDEEFDEDC640ED031936E2E453D7 (void);
// 0x000000B2 System.Collections.IEnumerator landonfoley::LoadScene()
extern void landonfoley_LoadScene_m5EA718931FBA4CAFD6D4F91D97C18FAE7CCF89E4 (void);
// 0x000000B3 System.Void landonfoley::CheckAds()
extern void landonfoley_CheckAds_m712F2A46894583FAB9923549A28DDCC0F0923E14 (void);
// 0x000000B4 System.Void landonfoley::Awake()
extern void landonfoley_Awake_m04CD543EE8881BB369DE234C6796C9680F434BD9 (void);
// 0x000000B5 System.Void landonfoley::Update()
extern void landonfoley_Update_m24736DB383A081F5CF56575FC319DE9A591A37AA (void);
// 0x000000B6 System.Void landonfoley::ShowInterstitial()
extern void landonfoley_ShowInterstitial_m145A1C65384417A37C670B0A19282BDE1678DA9D (void);
// 0x000000B7 System.Collections.IEnumerator landonfoley::chasityhatch()
extern void landonfoley_chasityhatch_mDD85148460A80015280E19D40FF058A5E1D8CEEF (void);
// 0x000000B8 System.Void landonfoley::ShowAdMob()
extern void landonfoley_ShowAdMob_m30C8D4BD783DF8591C09AE3F11DA2F3408FB7805 (void);
// 0x000000B9 System.Void landonfoley::.ctor()
extern void landonfoley__ctor_m602F2FAFE1E64234B5806F62D1C66C00F4F82DDC (void);
// 0x000000BA System.Void landonfoley::.cctor()
extern void landonfoley__cctor_mAA998A45FCCEF1CF7A103FF63D11D1E949927077 (void);
// 0x000000BB System.Void landonfoley::<CheckAds>b__30_0(System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>)
extern void landonfoley_U3CCheckAdsU3Eb__30_0_m4B77574100C0037A433FFF0EF383F919CFEBD0D2 (void);
// 0x000000BC System.Void landonfoley/<LoadScene>d__28::.ctor(System.Int32)
extern void U3CLoadSceneU3Ed__28__ctor_m7162211DB904170AF0F6A5E74D4B80CFB6B78BE3 (void);
// 0x000000BD System.Void landonfoley/<LoadScene>d__28::System.IDisposable.Dispose()
extern void U3CLoadSceneU3Ed__28_System_IDisposable_Dispose_m5367329A8D97B43B736AC695FC321F6D14D85859 (void);
// 0x000000BE System.Boolean landonfoley/<LoadScene>d__28::MoveNext()
extern void U3CLoadSceneU3Ed__28_MoveNext_m32319F200F9E9B5B67A156539F2513C0E22AB888 (void);
// 0x000000BF System.Object landonfoley/<LoadScene>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadSceneU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m054113F2BC0364B3927CA7FAB50430951DCF4867 (void);
// 0x000000C0 System.Void landonfoley/<LoadScene>d__28::System.Collections.IEnumerator.Reset()
extern void U3CLoadSceneU3Ed__28_System_Collections_IEnumerator_Reset_m3F34F1B0A8E155FAB4EECAAD59B1746A4D2487F9 (void);
// 0x000000C1 System.Object landonfoley/<LoadScene>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CLoadSceneU3Ed__28_System_Collections_IEnumerator_get_Current_m5FBDB7EE8B7B753ACA1574DEE492CC05F5ED11A4 (void);
// 0x000000C2 System.Void landonfoley/<chasityhatch>d__36::.ctor(System.Int32)
extern void U3CchasityhatchU3Ed__36__ctor_m6A4A1DCED6165560060269BC8531562A78267FAA (void);
// 0x000000C3 System.Void landonfoley/<chasityhatch>d__36::System.IDisposable.Dispose()
extern void U3CchasityhatchU3Ed__36_System_IDisposable_Dispose_m61197209C1E485F52B43B86BC84DB4D7A6424C44 (void);
// 0x000000C4 System.Boolean landonfoley/<chasityhatch>d__36::MoveNext()
extern void U3CchasityhatchU3Ed__36_MoveNext_m031F182BA9A449ED40C56C22D8EE2705226BC110 (void);
// 0x000000C5 System.Object landonfoley/<chasityhatch>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CchasityhatchU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE67C8305270CDFF3269681DBA5E1876625307B6 (void);
// 0x000000C6 System.Void landonfoley/<chasityhatch>d__36::System.Collections.IEnumerator.Reset()
extern void U3CchasityhatchU3Ed__36_System_Collections_IEnumerator_Reset_mB7DF68B57F7FFEF4AB96CC71CE421CF4C01136EA (void);
// 0x000000C7 System.Object landonfoley/<chasityhatch>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CchasityhatchU3Ed__36_System_Collections_IEnumerator_get_Current_m3C0CCB71185D504B1A3413E702557B760E139842 (void);
// 0x000000C8 shanerodriguez shanerodriguez::get_instance()
extern void shanerodriguez_get_instance_m8CD885A0840AB96677E119E8E2D907F4C179CA12 (void);
// 0x000000C9 System.Void shanerodriguez::Awake()
extern void shanerodriguez_Awake_mFB2531ECE668171780C046C4B9A1D26B0773CE92 (void);
// 0x000000CA System.Void shanerodriguez::Start()
extern void shanerodriguez_Start_mF3AC9066A78C4C7C2718B4C2451F55482286041D (void);
// 0x000000CB System.Void shanerodriguez::christymoreno()
extern void shanerodriguez_christymoreno_m3A79CE029E97AA79E4CB54C1212D9C8615A559B7 (void);
// 0x000000CC System.Void shanerodriguez::monasuarez()
extern void shanerodriguez_monasuarez_mAC08D69CA3954CAEE4E50B05EB8FA9E8EF9584E4 (void);
// 0x000000CD System.Void shanerodriguez::lakeishakay(System.Object,System.EventArgs)
extern void shanerodriguez_lakeishakay_m0C69A92CB90E50B3A8EC33A1CF34E3023DE3174A (void);
// 0x000000CE System.Void shanerodriguez::elisehaynes()
extern void shanerodriguez_elisehaynes_mD75C8FCC19BECD6F5BFC6435309F22CCB8AB1654 (void);
// 0x000000CF System.Void shanerodriguez::rosalindamorrow()
extern void shanerodriguez_rosalindamorrow_m2E49D25FA62C0466B858B3B8AD1AF215B7752DDD (void);
// 0x000000D0 System.Void shanerodriguez::candyhendrix()
extern void shanerodriguez_candyhendrix_mDE3BC0FB742D5DF346BB1F210CF73962076AE297 (void);
// 0x000000D1 System.Void shanerodriguez::.ctor()
extern void shanerodriguez__ctor_m7BA03EF536646ECB0A82C19086D0AF24FFD915DC (void);
// 0x000000D2 System.Void albernal::aimeepratt(System.String)
extern void albernal_aimeepratt_mA3C19D1BE13370DA4BDBE132923131D736479D65 (void);
// 0x000000D3 System.Void albernal::deeedmonds(System.String)
extern void albernal_deeedmonds_m3EEC312B36AA51DF87D0ACBD5052E31AE7E57DA8 (void);
// 0x000000D4 System.Void albernal::luannratliff()
extern void albernal_luannratliff_mD2BA86DB037419A6FA999F7204C1397069071669 (void);
// 0x000000D5 System.Void albernal::Update()
extern void albernal_Update_m979A1AE239CE94413D39C084C8F8F9B9717F9133 (void);
// 0x000000D6 System.Void albernal::.ctor()
extern void albernal__ctor_m084C0E0B1005227D96971C8DB3C15B36DED8EBB6 (void);
// 0x000000D7 System.Void aureliolyons::Start()
extern void aureliolyons_Start_m5E233214205A0728466C12ACBFB4D72D9B9E2829 (void);
// 0x000000D8 System.Void aureliolyons::gloriadowning()
extern void aureliolyons_gloriadowning_m3397E9E6E72C319574A6657A7786111127CAC702 (void);
// 0x000000D9 System.Void aureliolyons::Update()
extern void aureliolyons_Update_m25903EA39E2E3B14E741CB8530F92470A1F8DC6D (void);
// 0x000000DA System.Void aureliolyons::.ctor()
extern void aureliolyons__ctor_mFF683DBF2F02F616E6DB53B36271F903CEA95F50 (void);
// 0x000000DB System.Void clintrizzo::Awake()
extern void clintrizzo_Awake_m85CEB8EF082940B4CADA0E49C8FC3C22CB322A15 (void);
// 0x000000DC System.Void clintrizzo::Start()
extern void clintrizzo_Start_m077FA0AA9C532EBC5F7D0C6458725B99822ADD36 (void);
// 0x000000DD System.Void clintrizzo::bobbieestrada()
extern void clintrizzo_bobbieestrada_m6723CD2FF581BF917844DAF92874ECA5E4CFC10F (void);
// 0x000000DE System.Void clintrizzo::vickiramey()
extern void clintrizzo_vickiramey_mD71DF03683E5100B4B9A665782EB5FEC6225229C (void);
// 0x000000DF System.Void clintrizzo::maratrevino()
extern void clintrizzo_maratrevino_mE488B02D4F651E21490A5F8850DF84E3FED737E3 (void);
// 0x000000E0 System.Void clintrizzo::Update()
extern void clintrizzo_Update_mA8355138C2F8404A6D63B94B6E4B2D18A96EBC8D (void);
// 0x000000E1 System.Void clintrizzo::britneychilds()
extern void clintrizzo_britneychilds_m599579AB0C0CC3457C77ACC74F12DB56107CCC26 (void);
// 0x000000E2 System.Void clintrizzo::terriecrump()
extern void clintrizzo_terriecrump_mD059A6409B47FDB133DBA4C3864D29BC6B559F97 (void);
// 0x000000E3 System.Void clintrizzo::kenyahubbard()
extern void clintrizzo_kenyahubbard_m05E6927B069FDD94105680A366979A27BDA2896B (void);
// 0x000000E4 System.Void clintrizzo::inesleblanc()
extern void clintrizzo_inesleblanc_mCEE487E09A11F52681FF897E126AB57285BFAB1C (void);
// 0x000000E5 System.Void clintrizzo::peggyalbrecht(System.String)
extern void clintrizzo_peggyalbrecht_mF263101B35585AFEA8E255A758A10CB03D9411EA (void);
// 0x000000E6 System.Void clintrizzo::.ctor()
extern void clintrizzo__ctor_m6449B90CA0C335B742F1D62F43DBD10A6E68367F (void);
// 0x000000E7 System.Void colintobin::.ctor()
extern void colintobin__ctor_mEF3FFA10311D668D8EBD701E326163D3BBD86ADB (void);
// 0x000000E8 darenprice darenprice::get_Instance()
extern void darenprice_get_Instance_m0F2502836C5A89B889A3286639EF6470C40D9F72 (void);
// 0x000000E9 System.Void darenprice::Awake()
extern void darenprice_Awake_mAB50D272DB9C8A2513EF11766C66CD82E17B2D54 (void);
// 0x000000EA System.Void darenprice::Update()
extern void darenprice_Update_mCB8A23E3D7D526CECB466B075A711FA1767F2872 (void);
// 0x000000EB System.Void darenprice::.ctor()
extern void darenprice__ctor_m12C34ADA2664E12BFF01E418BDA285297E338B8F (void);
// 0x000000EC System.Void Data::.cctor()
extern void Data__cctor_m30933DFE5404DED610EC6026AEF0BFF8DFC90961 (void);
// 0x000000ED System.Void eliortega::inesleblanc()
extern void eliortega_inesleblanc_mBE96BEACD2B7F1D066B0BB5DF20279BE8F42BF70 (void);
// 0x000000EE System.Void eliortega::iklan()
extern void eliortega_iklan_mEEA8EF22690F8E50A1EBD4561046E9B986092132 (void);
// 0x000000EF System.Void eliortega::peggyalbrecht()
extern void eliortega_peggyalbrecht_m6A4EBF65E44C1D523747E4714211BDCBF9C88565 (void);
// 0x000000F0 System.Void eliortega::gakaktif()
extern void eliortega_gakaktif_m3D815580B91B9B47E80274D750DEC66F076D96C2 (void);
// 0x000000F1 System.Void eliortega::.ctor()
extern void eliortega__ctor_mBB741ACDF0503CDAA74A26177F122340290ECBFE (void);
// 0x000000F2 System.Void fredrickhightower::Update()
extern void fredrickhightower_Update_m13D00C598D28E5E23C6E99FD9C96EC7C2C933F34 (void);
// 0x000000F3 System.Void fredrickhightower::.ctor()
extern void fredrickhightower__ctor_m1A478A473AEC86DCA6D3FD4D5E15ABBF7D9B1219 (void);
// 0x000000F4 System.Void iraskaggs::Awake()
extern void iraskaggs_Awake_m3CDBA28555EAA354ADEAE698F304696D6F809EFB (void);
// 0x000000F5 System.Void iraskaggs::.ctor()
extern void iraskaggs__ctor_m90EBAF82385585E082E263E7D1D669576C820C73 (void);
// 0x000000F6 System.Void jaimeescobedo::.ctor()
extern void jaimeescobedo__ctor_mDAE2EE491B3A48599084F086E95781A9B11FD460 (void);
// 0x000000F7 System.Void reubenvigil::alejandrayee()
extern void reubenvigil_alejandrayee_mCD60D72DBAF2D7FF585570D0A28DB8CD5E62FB20 (void);
// 0x000000F8 System.Void reubenvigil::.ctor()
extern void reubenvigil__ctor_m04F1C41528B0BF2471CCDFBA578A8CBC9360E605 (void);
// 0x000000F9 ronaldwood ronaldwood::get_Instance()
extern void ronaldwood_get_Instance_m5058552BF11FD3BE672C528CC856A3CACE7E1181 (void);
// 0x000000FA System.Void ronaldwood::Awake()
extern void ronaldwood_Awake_m37B71210FAA5471C7C1B206B12B5BD9ACAD176F1 (void);
// 0x000000FB System.Void ronaldwood::Start()
extern void ronaldwood_Start_m8C68BF73AA2426FFCD4E82411634C1C1C64585CB (void);
// 0x000000FC System.Void ronaldwood::ernestineserrano(System.Int32)
extern void ronaldwood_ernestineserrano_m06E55A532B067B2001FDD8600E683DE8F2635379 (void);
// 0x000000FD System.Void ronaldwood::.ctor()
extern void ronaldwood__ctor_m95243039B12D7CFBA72E0B0B19C693D527A28D62 (void);
// 0x000000FE System.Void vincentsteele::Start()
extern void vincentsteele_Start_mF5797E3ED06599D6D254F6440EFB5E5C8817A3EC (void);
// 0x000000FF System.Void vincentsteele::menu()
extern void vincentsteele_menu_m62EB2BD94214E2663A6B78566FADE3E29548A4DC (void);
// 0x00000100 System.Void vincentsteele::WUI_Open()
extern void vincentsteele_WUI_Open_mC9CE9D0663CE98781DA402B90448E1AFEC5243C1 (void);
// 0x00000101 System.Void vincentsteele::btn_No()
extern void vincentsteele_btn_No_mD2CFC09D840CE88C81A3C0AE9335B2D8D7E5A725 (void);
// 0x00000102 System.Void vincentsteele::btn_yes()
extern void vincentsteele_btn_yes_m8220F1F51BA9512AAC93AAE30692530B92E07C14 (void);
// 0x00000103 System.Void vincentsteele::.ctor()
extern void vincentsteele__ctor_m212071F132B945EAA6A619363570FA7F6CCDF17C (void);
// 0x00000104 System.Void stevemyers::Start()
extern void stevemyers_Start_mF4F2D049F003CB624AE53390E171DC644A26C20E (void);
// 0x00000105 System.Void stevemyers::.ctor()
extern void stevemyers__ctor_m082F8E57D18E0EB9C8384B224D9A89DBE86D669B (void);
// 0x00000106 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7 (void);
// 0x00000107 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46 (void);
// 0x00000108 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722 (void);
// 0x00000109 System.Void ChatController::.ctor()
extern void ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719 (void);
// 0x0000010A System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F (void);
// 0x0000010B System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B (void);
// 0x0000010C System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435 (void);
// 0x0000010D System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9 (void);
// 0x0000010E System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138 (void);
// 0x0000010F System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2 (void);
// 0x00000110 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C (void);
// 0x00000111 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0 (void);
// 0x00000112 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565 (void);
// 0x00000113 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m3EF23BF40634D4262D8A2AE3DB14140FEFB4BF52 (void);
// 0x00000114 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA (void);
// 0x00000115 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9 (void);
// 0x00000116 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0 (void);
// 0x00000117 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3 (void);
// 0x00000118 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D (void);
// 0x00000119 TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3 (void);
// 0x0000011A System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C (void);
// 0x0000011B TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82 (void);
// 0x0000011C System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5 (void);
// 0x0000011D TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C (void);
// 0x0000011E System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3 (void);
// 0x0000011F TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908 (void);
// 0x00000120 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427 (void);
// 0x00000121 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D (void);
// 0x00000122 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF (void);
// 0x00000123 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052 (void);
// 0x00000124 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1 (void);
// 0x00000125 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A (void);
// 0x00000126 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6 (void);
// 0x00000127 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189 (void);
// 0x00000128 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475 (void);
// 0x00000129 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33 (void);
// 0x0000012A System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6 (void);
// 0x0000012B System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02 (void);
// 0x0000012C System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB (void);
// 0x0000012D System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E (void);
// 0x0000012E System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963 (void);
// 0x0000012F System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2 (void);
// 0x00000130 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2 (void);
// 0x00000131 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC (void);
// 0x00000132 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9 (void);
// 0x00000133 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4 (void);
// 0x00000134 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69 (void);
// 0x00000135 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D (void);
// 0x00000136 System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9 (void);
// 0x00000137 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6 (void);
// 0x00000138 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m553F892690ED74A33F57B1359743D31F8BB93C2A (void);
// 0x00000139 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD (void);
// 0x0000013A System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA (void);
// 0x0000013B System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB (void);
// 0x0000013C System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A (void);
// 0x0000013D System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC (void);
// 0x0000013E System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9 (void);
// 0x0000013F System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46 (void);
// 0x00000140 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC9F90586F057E3728D9F93BB0E12197C9B994EEA (void);
// 0x00000141 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A (void);
// 0x00000142 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29 (void);
// 0x00000143 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1 (void);
// 0x00000144 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D (void);
// 0x00000145 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6 (void);
// 0x00000146 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90 (void);
// 0x00000147 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27 (void);
// 0x00000148 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788 (void);
// 0x00000149 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5 (void);
// 0x0000014A System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F (void);
// 0x0000014B System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F (void);
// 0x0000014C System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878 (void);
// 0x0000014D System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72 (void);
// 0x0000014E System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695 (void);
// 0x0000014F System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A (void);
// 0x00000150 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371 (void);
// 0x00000151 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88 (void);
// 0x00000152 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56 (void);
// 0x00000153 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B (void);
// 0x00000154 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D (void);
// 0x00000155 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71 (void);
// 0x00000156 System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC (void);
// 0x00000157 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95 (void);
// 0x00000158 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1C76BF8EAC2CDC2BAC58755622763B9318DA51CA (void);
// 0x00000159 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA (void);
// 0x0000015A System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8 (void);
// 0x0000015B System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE (void);
// 0x0000015C System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807 (void);
// 0x0000015D System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93 (void);
// 0x0000015E System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690 (void);
// 0x0000015F UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB (void);
// 0x00000160 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362 (void);
// 0x00000161 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22 (void);
// 0x00000162 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2 (void);
// 0x00000163 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51 (void);
// 0x00000164 System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E (void);
// 0x00000165 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB (void);
// 0x00000166 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mB6C5974E8F57160AE544E1D2FD44621EEF3ACAB5 (void);
// 0x00000167 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5 (void);
// 0x00000168 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D (void);
// 0x00000169 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6 (void);
// 0x0000016A System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0 (void);
// 0x0000016B System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA (void);
// 0x0000016C System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2 (void);
// 0x0000016D System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45 (void);
// 0x0000016E System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA (void);
// 0x0000016F System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9B7AEE80C1E70D2D2FF5811A54AFD6189CD7F5A9 (void);
// 0x00000170 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8 (void);
// 0x00000171 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C (void);
// 0x00000172 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39 (void);
// 0x00000173 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95 (void);
// 0x00000174 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689 (void);
// 0x00000175 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A (void);
// 0x00000176 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52 (void);
// 0x00000177 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1 (void);
// 0x00000178 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025 (void);
// 0x00000179 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45 (void);
// 0x0000017A System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B (void);
// 0x0000017B System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE (void);
// 0x0000017C System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854 (void);
// 0x0000017D System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mD12057609EFCBCA8E7B61B0421D4A7C5A206C8C3 (void);
// 0x0000017E System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186 (void);
// 0x0000017F System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC (void);
// 0x00000180 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5 (void);
// 0x00000181 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C (void);
// 0x00000182 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683 (void);
// 0x00000183 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mE5E0678716735BDF0D632FE43E392981E75A1C4D (void);
// 0x00000184 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E (void);
// 0x00000185 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2 (void);
// 0x00000186 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49 (void);
// 0x00000187 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148 (void);
// 0x00000188 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE (void);
// 0x00000189 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E (void);
// 0x0000018A System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m272097816057A64A9FFE16F69C6844DCF88E9557 (void);
// 0x0000018B System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_mD3C24C6814482113FD231827E550FBBCC91424A0 (void);
// 0x0000018C System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m83285E807FA4462B99B68D1EB12B2360238C53EB (void);
// 0x0000018D System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m588E025C05E03684A11ABC91B50734A349D28CC8 (void);
// 0x0000018E System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2412DC176F8CA3096658EB0E27AC28218DAEC03A (void);
// 0x0000018F System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mCCE19093B7355F3E23834E27A8517661DF833797 (void);
// 0x00000190 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mE53E0B4DBE6AF5DAC110C3F626B34C5965845E54 (void);
// 0x00000191 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m1ECB51A93EE3B236301948784A3260FD72814923 (void);
// 0x00000192 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m461761745A9C5FF4F7995C3DB33DB43848AEB05B (void);
// 0x00000193 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m1FC162511DF31A9CDBD0101083FBCB11380554C4 (void);
// 0x00000194 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A5E330ACDAD25422A7D642301F58E6C1EE1B041 (void);
// 0x00000195 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_m5A7148435B35A0A84329416FF765D45F6AA0F4E1 (void);
// 0x00000196 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m066140B8D4CD5DE3527A3A05183AE89B487B5D55 (void);
// 0x00000197 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66 (void);
// 0x00000198 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233 (void);
// 0x00000199 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084 (void);
// 0x0000019A System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C (void);
// 0x0000019B System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839 (void);
// 0x0000019C System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063 (void);
// 0x0000019D System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1 (void);
// 0x0000019E System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A (void);
// 0x0000019F System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C (void);
// 0x000001A0 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A (void);
// 0x000001A1 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079 (void);
// 0x000001A2 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723 (void);
// 0x000001A3 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43 (void);
// 0x000001A4 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E (void);
// 0x000001A5 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6 (void);
// 0x000001A6 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE (void);
// 0x000001A7 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215 (void);
// 0x000001A8 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79 (void);
// 0x000001A9 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559 (void);
// 0x000001AA System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779 (void);
// 0x000001AB System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF (void);
// 0x000001AC System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E (void);
// 0x000001AD System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797 (void);
// 0x000001AE System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B (void);
// 0x000001AF System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A (void);
// 0x000001B0 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503 (void);
// 0x000001B1 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE (void);
// 0x000001B2 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87 (void);
// 0x000001B3 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B (void);
// 0x000001B4 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D (void);
// 0x000001B5 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE (void);
// 0x000001B6 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF (void);
// 0x000001B7 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98 (void);
// 0x000001B8 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65 (void);
// 0x000001B9 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97 (void);
// 0x000001BA System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772 (void);
// 0x000001BB System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398 (void);
// 0x000001BC System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301 (void);
// 0x000001BD System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491 (void);
// 0x000001BE System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F (void);
// 0x000001BF System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3 (void);
// 0x000001C0 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36 (void);
// 0x000001C1 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848 (void);
// 0x000001C2 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1 (void);
// 0x000001C3 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B (void);
// 0x000001C4 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC (void);
// 0x000001C5 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1 (void);
// 0x000001C6 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262 (void);
// 0x000001C7 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF (void);
// 0x000001C8 System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60 (void);
// 0x000001C9 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1 (void);
// 0x000001CA System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m319AC50F2DE1572FB7D7AF4F5F65958D01477899 (void);
// 0x000001CB System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C (void);
// 0x000001CC System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C (void);
// 0x000001CD System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354 (void);
// 0x000001CE System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2 (void);
// 0x000001CF System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76 (void);
// 0x000001D0 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8 (void);
// 0x000001D1 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88 (void);
// 0x000001D2 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D (void);
// 0x000001D3 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A (void);
// 0x000001D4 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8 (void);
// 0x000001D5 System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273 (void);
// 0x000001D6 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405 (void);
// 0x000001D7 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m15291DCCCEC264095634B26DD6F24D52360BDAF0 (void);
// 0x000001D8 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9 (void);
// 0x000001D9 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867 (void);
// 0x000001DA System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304 (void);
// 0x000001DB System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23 (void);
// 0x000001DC System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E (void);
// 0x000001DD System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77 (void);
// 0x000001DE System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38 (void);
// 0x000001DF System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB (void);
// 0x000001E0 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A (void);
// 0x000001E1 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7 (void);
// 0x000001E2 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934 (void);
// 0x000001E3 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE (void);
// 0x000001E4 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m2F84864A089CBA0B878B7AC1EA39A49B82682A90 (void);
// 0x000001E5 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B (void);
// 0x000001E6 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E (void);
// 0x000001E7 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21 (void);
// 0x000001E8 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5 (void);
// 0x000001E9 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1 (void);
// 0x000001EA System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D (void);
// 0x000001EB System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F (void);
// 0x000001EC System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198 (void);
// 0x000001ED System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D (void);
// 0x000001EE System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8 (void);
// 0x000001EF System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348 (void);
// 0x000001F0 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845 (void);
// 0x000001F1 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5A5869FEFA67D5E9659F1145B83581D954550C1A (void);
// 0x000001F2 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0 (void);
// 0x000001F3 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0 (void);
// 0x000001F4 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47 (void);
// 0x000001F5 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47 (void);
// 0x000001F6 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB (void);
// 0x000001F7 System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7 (void);
// 0x000001F8 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0 (void);
// 0x000001F9 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422 (void);
// 0x000001FA System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A (void);
// 0x000001FB System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A (void);
// 0x000001FC System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6 (void);
// 0x000001FD System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E (void);
// 0x000001FE System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E (void);
// 0x000001FF System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B (void);
// 0x00000200 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDE5E71C88F5096FD70EB061287ADF0B847732821 (void);
// 0x00000201 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C (void);
// 0x00000202 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F (void);
// 0x00000203 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809 (void);
// 0x00000204 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF (void);
// 0x00000205 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98 (void);
// 0x00000206 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795 (void);
// 0x00000207 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB (void);
// 0x00000208 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53 (void);
// 0x00000209 System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02 (void);
// 0x0000020A System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3 (void);
// 0x0000020B System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m07746C332D2D8CE5DEA59873C26F2FAD4B369B42 (void);
// 0x0000020C System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19 (void);
// 0x0000020D GleyMobileAds.MediationSettings GleyMobileAds.AdSettings::GetAdvertiserSettings(GleyMobileAds.SupportedAdvertisers)
extern void AdSettings_GetAdvertiserSettings_m1E77A663C1F24B189AB61CE7558ABF9DCF78B132 (void);
// 0x0000020E System.Collections.Generic.List`1<GleyMobileAds.PlatformSettings> GleyMobileAds.AdSettings::GetPlaftormSettings(GleyMobileAds.SupportedAdvertisers)
extern void AdSettings_GetPlaftormSettings_m5A8699B4ACC0E748222401F828A7F16913FA1A9E (void);
// 0x0000020F System.Void GleyMobileAds.AdSettings::.ctor()
extern void AdSettings__ctor_mF0E5BEDDC27E4EF89F7C557645C3A4CAAAE13B1F (void);
// 0x00000210 System.Void GleyMobileAds.AdSettings/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mB1BAFA0E455B857F037E6260A9198E1D1B6DABF3 (void);
// 0x00000211 System.Boolean GleyMobileAds.AdSettings/<>c__DisplayClass12_0::<GetAdvertiserSettings>b__0(GleyMobileAds.MediationSettings)
extern void U3CU3Ec__DisplayClass12_0_U3CGetAdvertiserSettingsU3Eb__0_mF478B3EC97BF15937A2F1EF63B1D57D0CA8047D1 (void);
// 0x00000212 System.Void GleyMobileAds.AdSettings/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m106E944E072268975F43D8F18F0D90F02F68B41D (void);
// 0x00000213 System.Boolean GleyMobileAds.AdSettings/<>c__DisplayClass13_0::<GetPlaftormSettings>b__0(GleyMobileAds.AdvertiserSettings)
extern void U3CU3Ec__DisplayClass13_0_U3CGetPlaftormSettingsU3Eb__0_m5DDCE227765A7FBADC4419A599AB2D57D90BF930 (void);
// 0x00000214 System.Int32 GleyMobileAds.AdTypeSettings::get_Order()
extern void AdTypeSettings_get_Order_m7D8BB39251BDE15275517EDDBF3927AF23FB32FC (void);
// 0x00000215 System.Int32 GleyMobileAds.AdTypeSettings::get_Weight()
extern void AdTypeSettings_get_Weight_m6878581FE9D5AB11CF9D726E6A63F66F4DC5D8D1 (void);
// 0x00000216 System.Void GleyMobileAds.AdvertiserId::.ctor()
extern void AdvertiserId__ctor_m17D085AA1BD7D48FC86F61F7968E7D3E5DEC66B9 (void);
// 0x00000217 System.Collections.IEnumerator GleyMobileAds.FileLoader::LoadFile(System.String,System.Boolean)
extern void FileLoader_LoadFile_mDAFF2DE24948C9852577D2475240FDA399D4DB16 (void);
// 0x00000218 System.String GleyMobileAds.FileLoader::GetResult()
extern void FileLoader_GetResult_m17165BAE89943A448A435AC18A79DD19B203309C (void);
// 0x00000219 System.Void GleyMobileAds.FileLoader::.ctor()
extern void FileLoader__ctor_mC9963743FC9A76090B396F79A2613464755311FB (void);
// 0x0000021A System.Void GleyMobileAds.FileLoader/<LoadFile>d__1::.ctor(System.Int32)
extern void U3CLoadFileU3Ed__1__ctor_m970A3C6651804EABF68C71A9DFAF3992F663E737 (void);
// 0x0000021B System.Void GleyMobileAds.FileLoader/<LoadFile>d__1::System.IDisposable.Dispose()
extern void U3CLoadFileU3Ed__1_System_IDisposable_Dispose_m8CB2DB10C98042C306DB5A8A1A72712139448952 (void);
// 0x0000021C System.Boolean GleyMobileAds.FileLoader/<LoadFile>d__1::MoveNext()
extern void U3CLoadFileU3Ed__1_MoveNext_mC47A3C5A697332862F015CB094302E180F803AAF (void);
// 0x0000021D System.Object GleyMobileAds.FileLoader/<LoadFile>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadFileU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m568A7BF4EED5DEC7C2971C6D316803C56164C93E (void);
// 0x0000021E System.Void GleyMobileAds.FileLoader/<LoadFile>d__1::System.Collections.IEnumerator.Reset()
extern void U3CLoadFileU3Ed__1_System_Collections_IEnumerator_Reset_mE9AB34B42AE6397682F53FD2589818713774BEE5 (void);
// 0x0000021F System.Object GleyMobileAds.FileLoader/<LoadFile>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CLoadFileU3Ed__1_System_Collections_IEnumerator_get_Current_m2A7F302E4AF8406B87F84CB59976566E6019AD0C (void);
// 0x00000220 System.Void GleyMobileAds.ICustomAds::InitializeAds(UserConsent,UserConsent,System.Collections.Generic.List`1<GleyMobileAds.PlatformSettings>)
// 0x00000221 System.Boolean GleyMobileAds.ICustomAds::IsRewardVideoAvailable()
// 0x00000222 System.Void GleyMobileAds.ICustomAds::ShowRewardVideo(UnityEngine.Events.UnityAction`1<System.Boolean>)
// 0x00000223 System.Void GleyMobileAds.ICustomAds::ShowRewardVideo(UnityEngine.Events.UnityAction`2<System.Boolean,System.String>)
// 0x00000224 System.Boolean GleyMobileAds.ICustomAds::IsInterstitialAvailable()
// 0x00000225 System.Void GleyMobileAds.ICustomAds::ShowInterstitial(UnityEngine.Events.UnityAction)
// 0x00000226 System.Void GleyMobileAds.ICustomAds::ShowInterstitial(UnityEngine.Events.UnityAction`1<System.String>)
// 0x00000227 System.Boolean GleyMobileAds.ICustomAds::IsBannerAvailable()
// 0x00000228 System.Void GleyMobileAds.ICustomAds::ShowBanner(BannerPosition,BannerType,UnityEngine.Events.UnityAction`3<System.Boolean,BannerPosition,BannerType>)
// 0x00000229 System.Void GleyMobileAds.ICustomAds::HideBanner()
// 0x0000022A System.Boolean GleyMobileAds.ICustomAds::BannerAlreadyUsed()
// 0x0000022B System.Void GleyMobileAds.ICustomAds::ResetBannerUsage()
// 0x0000022C System.Void GleyMobileAds.ICustomAds::UpdateConsent(UserConsent,UserConsent)
// 0x0000022D GleyMobileAds.SupportedAdvertisers GleyMobileAds.MediationSettings::GetAdvertiser()
extern void MediationSettings_GetAdvertiser_m64A2F2780B9BE42FD795383211072AC425D84A91 (void);
// 0x0000022E System.Void GleyMobileAds.ScreenWriter::Write(System.Object)
extern void ScreenWriter_Write_m069355DA11F6B219BF69C5588060436B25B22944 (void);
// 0x0000022F System.Void GleyMobileAds.ScreenWriter::OnGUI()
extern void ScreenWriter_OnGUI_m63D71634A254C362853678A686E5864E0CD80AF8 (void);
// 0x00000230 System.Void GleyMobileAds.ScreenWriter::.ctor()
extern void ScreenWriter__ctor_mA93415325844260A0C9B020F760A90336FC2B747 (void);
// 0x00000231 System.Void GleyMobileAds.CustomAdColony::HideBanner()
extern void CustomAdColony_HideBanner_m67C77B89F76742D517E9CAC0EE6954CF33692CBC (void);
// 0x00000232 System.Void GleyMobileAds.CustomAdColony::InitializeAds(UserConsent,UserConsent,System.Collections.Generic.List`1<GleyMobileAds.PlatformSettings>)
extern void CustomAdColony_InitializeAds_mAC559C88959F13937E8942C3E006034AC61D0280 (void);
// 0x00000233 System.Boolean GleyMobileAds.CustomAdColony::IsBannerAvailable()
extern void CustomAdColony_IsBannerAvailable_m55202EA495D407A45D9BE123C1B9122BA110D0F7 (void);
// 0x00000234 System.Boolean GleyMobileAds.CustomAdColony::IsInterstitialAvailable()
extern void CustomAdColony_IsInterstitialAvailable_m3728314E8872CA431B97C0A3298AB6F772827942 (void);
// 0x00000235 System.Boolean GleyMobileAds.CustomAdColony::IsRewardVideoAvailable()
extern void CustomAdColony_IsRewardVideoAvailable_mD23ACB4AB27DC04A3744A1C3835B56F7C0007738 (void);
// 0x00000236 System.Void GleyMobileAds.CustomAdColony::ResetBannerUsage()
extern void CustomAdColony_ResetBannerUsage_mE2E1F4F3276223B6639C9889B6A40C1B7482AF83 (void);
// 0x00000237 System.Boolean GleyMobileAds.CustomAdColony::BannerAlreadyUsed()
extern void CustomAdColony_BannerAlreadyUsed_m28C96185DE0DF642940A144A6F205FD3E366EA84 (void);
// 0x00000238 System.Void GleyMobileAds.CustomAdColony::ShowBanner(BannerPosition,BannerType,UnityEngine.Events.UnityAction`3<System.Boolean,BannerPosition,BannerType>)
extern void CustomAdColony_ShowBanner_mA5F76CF6CD1300D919F380FD58834050E6A381AC (void);
// 0x00000239 System.Void GleyMobileAds.CustomAdColony::ShowInterstitial(UnityEngine.Events.UnityAction)
extern void CustomAdColony_ShowInterstitial_mD85A33BF0489ED7E3E5794224C67383994386C4C (void);
// 0x0000023A System.Void GleyMobileAds.CustomAdColony::ShowInterstitial(UnityEngine.Events.UnityAction`1<System.String>)
extern void CustomAdColony_ShowInterstitial_mAC10A520CE633A38B1702579A05855641EE47720 (void);
// 0x0000023B System.Void GleyMobileAds.CustomAdColony::ShowRewardVideo(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void CustomAdColony_ShowRewardVideo_m1211295731298D34D73AB1F8A1F3993F4DAA9474 (void);
// 0x0000023C System.Void GleyMobileAds.CustomAdColony::ShowRewardVideo(UnityEngine.Events.UnityAction`2<System.Boolean,System.String>)
extern void CustomAdColony_ShowRewardVideo_mB1B41D547286E7C02E6121D6BE3DA199B9C843DD (void);
// 0x0000023D System.Void GleyMobileAds.CustomAdColony::UpdateConsent(UserConsent,UserConsent)
extern void CustomAdColony_UpdateConsent_m7B52C3E965D0AE34D43CB93029147391DE238259 (void);
// 0x0000023E System.Void GleyMobileAds.CustomAdColony::.ctor()
extern void CustomAdColony__ctor_mB424FD71AAFF886B8C7F256A8D9430FDE15FDB55 (void);
// 0x0000023F System.Void GleyMobileAds.CustomAdmob::InitializeAds(UserConsent,UserConsent,System.Collections.Generic.List`1<GleyMobileAds.PlatformSettings>)
extern void CustomAdmob_InitializeAds_mD94633617C19A4410D925E5FFF2E98AE05206FEE (void);
// 0x00000240 System.Boolean GleyMobileAds.CustomAdmob::IsInterstitialAvailable()
extern void CustomAdmob_IsInterstitialAvailable_mE5BC6944670D120A567C98610A04FD62064667B2 (void);
// 0x00000241 System.Boolean GleyMobileAds.CustomAdmob::IsRewardVideoAvailable()
extern void CustomAdmob_IsRewardVideoAvailable_m0159130F2333318FE135E94A699FC8C6A0182E53 (void);
// 0x00000242 System.Void GleyMobileAds.CustomAdmob::ShowInterstitial(UnityEngine.Events.UnityAction)
extern void CustomAdmob_ShowInterstitial_m5F4CA0EED18DCD7D2F069AC46A65455472FE660A (void);
// 0x00000243 System.Void GleyMobileAds.CustomAdmob::ShowInterstitial(UnityEngine.Events.UnityAction`1<System.String>)
extern void CustomAdmob_ShowInterstitial_m096E97FBFC86DB92A23D03C943B9F31DDA9CABEE (void);
// 0x00000244 System.Void GleyMobileAds.CustomAdmob::ShowRewardVideo(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void CustomAdmob_ShowRewardVideo_mD73E1D238761E8D2A2712B5A1763DC718949A917 (void);
// 0x00000245 System.Void GleyMobileAds.CustomAdmob::HideBanner()
extern void CustomAdmob_HideBanner_mE6A838E0DD344400C6FB35C589DF5489047F03DB (void);
// 0x00000246 System.Boolean GleyMobileAds.CustomAdmob::IsBannerAvailable()
extern void CustomAdmob_IsBannerAvailable_m9888FCC5F0043970D55AFB2023AF8090AD9579D4 (void);
// 0x00000247 System.Void GleyMobileAds.CustomAdmob::ResetBannerUsage()
extern void CustomAdmob_ResetBannerUsage_m39DA0BD00AFC29B1990F4AFAA4C0E2335D684DA5 (void);
// 0x00000248 System.Boolean GleyMobileAds.CustomAdmob::BannerAlreadyUsed()
extern void CustomAdmob_BannerAlreadyUsed_m9B9CAA130322C5593567A4F4FF66B25FE9EAD499 (void);
// 0x00000249 System.Void GleyMobileAds.CustomAdmob::ShowBanner(BannerPosition,BannerType,UnityEngine.Events.UnityAction`3<System.Boolean,BannerPosition,BannerType>)
extern void CustomAdmob_ShowBanner_m69DB8B4C5ED7391C765B54E927BC8186E87C938F (void);
// 0x0000024A System.Void GleyMobileAds.CustomAdmob::ShowRewardVideo(UnityEngine.Events.UnityAction`2<System.Boolean,System.String>)
extern void CustomAdmob_ShowRewardVideo_mA5DF20A3FD9A6AE0C17AEDA5A479719B08E3C430 (void);
// 0x0000024B System.Void GleyMobileAds.CustomAdmob::UpdateConsent(UserConsent,UserConsent)
extern void CustomAdmob_UpdateConsent_m047AB4CE80E08EFA72E7D302659101CE124C4E81 (void);
// 0x0000024C System.Void GleyMobileAds.CustomAdmob::.ctor()
extern void CustomAdmob__ctor_m579D71179E7AFAB559F57EB2F1EF7384E8FACE85 (void);
// 0x0000024D System.Void GleyMobileAds.CustomAppLovin::InitializeAds(UserConsent,UserConsent,System.Collections.Generic.List`1<GleyMobileAds.PlatformSettings>)
extern void CustomAppLovin_InitializeAds_m0BB3CD522BF6726FC7B7F6E67028ACC1E200191D (void);
// 0x0000024E System.Void GleyMobileAds.CustomAppLovin::UpdateConsent(UserConsent,UserConsent)
extern void CustomAppLovin_UpdateConsent_m30BE118B1AEEBF318A0A464F6EDD24A59BC719A6 (void);
// 0x0000024F System.Boolean GleyMobileAds.CustomAppLovin::IsBannerAvailable()
extern void CustomAppLovin_IsBannerAvailable_m2F5F25216EB9EEDCB759AE834197EBF596FA7379 (void);
// 0x00000250 System.Void GleyMobileAds.CustomAppLovin::ShowBanner(BannerPosition,BannerType,UnityEngine.Events.UnityAction`3<System.Boolean,BannerPosition,BannerType>)
extern void CustomAppLovin_ShowBanner_m0E50800F94F991D0E9212026F175C5CB9C2E57A8 (void);
// 0x00000251 System.Void GleyMobileAds.CustomAppLovin::HideBanner()
extern void CustomAppLovin_HideBanner_m1D5209AFA4EB1DB8373E385FB563A14CBC8E070F (void);
// 0x00000252 System.Void GleyMobileAds.CustomAppLovin::ResetBannerUsage()
extern void CustomAppLovin_ResetBannerUsage_m2601123941B452988E9B8F5557880259BC27342F (void);
// 0x00000253 System.Boolean GleyMobileAds.CustomAppLovin::BannerAlreadyUsed()
extern void CustomAppLovin_BannerAlreadyUsed_mC1998E02D0CA95ACAECD731D0D2E8341AB453078 (void);
// 0x00000254 System.Boolean GleyMobileAds.CustomAppLovin::IsInterstitialAvailable()
extern void CustomAppLovin_IsInterstitialAvailable_m79BA5B3F3693EA770F91EC8D99CB7BD32C907737 (void);
// 0x00000255 System.Void GleyMobileAds.CustomAppLovin::ShowInterstitial(UnityEngine.Events.UnityAction)
extern void CustomAppLovin_ShowInterstitial_m8C6B3C7CCA6DE40B82D6FFD271CFB9E6A02557AA (void);
// 0x00000256 System.Void GleyMobileAds.CustomAppLovin::ShowInterstitial(UnityEngine.Events.UnityAction`1<System.String>)
extern void CustomAppLovin_ShowInterstitial_m9A137650350497EB33FC36B46CDDAB2B9226DEA6 (void);
// 0x00000257 System.Boolean GleyMobileAds.CustomAppLovin::IsRewardVideoAvailable()
extern void CustomAppLovin_IsRewardVideoAvailable_mF9AF2DEC963A890ADEBDF1434BF7D10929695565 (void);
// 0x00000258 System.Void GleyMobileAds.CustomAppLovin::ShowRewardVideo(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void CustomAppLovin_ShowRewardVideo_m7A6D6AA733FED4DC53CEB45D2EE8BB10647605D5 (void);
// 0x00000259 System.Void GleyMobileAds.CustomAppLovin::ShowRewardVideo(UnityEngine.Events.UnityAction`2<System.Boolean,System.String>)
extern void CustomAppLovin_ShowRewardVideo_m2E187E6A02874B3A45BA33B30E181D1F0963BBA7 (void);
// 0x0000025A System.Void GleyMobileAds.CustomAppLovin::.ctor()
extern void CustomAppLovin__ctor_m30A6378ED182F9C5183C8EECF4775AC39ED9390E (void);
// 0x0000025B System.Void GleyMobileAds.CustomChartboost::HideBanner()
extern void CustomChartboost_HideBanner_mCF05919C7039563916824B384567050E70E720BF (void);
// 0x0000025C System.Void GleyMobileAds.CustomChartboost::InitializeAds(UserConsent,UserConsent,System.Collections.Generic.List`1<GleyMobileAds.PlatformSettings>)
extern void CustomChartboost_InitializeAds_m13AA8E4F09F872D689E7DD3FAFA4B0C91F04270D (void);
// 0x0000025D System.Boolean GleyMobileAds.CustomChartboost::IsBannerAvailable()
extern void CustomChartboost_IsBannerAvailable_mA4BC1381E83A057E0C5AE10DE42A99AA872EFD59 (void);
// 0x0000025E System.Void GleyMobileAds.CustomChartboost::ResetBannerUsage()
extern void CustomChartboost_ResetBannerUsage_m809A953549381A94686F4D24439A08D6E5FC967A (void);
// 0x0000025F System.Boolean GleyMobileAds.CustomChartboost::BannerAlreadyUsed()
extern void CustomChartboost_BannerAlreadyUsed_m2C51D9FB9B14E4E4712CE4BFE6745C5D256F6A39 (void);
// 0x00000260 System.Boolean GleyMobileAds.CustomChartboost::IsInterstitialAvailable()
extern void CustomChartboost_IsInterstitialAvailable_m9EB774903EBA3B09D1DCB751B834F683A1548B76 (void);
// 0x00000261 System.Boolean GleyMobileAds.CustomChartboost::IsRewardVideoAvailable()
extern void CustomChartboost_IsRewardVideoAvailable_mCD06B13782196EC0EC77E4BFFEE1224E85CFCF5B (void);
// 0x00000262 System.Void GleyMobileAds.CustomChartboost::ShowBanner(BannerPosition,BannerType,UnityEngine.Events.UnityAction`3<System.Boolean,BannerPosition,BannerType>)
extern void CustomChartboost_ShowBanner_m7FD4E320FEA43DBB06EDE795CEE8F2FADA74DF8C (void);
// 0x00000263 System.Void GleyMobileAds.CustomChartboost::ShowInterstitial(UnityEngine.Events.UnityAction)
extern void CustomChartboost_ShowInterstitial_m15330E7DB53F7CEA0314882168A30C85775CB836 (void);
// 0x00000264 System.Void GleyMobileAds.CustomChartboost::ShowInterstitial(UnityEngine.Events.UnityAction`1<System.String>)
extern void CustomChartboost_ShowInterstitial_m7AB171DDA8F7850431519C3E5DBC10EC21228060 (void);
// 0x00000265 System.Void GleyMobileAds.CustomChartboost::ShowRewardVideo(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void CustomChartboost_ShowRewardVideo_m112711ED7C93B0DDE8481FC817447C92F53B3DF8 (void);
// 0x00000266 System.Void GleyMobileAds.CustomChartboost::ShowRewardVideo(UnityEngine.Events.UnityAction`2<System.Boolean,System.String>)
extern void CustomChartboost_ShowRewardVideo_m4FFACF4951DC2413F44EB9A4EF2E83979A5E6216 (void);
// 0x00000267 System.Void GleyMobileAds.CustomChartboost::UpdateConsent(UserConsent,UserConsent)
extern void CustomChartboost_UpdateConsent_mBC54F1F4E6EEAE01414B3D148012F0F97762C78D (void);
// 0x00000268 System.Void GleyMobileAds.CustomChartboost::.ctor()
extern void CustomChartboost__ctor_mE73B46B5BAB0E42557D1F23CF2069F1D1CF54D5A (void);
// 0x00000269 System.Boolean GleyMobileAds.CustomFacebook::BannerAlreadyUsed()
extern void CustomFacebook_BannerAlreadyUsed_mBB7EAC381AD5CA798C0778A25E919812C5ED732F (void);
// 0x0000026A System.Void GleyMobileAds.CustomFacebook::HideBanner()
extern void CustomFacebook_HideBanner_m82178BDF66D1FEB158D6F2B1B23FE74FD9976CDB (void);
// 0x0000026B System.Void GleyMobileAds.CustomFacebook::InitializeAds(UserConsent,UserConsent,System.Collections.Generic.List`1<GleyMobileAds.PlatformSettings>)
extern void CustomFacebook_InitializeAds_m9E930892F1BE1D6CFE0C38B6F1F0A9D84D233482 (void);
// 0x0000026C System.Boolean GleyMobileAds.CustomFacebook::IsBannerAvailable()
extern void CustomFacebook_IsBannerAvailable_m1CDBEFC83CFE385542F74C6A7A7074FB914F2057 (void);
// 0x0000026D System.Boolean GleyMobileAds.CustomFacebook::IsInterstitialAvailable()
extern void CustomFacebook_IsInterstitialAvailable_m541DF91EA4D5F5DF7B953EEEC9C8294935373804 (void);
// 0x0000026E System.Boolean GleyMobileAds.CustomFacebook::IsRewardVideoAvailable()
extern void CustomFacebook_IsRewardVideoAvailable_mC6B9F6E4AFDBF242D037427DC611D79C56582EDB (void);
// 0x0000026F System.Void GleyMobileAds.CustomFacebook::ResetBannerUsage()
extern void CustomFacebook_ResetBannerUsage_m167655BB42B8C41DC54A995B1E43312BB6F5A52B (void);
// 0x00000270 System.Void GleyMobileAds.CustomFacebook::ShowBanner(BannerPosition,BannerType,UnityEngine.Events.UnityAction`3<System.Boolean,BannerPosition,BannerType>)
extern void CustomFacebook_ShowBanner_m1ED7F4F8821FE5065975484CEEEA4996DDA85581 (void);
// 0x00000271 System.Void GleyMobileAds.CustomFacebook::ShowInterstitial(UnityEngine.Events.UnityAction)
extern void CustomFacebook_ShowInterstitial_mC3280B1966245225E21666E109344FD24F9B510F (void);
// 0x00000272 System.Void GleyMobileAds.CustomFacebook::ShowInterstitial(UnityEngine.Events.UnityAction`1<System.String>)
extern void CustomFacebook_ShowInterstitial_m90F1CD5D16129CA0F70F64BB52BFA31306CD2D6C (void);
// 0x00000273 System.Void GleyMobileAds.CustomFacebook::ShowRewardVideo(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void CustomFacebook_ShowRewardVideo_mFF3AE431E7279BB711A8537763E49D6265900348 (void);
// 0x00000274 System.Void GleyMobileAds.CustomFacebook::ShowRewardVideo(UnityEngine.Events.UnityAction`2<System.Boolean,System.String>)
extern void CustomFacebook_ShowRewardVideo_m46EC138CC20BDD67A5840DB0EECDF50668DED0E7 (void);
// 0x00000275 System.Void GleyMobileAds.CustomFacebook::UpdateConsent(UserConsent,UserConsent)
extern void CustomFacebook_UpdateConsent_mF5ED957C5C108E53AC5E8C23A53E3F742EC8957B (void);
// 0x00000276 System.Void GleyMobileAds.CustomFacebook::.ctor()
extern void CustomFacebook__ctor_m1EB10359D2E1E52280C7FDD1CDD277ED18CCDA6B (void);
// 0x00000277 System.Void GleyMobileAds.CustomHeyzap::HideBanner()
extern void CustomHeyzap_HideBanner_mC51C233CFCDB7549ABBFF5297E5EEDC85252E03F (void);
// 0x00000278 System.Void GleyMobileAds.CustomHeyzap::InitializeAds(UserConsent,UserConsent,System.Collections.Generic.List`1<GleyMobileAds.PlatformSettings>)
extern void CustomHeyzap_InitializeAds_m70CD061B5D5E08B3EB255039133BDA1CCBC9A8DB (void);
// 0x00000279 System.Boolean GleyMobileAds.CustomHeyzap::IsBannerAvailable()
extern void CustomHeyzap_IsBannerAvailable_mB306F799769A2C3B87A1DB77EB30A0963D454939 (void);
// 0x0000027A System.Void GleyMobileAds.CustomHeyzap::ResetBannerUsage()
extern void CustomHeyzap_ResetBannerUsage_mAEE75F74D364A7D0006B87EA1BAC0C02C9B2F409 (void);
// 0x0000027B System.Boolean GleyMobileAds.CustomHeyzap::BannerAlreadyUsed()
extern void CustomHeyzap_BannerAlreadyUsed_m4C68730C2A12C2DF3ADCD8982CD585363BBCCE75 (void);
// 0x0000027C System.Boolean GleyMobileAds.CustomHeyzap::IsInterstitialAvailable()
extern void CustomHeyzap_IsInterstitialAvailable_mC2C20599E12682C72F11EEC2D0CB821E2ACF49D6 (void);
// 0x0000027D System.Boolean GleyMobileAds.CustomHeyzap::IsRewardVideoAvailable()
extern void CustomHeyzap_IsRewardVideoAvailable_m4746B157E86BAB6E3B9763ACBD90AEB8DA4E6041 (void);
// 0x0000027E System.Void GleyMobileAds.CustomHeyzap::ShowBanner(BannerPosition,BannerType,UnityEngine.Events.UnityAction`3<System.Boolean,BannerPosition,BannerType>)
extern void CustomHeyzap_ShowBanner_mC121591CF89DF5044A54AE30A443C0C80CEB6788 (void);
// 0x0000027F System.Void GleyMobileAds.CustomHeyzap::ShowInterstitial(UnityEngine.Events.UnityAction)
extern void CustomHeyzap_ShowInterstitial_m71661788F1513FF5C3EAA7FDE2201F24777CC561 (void);
// 0x00000280 System.Void GleyMobileAds.CustomHeyzap::ShowInterstitial(UnityEngine.Events.UnityAction`1<System.String>)
extern void CustomHeyzap_ShowInterstitial_mEED68C3F969F59269980FC41CB44B9D5665FF6CD (void);
// 0x00000281 System.Void GleyMobileAds.CustomHeyzap::ShowRewardVideo(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void CustomHeyzap_ShowRewardVideo_mC3A0B071A8A71E2E33674ACBBA2974EB4F83F7E8 (void);
// 0x00000282 System.Void GleyMobileAds.CustomHeyzap::ShowRewardVideo(UnityEngine.Events.UnityAction`2<System.Boolean,System.String>)
extern void CustomHeyzap_ShowRewardVideo_mB2744537F37DA9FBEF69F779C8F6679FCE0A43A9 (void);
// 0x00000283 System.Void GleyMobileAds.CustomHeyzap::UpdateConsent(UserConsent,UserConsent)
extern void CustomHeyzap_UpdateConsent_m5D859125EE517A053ADE1D7DDBC71E53A4FA7794 (void);
// 0x00000284 System.Void GleyMobileAds.CustomHeyzap::.ctor()
extern void CustomHeyzap__ctor_mD06C0AE8A9BC7CF13E201E317A5E3F05EFA51BB7 (void);
// 0x00000285 System.Boolean GleyMobileAds.CustomIronSource::BannerAlreadyUsed()
extern void CustomIronSource_BannerAlreadyUsed_m5200D6AFCF21E47A94ABCCD375A621B41D024646 (void);
// 0x00000286 System.Void GleyMobileAds.CustomIronSource::HideBanner()
extern void CustomIronSource_HideBanner_mDEDCC1436C331E5AA2209D474C5A46021DD79E02 (void);
// 0x00000287 System.Void GleyMobileAds.CustomIronSource::InitializeAds(UserConsent,UserConsent,System.Collections.Generic.List`1<GleyMobileAds.PlatformSettings>)
extern void CustomIronSource_InitializeAds_m05E00D52BF0D5C5A523D6E963B9FEE108DA57230 (void);
// 0x00000288 System.Boolean GleyMobileAds.CustomIronSource::IsBannerAvailable()
extern void CustomIronSource_IsBannerAvailable_m1BB38927A86FB665C02897B1B261FAD6A1252E05 (void);
// 0x00000289 System.Boolean GleyMobileAds.CustomIronSource::IsInterstitialAvailable()
extern void CustomIronSource_IsInterstitialAvailable_mC384A984D44916F43473056CA69C046BE19264D7 (void);
// 0x0000028A System.Boolean GleyMobileAds.CustomIronSource::IsRewardVideoAvailable()
extern void CustomIronSource_IsRewardVideoAvailable_m56EC0AB71C0E7D4AA13D6FBB009DEFB853A7EF95 (void);
// 0x0000028B System.Void GleyMobileAds.CustomIronSource::ResetBannerUsage()
extern void CustomIronSource_ResetBannerUsage_m62B6E25A645DF6B2FDE5CA780DAB0849E0C6E43A (void);
// 0x0000028C System.Void GleyMobileAds.CustomIronSource::ShowBanner(BannerPosition,BannerType,UnityEngine.Events.UnityAction`3<System.Boolean,BannerPosition,BannerType>)
extern void CustomIronSource_ShowBanner_m4FCC85857ED07A37F26F3D35C3228CD0311C09F3 (void);
// 0x0000028D System.Void GleyMobileAds.CustomIronSource::ShowInterstitial(UnityEngine.Events.UnityAction)
extern void CustomIronSource_ShowInterstitial_m8F8B63159225E947756F1F51D081E3B8D8FF2873 (void);
// 0x0000028E System.Void GleyMobileAds.CustomIronSource::ShowInterstitial(UnityEngine.Events.UnityAction`1<System.String>)
extern void CustomIronSource_ShowInterstitial_mC7F8425FE69CF8465F01AD4883DBF11F2757EC74 (void);
// 0x0000028F System.Void GleyMobileAds.CustomIronSource::ShowRewardVideo(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void CustomIronSource_ShowRewardVideo_m8D4561F5039FC9DC46FA2E93AD867684BA3FC287 (void);
// 0x00000290 System.Void GleyMobileAds.CustomIronSource::ShowRewardVideo(UnityEngine.Events.UnityAction`2<System.Boolean,System.String>)
extern void CustomIronSource_ShowRewardVideo_mF56231EE7B84F7C65A58D1F787CE32513F03F6BD (void);
// 0x00000291 System.Void GleyMobileAds.CustomIronSource::UpdateConsent(UserConsent,UserConsent)
extern void CustomIronSource_UpdateConsent_m95A3688C073195CDEFF8F2EC62F3A38B924A1BC0 (void);
// 0x00000292 System.Void GleyMobileAds.CustomIronSource::.ctor()
extern void CustomIronSource__ctor_m2534961FF616A935C5E3E4DF41858B9EEA5F0683 (void);
// 0x00000293 System.Boolean GleyMobileAds.CustomMoPub::BannerAlreadyUsed()
extern void CustomMoPub_BannerAlreadyUsed_mE47384CE8C7FD0968BFEA1C06840977BEE253D3C (void);
// 0x00000294 System.Void GleyMobileAds.CustomMoPub::HideBanner()
extern void CustomMoPub_HideBanner_mBF4E8DB48335DDD5E4F957ED306ACF245CE83BCC (void);
// 0x00000295 System.Void GleyMobileAds.CustomMoPub::InitializeAds(UserConsent,UserConsent,System.Collections.Generic.List`1<GleyMobileAds.PlatformSettings>)
extern void CustomMoPub_InitializeAds_m8922631807A2B7247C47C365D68A74BDCFCFA05B (void);
// 0x00000296 System.Boolean GleyMobileAds.CustomMoPub::IsBannerAvailable()
extern void CustomMoPub_IsBannerAvailable_mA8025F7C6C0AA2B673A3CB989B1AB0B5BB9B178A (void);
// 0x00000297 System.Boolean GleyMobileAds.CustomMoPub::IsInterstitialAvailable()
extern void CustomMoPub_IsInterstitialAvailable_mDC65CC333362BED3B5F4BC0B55F7998C89DEF9D8 (void);
// 0x00000298 System.Boolean GleyMobileAds.CustomMoPub::IsRewardVideoAvailable()
extern void CustomMoPub_IsRewardVideoAvailable_mACBA0298B71F1A9DB9B9AFB1425AC4BCAB5B9609 (void);
// 0x00000299 System.Void GleyMobileAds.CustomMoPub::ResetBannerUsage()
extern void CustomMoPub_ResetBannerUsage_m210E3DEEB1CA7CE2D07929F64E7C71BCF816B097 (void);
// 0x0000029A System.Void GleyMobileAds.CustomMoPub::ShowBanner(BannerPosition,BannerType,UnityEngine.Events.UnityAction`3<System.Boolean,BannerPosition,BannerType>)
extern void CustomMoPub_ShowBanner_m503DEE7AF9581F89D76BD8DC34E1FAF492DDEB0F (void);
// 0x0000029B System.Void GleyMobileAds.CustomMoPub::ShowInterstitial(UnityEngine.Events.UnityAction)
extern void CustomMoPub_ShowInterstitial_m7989DEDB8A65ECBA8F2EE8CB4EC193AE2E6BA715 (void);
// 0x0000029C System.Void GleyMobileAds.CustomMoPub::ShowInterstitial(UnityEngine.Events.UnityAction`1<System.String>)
extern void CustomMoPub_ShowInterstitial_mA949C1605C5DEB2F009641DF58D6570BA5DABAB9 (void);
// 0x0000029D System.Void GleyMobileAds.CustomMoPub::ShowRewardVideo(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void CustomMoPub_ShowRewardVideo_m56C1688567736F1392DEF53F6B66CD6834803AE1 (void);
// 0x0000029E System.Void GleyMobileAds.CustomMoPub::ShowRewardVideo(UnityEngine.Events.UnityAction`2<System.Boolean,System.String>)
extern void CustomMoPub_ShowRewardVideo_mF4F8783B111741F7D0271551A42E9E90FFD38F3F (void);
// 0x0000029F System.Void GleyMobileAds.CustomMoPub::UpdateConsent(UserConsent,UserConsent)
extern void CustomMoPub_UpdateConsent_m70153BFB4F0291458E39E37A1779D98F6EFD98CE (void);
// 0x000002A0 System.Void GleyMobileAds.CustomMoPub::.ctor()
extern void CustomMoPub__ctor_m966364C41B91C225D0A267263294F5B6A7D317D1 (void);
// 0x000002A1 System.Void GleyMobileAds.CustomUnityAds::InitializeAds(UserConsent,UserConsent,System.Collections.Generic.List`1<GleyMobileAds.PlatformSettings>)
extern void CustomUnityAds_InitializeAds_m7EDB62658A42799F791F11758DC0D7EBE9151825 (void);
// 0x000002A2 System.Void GleyMobileAds.CustomUnityAds::UpdateConsent(UserConsent,UserConsent)
extern void CustomUnityAds_UpdateConsent_m5B308531B63CA201A803BBB37E5D104339D51F74 (void);
// 0x000002A3 System.Void GleyMobileAds.CustomUnityAds::OnInitializationComplete()
extern void CustomUnityAds_OnInitializationComplete_m54A63A944BA0E473F680DC5DB3D78D3F746B7C42 (void);
// 0x000002A4 System.Void GleyMobileAds.CustomUnityAds::OnInitializationFailed(UnityEngine.Advertisements.UnityAdsInitializationError,System.String)
extern void CustomUnityAds_OnInitializationFailed_mA17CEE610E200C2292488EF29B3C56AF16AE3E81 (void);
// 0x000002A5 System.Boolean GleyMobileAds.CustomUnityAds::IsInterstitialAvailable()
extern void CustomUnityAds_IsInterstitialAvailable_m9AEADFED526D379C65FABBF16778FF36BD013BDE (void);
// 0x000002A6 System.Void GleyMobileAds.CustomUnityAds::LoadInterstitialAd()
extern void CustomUnityAds_LoadInterstitialAd_mA5495BA7D84931A1707E51BB2BB696D167E73CF6 (void);
// 0x000002A7 System.Void GleyMobileAds.CustomUnityAds::ShowInterstitial(UnityEngine.Events.UnityAction)
extern void CustomUnityAds_ShowInterstitial_mAC0BB9A1ED0B2E25EA7F274906B6E7A77259F9F0 (void);
// 0x000002A8 System.Void GleyMobileAds.CustomUnityAds::ShowInterstitial(UnityEngine.Events.UnityAction`1<System.String>)
extern void CustomUnityAds_ShowInterstitial_m2438EE3456BE7EE236B61492E2492E0D2D72A36A (void);
// 0x000002A9 System.Boolean GleyMobileAds.CustomUnityAds::IsRewardVideoAvailable()
extern void CustomUnityAds_IsRewardVideoAvailable_m5E45604CC646DD9E26E297C79C48BC0A29D88558 (void);
// 0x000002AA System.Void GleyMobileAds.CustomUnityAds::LoadRewardedVideo()
extern void CustomUnityAds_LoadRewardedVideo_m51A3E35695493DBD7AD1C1B7C3F6C1A52882B8B0 (void);
// 0x000002AB System.Void GleyMobileAds.CustomUnityAds::ShowRewardVideo(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void CustomUnityAds_ShowRewardVideo_m9CAF177D09CCC9E941DC26065BEEC04F040CC20F (void);
// 0x000002AC System.Void GleyMobileAds.CustomUnityAds::ShowRewardVideo(UnityEngine.Events.UnityAction`2<System.Boolean,System.String>)
extern void CustomUnityAds_ShowRewardVideo_m306596F70EE30024436E86CEB840B27565ACE456 (void);
// 0x000002AD System.Void GleyMobileAds.CustomUnityAds::OnUnityAdsAdLoaded(System.String)
extern void CustomUnityAds_OnUnityAdsAdLoaded_m2FEA529318E8EDB0602F2B96519EBC836DC8C7CD (void);
// 0x000002AE System.Void GleyMobileAds.CustomUnityAds::OnUnityAdsFailedToLoad(System.String,UnityEngine.Advertisements.UnityAdsLoadError,System.String)
extern void CustomUnityAds_OnUnityAdsFailedToLoad_mCA36574A6E4F033770B7E507D1470ABCE68BF840 (void);
// 0x000002AF System.Void GleyMobileAds.CustomUnityAds::OnUnityAdsShowFailure(System.String,UnityEngine.Advertisements.UnityAdsShowError,System.String)
extern void CustomUnityAds_OnUnityAdsShowFailure_mF6A11A2837FB31A46B9CCCFE5C97D391DB8A77D7 (void);
// 0x000002B0 System.Void GleyMobileAds.CustomUnityAds::OnUnityAdsShowStart(System.String)
extern void CustomUnityAds_OnUnityAdsShowStart_m914DC9FDE3999866F50C0E02AF15FA460D599869 (void);
// 0x000002B1 System.Void GleyMobileAds.CustomUnityAds::OnUnityAdsShowClick(System.String)
extern void CustomUnityAds_OnUnityAdsShowClick_m1EF62371F2374020AE559055B797F8BC934F6DB5 (void);
// 0x000002B2 System.Void GleyMobileAds.CustomUnityAds::OnUnityAdsShowComplete(System.String,UnityEngine.Advertisements.UnityAdsShowCompletionState)
extern void CustomUnityAds_OnUnityAdsShowComplete_mDCC998C3000DE03C5C02548E681BED22C923DE82 (void);
// 0x000002B3 System.Boolean GleyMobileAds.CustomUnityAds::IsBannerAvailable()
extern void CustomUnityAds_IsBannerAvailable_m87D5C224168BC39A63B019020DC28749309DDB5D (void);
// 0x000002B4 System.Void GleyMobileAds.CustomUnityAds::ResetBannerUsage()
extern void CustomUnityAds_ResetBannerUsage_m96E1D790E6CDB971907914D3D1C2A80819B13EF6 (void);
// 0x000002B5 System.Boolean GleyMobileAds.CustomUnityAds::BannerAlreadyUsed()
extern void CustomUnityAds_BannerAlreadyUsed_m1C15E006270341620FAA97327BF982024E6DA956 (void);
// 0x000002B6 System.Void GleyMobileAds.CustomUnityAds::ShowBanner(BannerPosition,BannerType,UnityEngine.Events.UnityAction`3<System.Boolean,BannerPosition,BannerType>)
extern void CustomUnityAds_ShowBanner_m7F85B13044939763C6A664F09763346EB376F550 (void);
// 0x000002B7 System.Void GleyMobileAds.CustomUnityAds::BannerLoadSuccess()
extern void CustomUnityAds_BannerLoadSuccess_m52CBCEB7B964B2186F26633752858799688AA259 (void);
// 0x000002B8 System.Void GleyMobileAds.CustomUnityAds::BannerLoadFailed(System.String)
extern void CustomUnityAds_BannerLoadFailed_m1ADB483C0759DFBFF6682C6068F556E4E659BE2B (void);
// 0x000002B9 System.Void GleyMobileAds.CustomUnityAds::BanerDisplayed()
extern void CustomUnityAds_BanerDisplayed_m3E2F8D295B5D0AF0E1198CB4C308EEA8C2111D0C (void);
// 0x000002BA System.Void GleyMobileAds.CustomUnityAds::BannerHidded()
extern void CustomUnityAds_BannerHidded_m8A71C0D1741BE0662EC11B6138AFD8D54B92B236 (void);
// 0x000002BB System.Void GleyMobileAds.CustomUnityAds::HideBanner()
extern void CustomUnityAds_HideBanner_m7EA9F46F93D8624C0A55A4185944B5A7236C5EBC (void);
// 0x000002BC System.Void GleyMobileAds.CustomUnityAds::.ctor()
extern void CustomUnityAds__ctor_m89D1595DC90A673AF78961A433344067F15FB00D (void);
// 0x000002BD System.Void GleyMobileAds.CustomUnityAds/<>c::.cctor()
extern void U3CU3Ec__cctor_mC50B844DD3247AD830FAB618C55697412ED20CED (void);
// 0x000002BE System.Void GleyMobileAds.CustomUnityAds/<>c::.ctor()
extern void U3CU3Ec__ctor_mCB38407397A4908EDEBDD0D0FCFF3FDF69727FB2 (void);
// 0x000002BF System.Boolean GleyMobileAds.CustomUnityAds/<>c::<InitializeAds>b__19_0(GleyMobileAds.PlatformSettings)
extern void U3CU3Ec_U3CInitializeAdsU3Eb__19_0_mA30A1CEFD7AB06B23A48FA9C4F25F09DD970893B (void);
// 0x000002C0 System.Void GleyMobileAds.CustomVungle::HideBanner()
extern void CustomVungle_HideBanner_m5A6EB2EEBE04D84A478EC6D9BD889B0C70D45BF2 (void);
// 0x000002C1 System.Void GleyMobileAds.CustomVungle::InitializeAds(UserConsent,UserConsent,System.Collections.Generic.List`1<GleyMobileAds.PlatformSettings>)
extern void CustomVungle_InitializeAds_m8B26BB356972E521985A11DAD40A4324C8C36F7E (void);
// 0x000002C2 System.Void GleyMobileAds.CustomVungle::ResetBannerUsage()
extern void CustomVungle_ResetBannerUsage_mF81AD6EFB3C29E4D81358CE26DCD96116BC21D41 (void);
// 0x000002C3 System.Boolean GleyMobileAds.CustomVungle::BannerAlreadyUsed()
extern void CustomVungle_BannerAlreadyUsed_mA723D513D6DBB72E967D1535B0500F5555CAE5FB (void);
// 0x000002C4 System.Boolean GleyMobileAds.CustomVungle::IsBannerAvailable()
extern void CustomVungle_IsBannerAvailable_mA51536DC2E4913F94A9E322104BF4B9E2116885C (void);
// 0x000002C5 System.Boolean GleyMobileAds.CustomVungle::IsInterstitialAvailable()
extern void CustomVungle_IsInterstitialAvailable_mC6DC043A03C68CCE7CAF3FF6E48FE97013685E0C (void);
// 0x000002C6 System.Boolean GleyMobileAds.CustomVungle::IsRewardVideoAvailable()
extern void CustomVungle_IsRewardVideoAvailable_m6D82CDB3D74DBC9C754236C0A4005D6BFAA17CD5 (void);
// 0x000002C7 System.Void GleyMobileAds.CustomVungle::ShowBanner(BannerPosition,BannerType,UnityEngine.Events.UnityAction`3<System.Boolean,BannerPosition,BannerType>)
extern void CustomVungle_ShowBanner_m0244A5BD9B0EC0BC1469A3D99BA4131093788AFA (void);
// 0x000002C8 System.Void GleyMobileAds.CustomVungle::ShowInterstitial(UnityEngine.Events.UnityAction)
extern void CustomVungle_ShowInterstitial_mB50AF8F6D184EACFDECFE293C1211022B9926094 (void);
// 0x000002C9 System.Void GleyMobileAds.CustomVungle::ShowInterstitial(UnityEngine.Events.UnityAction`1<System.String>)
extern void CustomVungle_ShowInterstitial_mF1C7FA63E5498F0FD5181F88B2B2E244FBD492C8 (void);
// 0x000002CA System.Void GleyMobileAds.CustomVungle::ShowRewardVideo(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void CustomVungle_ShowRewardVideo_mE785558EC0FA9153A5621AE68B0B5BBE13755AA7 (void);
// 0x000002CB System.Void GleyMobileAds.CustomVungle::ShowRewardVideo(UnityEngine.Events.UnityAction`2<System.Boolean,System.String>)
extern void CustomVungle_ShowRewardVideo_m73ECBFD7962E4F176AD18C3F4286F568601FFFA5 (void);
// 0x000002CC System.Void GleyMobileAds.CustomVungle::UpdateConsent(UserConsent,UserConsent)
extern void CustomVungle_UpdateConsent_m7930EBA34A2FEC778851DCFB9555EB3AEF7DF072 (void);
// 0x000002CD System.Void GleyMobileAds.CustomVungle::.ctor()
extern void CustomVungle__ctor_mAD2F0189FD486D9EEFA057B653822E27E1B40ECA (void);
// 0x000002CE System.Void GleyMobileAds.AdOrder::.ctor()
extern void AdOrder__ctor_m04AB006BBE638B915DB3B5D02BC1AB12757444D7 (void);
// 0x000002CF System.Void AudienceNetwork.AdHandler::ExecuteOnMainThread(System.Action)
extern void AdHandler_ExecuteOnMainThread_mB5A3D62F559F7722558B4D2910E78E82CEC9DD8D (void);
// 0x000002D0 System.Void AudienceNetwork.AdHandler::Update()
extern void AdHandler_Update_mDD20B10318EF0F298A2C38D074752307240935DB (void);
// 0x000002D1 System.Void AudienceNetwork.AdHandler::RemoveFromParent()
extern void AdHandler_RemoveFromParent_m4227A457574B9EED99906159CCCED8EA84A713EC (void);
// 0x000002D2 System.Void AudienceNetwork.AdHandler::.ctor()
extern void AdHandler__ctor_mEA69C635E80E5F942993D8EEE06C28CF1E4B9503 (void);
// 0x000002D3 System.Void AudienceNetwork.AdHandler::.cctor()
extern void AdHandler__cctor_mF1FB384195F295BB6CF7E37277844F40887505D8 (void);
// 0x000002D4 System.Void AudienceNetwork.AdSettings::SetUrlPrefix(System.String)
extern void AdSettings_SetUrlPrefix_mD9CD316170FECFE681B7A1A3AAC12D80FB173567 (void);
// 0x000002D5 System.Void AudienceNetwork.IAdSettingsBridge::SetUrlPrefix(System.String)
// 0x000002D6 System.Void AudienceNetwork.AdSettingsBridge::.ctor()
extern void AdSettingsBridge__ctor_m167DBFC3BF6DA8D8B7B689EEDCA061B9D46EFEF3 (void);
// 0x000002D7 System.Void AudienceNetwork.AdSettingsBridge::.cctor()
extern void AdSettingsBridge__cctor_m40590CBE42B65388696A19E86811CED2DD1284E9 (void);
// 0x000002D8 AudienceNetwork.IAdSettingsBridge AudienceNetwork.AdSettingsBridge::CreateInstance()
extern void AdSettingsBridge_CreateInstance_m65495959DF4344EA50B936DD51A9E3F70D6E46D3 (void);
// 0x000002D9 System.Void AudienceNetwork.AdSettingsBridge::SetUrlPrefix(System.String)
extern void AdSettingsBridge_SetUrlPrefix_m259B2820C3B9A4145140C7F48A2BE896A2E6DFA5 (void);
// 0x000002DA System.Void AudienceNetwork.AdSettingsBridgeAndroid::SetUrlPrefix(System.String)
extern void AdSettingsBridgeAndroid_SetUrlPrefix_m08FE29A152BE1723AC91501562A8B3CEF6D31AB1 (void);
// 0x000002DB UnityEngine.AndroidJavaClass AudienceNetwork.AdSettingsBridgeAndroid::GetAdSettingsObject()
extern void AdSettingsBridgeAndroid_GetAdSettingsObject_m3AE4E6BB1C80066D4E32FC15A90E2B881FFCFB9B (void);
// 0x000002DC System.Void AudienceNetwork.AdSettingsBridgeAndroid::.ctor()
extern void AdSettingsBridgeAndroid__ctor_m1F64478199F03FC0E03D4CEB1E0CC2B8A5FC5D4C (void);
// 0x000002DD System.Void AudienceNetwork.FBAdViewBridgeCallback::.ctor(System.Object,System.IntPtr)
extern void FBAdViewBridgeCallback__ctor_m6710F53F2806B18989E0D5E0735DC79AE7995603 (void);
// 0x000002DE System.Void AudienceNetwork.FBAdViewBridgeCallback::Invoke()
extern void FBAdViewBridgeCallback_Invoke_m6A30B46359E32849493AC52F3A4A28B5A18E62D7 (void);
// 0x000002DF System.Void AudienceNetwork.FBAdViewBridgeErrorCallback::.ctor(System.Object,System.IntPtr)
extern void FBAdViewBridgeErrorCallback__ctor_m10F4B984486537D850D8CE04A02A270BE7D461B8 (void);
// 0x000002E0 System.Void AudienceNetwork.FBAdViewBridgeErrorCallback::Invoke(System.String)
extern void FBAdViewBridgeErrorCallback_Invoke_mC305B139F7C2B563BD839A976F5226A7C6501C32 (void);
// 0x000002E1 System.String AudienceNetwork.AdView::get_PlacementId()
extern void AdView_get_PlacementId_mEDE23B31999A8FD01DED8BAEED1BE1928899E273 (void);
// 0x000002E2 System.Void AudienceNetwork.AdView::set_PlacementId(System.String)
extern void AdView_set_PlacementId_m19FCF6CE81E080B58DFC68AEAA56B98E5971432D (void);
// 0x000002E3 AudienceNetwork.FBAdViewBridgeCallback AudienceNetwork.AdView::get_AdViewDidLoad()
extern void AdView_get_AdViewDidLoad_m9A5FDB200522996BFA60437A71139BF6E8E3F925 (void);
// 0x000002E4 System.Void AudienceNetwork.AdView::set_AdViewDidLoad(AudienceNetwork.FBAdViewBridgeCallback)
extern void AdView_set_AdViewDidLoad_mBD127C9EF81C596B73E4C4343A062E2717FFDF98 (void);
// 0x000002E5 AudienceNetwork.FBAdViewBridgeCallback AudienceNetwork.AdView::get_AdViewWillLogImpression()
extern void AdView_get_AdViewWillLogImpression_mCFAF35C01F2ED04BAF6007FE5EB4D63FC0DF0B49 (void);
// 0x000002E6 System.Void AudienceNetwork.AdView::set_AdViewWillLogImpression(AudienceNetwork.FBAdViewBridgeCallback)
extern void AdView_set_AdViewWillLogImpression_mDE32879CEBEC50C7342EA1240CE30214B520C029 (void);
// 0x000002E7 AudienceNetwork.FBAdViewBridgeErrorCallback AudienceNetwork.AdView::get_AdViewDidFailWithError()
extern void AdView_get_AdViewDidFailWithError_m4B5A5230BF887E968F26F012F4D606BC94B17378 (void);
// 0x000002E8 System.Void AudienceNetwork.AdView::set_AdViewDidFailWithError(AudienceNetwork.FBAdViewBridgeErrorCallback)
extern void AdView_set_AdViewDidFailWithError_m30469B5CAE7CEE6B40B1B357FB2D3C92FAC5BC79 (void);
// 0x000002E9 AudienceNetwork.FBAdViewBridgeCallback AudienceNetwork.AdView::get_AdViewDidClick()
extern void AdView_get_AdViewDidClick_mE7E5F6BC71E6F33E7E321E3D463E18C009AD1609 (void);
// 0x000002EA System.Void AudienceNetwork.AdView::set_AdViewDidClick(AudienceNetwork.FBAdViewBridgeCallback)
extern void AdView_set_AdViewDidClick_mADFDE1A9F8C233CB8C15EDB76E0E5F2FC972EA42 (void);
// 0x000002EB AudienceNetwork.FBAdViewBridgeCallback AudienceNetwork.AdView::get_AdViewDidFinishClick()
extern void AdView_get_AdViewDidFinishClick_m9799B996F1564840DC99570DF3AAB4F75CCA28F5 (void);
// 0x000002EC System.Void AudienceNetwork.AdView::.ctor(System.String,AudienceNetwork.AdSize)
extern void AdView__ctor_m46F11F5DDA003405240B3AE57EE3553E38BD78E8 (void);
// 0x000002ED System.Void AudienceNetwork.AdView::Finalize()
extern void AdView_Finalize_m0426F0F3B7D9DE2A23CD69DCE7B93DCD12FA4F3F (void);
// 0x000002EE System.Void AudienceNetwork.AdView::Dispose()
extern void AdView_Dispose_m2463F1476326B80455743D0D1D5FF32842E51D7A (void);
// 0x000002EF System.Void AudienceNetwork.AdView::Dispose(System.Boolean)
extern void AdView_Dispose_m4776B74E40250D31A3475FD0F8C331BDF8CEF0B3 (void);
// 0x000002F0 System.String AudienceNetwork.AdView::ToString()
extern void AdView_ToString_m351063BB697CB0B749ADDEFEAC97965BDD668BC7 (void);
// 0x000002F1 System.Void AudienceNetwork.AdView::Register(UnityEngine.GameObject)
extern void AdView_Register_m0C63ACD6BBF4CD7724E1528D58B4680A932C03CC (void);
// 0x000002F2 System.Void AudienceNetwork.AdView::LoadAd()
extern void AdView_LoadAd_m166AC4D5EB49FF2061D7E0F06BAB11BFFB5C6006 (void);
// 0x000002F3 System.Boolean AudienceNetwork.AdView::IsValid()
extern void AdView_IsValid_m81A2AC325D900213AE176732B4321B7A485A21C4 (void);
// 0x000002F4 System.Void AudienceNetwork.AdView::LoadAdFromData()
extern void AdView_LoadAdFromData_m3E40E0C4ED9411A654EDFF5E386A1A8157F21CEB (void);
// 0x000002F5 System.Double AudienceNetwork.AdView::HeightFromType(AudienceNetwork.AdView,AudienceNetwork.AdSize)
extern void AdView_HeightFromType_m2662ACAE43FB8A763CFFFEA9F39785B51D24F80A (void);
// 0x000002F6 System.Boolean AudienceNetwork.AdView::Show(AudienceNetwork.AdPosition)
extern void AdView_Show_m9E4B30BA17205CBFCB1B6EDB84C35631E41FD63C (void);
// 0x000002F7 System.Boolean AudienceNetwork.AdView::Show(System.Double)
extern void AdView_Show_mB4616203352A95F93F7819ECF35848E35825F3E8 (void);
// 0x000002F8 System.Boolean AudienceNetwork.AdView::Show(System.Double,System.Double)
extern void AdView_Show_mCEC1DD5DC80D5D157DEDE4F076DB4F92BB39A4E1 (void);
// 0x000002F9 System.Boolean AudienceNetwork.AdView::Show(System.Double,System.Double,System.Double,System.Double)
extern void AdView_Show_m0CC5324E36CDF6938F768FB77DB1D6292C0DE36E (void);
// 0x000002FA System.Void AudienceNetwork.AdView::ExecuteOnMainThread(System.Action)
extern void AdView_ExecuteOnMainThread_m6F15C2D493BDBF814BA82FD3CE41916EE9CA7566 (void);
// 0x000002FB System.Boolean AudienceNetwork.AdView::op_Implicit(AudienceNetwork.AdView)
extern void AdView_op_Implicit_mC7FC0D167FBE703863652F01D91F2B06D52173AF (void);
// 0x000002FC System.Void AudienceNetwork.AdView::<LoadAdFromData>b__37_0()
extern void AdView_U3CLoadAdFromDataU3Eb__37_0_m94E18D515027B305FDD5C96270B407DCEF7F5400 (void);
// 0x000002FD System.Int32 AudienceNetwork.IAdViewBridge::Create(System.String,AudienceNetwork.AdView,AudienceNetwork.AdSize)
// 0x000002FE System.Int32 AudienceNetwork.IAdViewBridge::Load(System.Int32)
// 0x000002FF System.Boolean AudienceNetwork.IAdViewBridge::IsValid(System.Int32)
// 0x00000300 System.Boolean AudienceNetwork.IAdViewBridge::Show(System.Int32,System.Double,System.Double,System.Double,System.Double)
// 0x00000301 System.Void AudienceNetwork.IAdViewBridge::Release(System.Int32)
// 0x00000302 System.Void AudienceNetwork.IAdViewBridge::OnLoad(System.Int32,AudienceNetwork.FBAdViewBridgeCallback)
// 0x00000303 System.Void AudienceNetwork.IAdViewBridge::OnImpression(System.Int32,AudienceNetwork.FBAdViewBridgeCallback)
// 0x00000304 System.Void AudienceNetwork.IAdViewBridge::OnClick(System.Int32,AudienceNetwork.FBAdViewBridgeCallback)
// 0x00000305 System.Void AudienceNetwork.IAdViewBridge::OnError(System.Int32,AudienceNetwork.FBAdViewBridgeErrorCallback)
// 0x00000306 System.Void AudienceNetwork.IAdViewBridge::OnFinishedClick(System.Int32,AudienceNetwork.FBAdViewBridgeCallback)
// 0x00000307 System.Void AudienceNetwork.AdViewBridge::.ctor()
extern void AdViewBridge__ctor_mF0D5C16A14D9B72FE4F3CE7E31218A45E605C442 (void);
// 0x00000308 System.Void AudienceNetwork.AdViewBridge::.cctor()
extern void AdViewBridge__cctor_m109B52BBADD726870D53DD87168BAF7E9DF75290 (void);
// 0x00000309 AudienceNetwork.IAdViewBridge AudienceNetwork.AdViewBridge::CreateInstance()
extern void AdViewBridge_CreateInstance_m83986ED51584DF6E056B91340B99FF2CDAA12069 (void);
// 0x0000030A System.Int32 AudienceNetwork.AdViewBridge::Create(System.String,AudienceNetwork.AdView,AudienceNetwork.AdSize)
extern void AdViewBridge_Create_mBE61431F616EAA9DA324937C71165A8EC1AC2B2B (void);
// 0x0000030B System.Int32 AudienceNetwork.AdViewBridge::Load(System.Int32)
extern void AdViewBridge_Load_mC114B465F86C6BC492772BA5ACA9C87299293C45 (void);
// 0x0000030C System.Boolean AudienceNetwork.AdViewBridge::IsValid(System.Int32)
extern void AdViewBridge_IsValid_mDF146998DE98CD4B18D6930D8063976793770856 (void);
// 0x0000030D System.Boolean AudienceNetwork.AdViewBridge::Show(System.Int32,System.Double,System.Double,System.Double,System.Double)
extern void AdViewBridge_Show_m2E2D895F1FB4A89E8E5920452FFD3E8A94DB9F47 (void);
// 0x0000030E System.Void AudienceNetwork.AdViewBridge::Release(System.Int32)
extern void AdViewBridge_Release_mE35B55B75CB0043D0A530BD6ADD48C664531EDF9 (void);
// 0x0000030F System.Void AudienceNetwork.AdViewBridge::OnLoad(System.Int32,AudienceNetwork.FBAdViewBridgeCallback)
extern void AdViewBridge_OnLoad_mBEFFBA7157C7A769D0834103845DDD299346FF6F (void);
// 0x00000310 System.Void AudienceNetwork.AdViewBridge::OnImpression(System.Int32,AudienceNetwork.FBAdViewBridgeCallback)
extern void AdViewBridge_OnImpression_mDA0B1D87136001D4D906456A0E2B2BC7850D9D6F (void);
// 0x00000311 System.Void AudienceNetwork.AdViewBridge::OnClick(System.Int32,AudienceNetwork.FBAdViewBridgeCallback)
extern void AdViewBridge_OnClick_m9369BC976EC9ACF94608840DB0C7CF6D07559CBB (void);
// 0x00000312 System.Void AudienceNetwork.AdViewBridge::OnError(System.Int32,AudienceNetwork.FBAdViewBridgeErrorCallback)
extern void AdViewBridge_OnError_m473E7FF95AD73CE72A093117B4D5EE91F4D0CA33 (void);
// 0x00000313 System.Void AudienceNetwork.AdViewBridge::OnFinishedClick(System.Int32,AudienceNetwork.FBAdViewBridgeCallback)
extern void AdViewBridge_OnFinishedClick_m4BEDC9B638BBE0233F9811CE666D4660CC679582 (void);
// 0x00000314 UnityEngine.AndroidJavaObject AudienceNetwork.AdViewBridgeAndroid::AdViewForAdViewId(System.Int32)
extern void AdViewBridgeAndroid_AdViewForAdViewId_m8ACD675C88EE8642600572F8E6A54262C746424F (void);
// 0x00000315 AudienceNetwork.AdViewContainer AudienceNetwork.AdViewBridgeAndroid::AdViewContainerForAdViewId(System.Int32)
extern void AdViewBridgeAndroid_AdViewContainerForAdViewId_mA318B16474C653E7840AA9BE36F349E1877075A3 (void);
// 0x00000316 UnityEngine.AndroidJavaObject AudienceNetwork.AdViewBridgeAndroid::JavaAdSizeFromAdSize(AudienceNetwork.AdSize)
extern void AdViewBridgeAndroid_JavaAdSizeFromAdSize_m5D1E1136009966DC3C7DB749322570854040D4A1 (void);
// 0x00000317 System.Int32 AudienceNetwork.AdViewBridgeAndroid::Create(System.String,AudienceNetwork.AdView,AudienceNetwork.AdSize)
extern void AdViewBridgeAndroid_Create_mF082BBD9880B87D613A142AB0249FD4F162E3623 (void);
// 0x00000318 System.Int32 AudienceNetwork.AdViewBridgeAndroid::Load(System.Int32)
extern void AdViewBridgeAndroid_Load_m1A96128BD57C5772C2DF5F814BF7C60850EF501F (void);
// 0x00000319 System.Boolean AudienceNetwork.AdViewBridgeAndroid::IsValid(System.Int32)
extern void AdViewBridgeAndroid_IsValid_mBE070BD4898B6C0E4E5E640C11EEC6D7D1BB84DF (void);
// 0x0000031A System.Boolean AudienceNetwork.AdViewBridgeAndroid::Show(System.Int32,System.Double,System.Double,System.Double,System.Double)
extern void AdViewBridgeAndroid_Show_m0A1B2A36285BFD26C2C53D2AC1B5B0CD577A16E3 (void);
// 0x0000031B System.Void AudienceNetwork.AdViewBridgeAndroid::Release(System.Int32)
extern void AdViewBridgeAndroid_Release_m40CB2C4825728AAD170561F551E9455F09D6D998 (void);
// 0x0000031C System.Void AudienceNetwork.AdViewBridgeAndroid::OnLoad(System.Int32,AudienceNetwork.FBAdViewBridgeCallback)
extern void AdViewBridgeAndroid_OnLoad_m8EE502E51A5F7F549BE87624B2C53AC0DA6F9AB4 (void);
// 0x0000031D System.Void AudienceNetwork.AdViewBridgeAndroid::OnImpression(System.Int32,AudienceNetwork.FBAdViewBridgeCallback)
extern void AdViewBridgeAndroid_OnImpression_mC06C1F6D3CB48795CB184D48BEAD0F30A5DCE693 (void);
// 0x0000031E System.Void AudienceNetwork.AdViewBridgeAndroid::OnClick(System.Int32,AudienceNetwork.FBAdViewBridgeCallback)
extern void AdViewBridgeAndroid_OnClick_m4335F566FD7AE7296A734253ADC6966CF441AD4D (void);
// 0x0000031F System.Void AudienceNetwork.AdViewBridgeAndroid::OnError(System.Int32,AudienceNetwork.FBAdViewBridgeErrorCallback)
extern void AdViewBridgeAndroid_OnError_mFAC616016446C6CA08C83C973E000F358CE4996E (void);
// 0x00000320 System.Void AudienceNetwork.AdViewBridgeAndroid::OnFinishedClick(System.Int32,AudienceNetwork.FBAdViewBridgeCallback)
extern void AdViewBridgeAndroid_OnFinishedClick_m5AD411FAF8BD4AA0D5674A8351FF3857BF32C65F (void);
// 0x00000321 System.Void AudienceNetwork.AdViewBridgeAndroid::.ctor()
extern void AdViewBridgeAndroid__ctor_m482A854667EA4A6C7679DDD616D17233CECE48D5 (void);
// 0x00000322 System.Void AudienceNetwork.AdViewBridgeAndroid::.cctor()
extern void AdViewBridgeAndroid__cctor_m8082021F23CD0DBEC60DCD65B0118B3F597177EB (void);
// 0x00000323 System.Void AudienceNetwork.AdViewBridgeAndroid/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mE2E09395E81909A176BFD52A868620E528B36698 (void);
// 0x00000324 System.Void AudienceNetwork.AdViewBridgeAndroid/<>c__DisplayClass11_0::<Show>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CShowU3Eb__0_mCC1227BB138DFBC93108B9F6B47320CE88F2A6DE (void);
// 0x00000325 System.Void AudienceNetwork.AdViewBridgeAndroid/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m53AF55B5CCB0DE1EAB0B968BA395EDEE81F5A8D6 (void);
// 0x00000326 System.Void AudienceNetwork.AdViewBridgeAndroid/<>c__DisplayClass13_0::<Release>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CReleaseU3Eb__0_mAFAD6BBF32E46A39AD0D8272BE87CB5FDC3C36A1 (void);
// 0x00000327 AudienceNetwork.AdView AudienceNetwork.AdViewContainer::get_adView()
extern void AdViewContainer_get_adView_mF173B99D00FDF979B8E88AEB2C89EB66BC63EA0B (void);
// 0x00000328 System.Void AudienceNetwork.AdViewContainer::set_adView(AudienceNetwork.AdView)
extern void AdViewContainer_set_adView_m4C8C90B38B7BEFB20372EA3B62CED435E889274C (void);
// 0x00000329 AudienceNetwork.FBAdViewBridgeCallback AudienceNetwork.AdViewContainer::get_onLoad()
extern void AdViewContainer_get_onLoad_mAD329E912D44818B5EE355E4D6349D6C84725336 (void);
// 0x0000032A System.Void AudienceNetwork.AdViewContainer::.ctor(AudienceNetwork.AdView)
extern void AdViewContainer__ctor_m83E84C60F2CA567DABD05A755594BDC95F5D2AF3 (void);
// 0x0000032B System.String AudienceNetwork.AdViewContainer::ToString()
extern void AdViewContainer_ToString_mAD9C8EB6E1AB3F7C3C09FCCE8A5F04209BB77BCA (void);
// 0x0000032C UnityEngine.AndroidJavaObject AudienceNetwork.AdViewContainer::LoadAdConfig(System.String)
extern void AdViewContainer_LoadAdConfig_m39A628320358AAABC53BEFA609545A1C41571CD5 (void);
// 0x0000032D System.Void AudienceNetwork.AdViewContainer::Load()
extern void AdViewContainer_Load_m21EC7E647E95F8A1345E0DE395C0A06508AC4738 (void);
// 0x0000032E System.Void AudienceNetwork.AdViewContainer::Load(System.String)
extern void AdViewContainer_Load_mF388040489C124FACB014FAD493FD575250D19EF (void);
// 0x0000032F System.Void AudienceNetwork.AdViewBridgeListenerProxy::.ctor(AudienceNetwork.AdView,UnityEngine.AndroidJavaObject)
extern void AdViewBridgeListenerProxy__ctor_mCC5976FF4B692779997B74F01F4388BAC1D74223 (void);
// 0x00000330 System.Void AudienceNetwork.AdViewBridgeListenerProxy::onError(UnityEngine.AndroidJavaObject,UnityEngine.AndroidJavaObject)
extern void AdViewBridgeListenerProxy_onError_m441EB8D3ECFDCEF7271843C0BC14E32B501B58B0 (void);
// 0x00000331 System.Void AudienceNetwork.AdViewBridgeListenerProxy::onAdLoaded(UnityEngine.AndroidJavaObject)
extern void AdViewBridgeListenerProxy_onAdLoaded_m21CD45E711594099143EF260399275F9FD81898E (void);
// 0x00000332 System.Void AudienceNetwork.AdViewBridgeListenerProxy::onAdClicked(UnityEngine.AndroidJavaObject)
extern void AdViewBridgeListenerProxy_onAdClicked_m739335F8101138FC8974A777BBE2B31126173E82 (void);
// 0x00000333 System.Void AudienceNetwork.AdViewBridgeListenerProxy::onLoggingImpression(UnityEngine.AndroidJavaObject)
extern void AdViewBridgeListenerProxy_onLoggingImpression_mA6C018DC458CAF831EFC20473236B11D71A2E134 (void);
// 0x00000334 System.Void AudienceNetwork.AdViewBridgeListenerProxy::<onAdClicked>b__5_0()
extern void AdViewBridgeListenerProxy_U3ConAdClickedU3Eb__5_0_m1FE6C08A128953255E5D837BD36E48718BAB0C40 (void);
// 0x00000335 System.Void AudienceNetwork.AdViewBridgeListenerProxy::<onLoggingImpression>b__6_0()
extern void AdViewBridgeListenerProxy_U3ConLoggingImpressionU3Eb__6_0_mFEA2510D1C0596F697A36A49664843F234A4B40D (void);
// 0x00000336 System.Void AudienceNetwork.AdViewBridgeListenerProxy/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m4CB6FC4ABA853AF2D3B10279925AF211EC5D4788 (void);
// 0x00000337 System.Void AudienceNetwork.AdViewBridgeListenerProxy/<>c__DisplayClass3_0::<onError>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3ConErrorU3Eb__0_m510A4AD2002C4E8DB1DE203EF84FC86F111AE154 (void);
// 0x00000338 System.Void AudienceNetwork.AudienceNetworkAds::Initialize()
extern void AudienceNetworkAds_Initialize_m9B76FC498D44CE550EDBB6075706D138C7B14B5F (void);
// 0x00000339 System.Boolean AudienceNetwork.AudienceNetworkAds::IsInitialized()
extern void AudienceNetworkAds_IsInitialized_m3ABD939BB17990224EA7DEB8AA847914D48AD23B (void);
// 0x0000033A System.Void AudienceNetwork.FBInterstitialAdBridgeCallback::.ctor(System.Object,System.IntPtr)
extern void FBInterstitialAdBridgeCallback__ctor_m84E5805C989B36CE9DD6B598491602D9BFE031CA (void);
// 0x0000033B System.Void AudienceNetwork.FBInterstitialAdBridgeCallback::Invoke()
extern void FBInterstitialAdBridgeCallback_Invoke_m3F30ABD6169AB25047BB628CE40340B13014D65F (void);
// 0x0000033C System.Void AudienceNetwork.FBInterstitialAdBridgeErrorCallback::.ctor(System.Object,System.IntPtr)
extern void FBInterstitialAdBridgeErrorCallback__ctor_mB1F2197CB65BE16BBBE9E2D47CF0A606E4BB8A18 (void);
// 0x0000033D System.Void AudienceNetwork.FBInterstitialAdBridgeErrorCallback::Invoke(System.String)
extern void FBInterstitialAdBridgeErrorCallback_Invoke_m81FC8F00C6229EBBDDB5FD87D85F838113E2B2BB (void);
// 0x0000033E System.String AudienceNetwork.InterstitialAd::get_PlacementId()
extern void InterstitialAd_get_PlacementId_mC1BD108C594367099DEE08B1B85529E84329B117 (void);
// 0x0000033F System.Void AudienceNetwork.InterstitialAd::set_PlacementId(System.String)
extern void InterstitialAd_set_PlacementId_m491B0D3ABB52877889D50EFAAE0683879429540E (void);
// 0x00000340 AudienceNetwork.FBInterstitialAdBridgeCallback AudienceNetwork.InterstitialAd::get_InterstitialAdDidLoad()
extern void InterstitialAd_get_InterstitialAdDidLoad_mE9EA30A35E2D33E084214A94E99A7C18EB90851B (void);
// 0x00000341 System.Void AudienceNetwork.InterstitialAd::set_InterstitialAdDidLoad(AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAd_set_InterstitialAdDidLoad_m706CF90FCC23551C07FC0E26701E04D461D805B2 (void);
// 0x00000342 AudienceNetwork.FBInterstitialAdBridgeCallback AudienceNetwork.InterstitialAd::get_InterstitialAdWillLogImpression()
extern void InterstitialAd_get_InterstitialAdWillLogImpression_mBE1AF8E676EF850F9E86A8ED2309E5B554E0A220 (void);
// 0x00000343 System.Void AudienceNetwork.InterstitialAd::set_InterstitialAdWillLogImpression(AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAd_set_InterstitialAdWillLogImpression_mE819096A17363F18315886DFA335AC957CEDEF41 (void);
// 0x00000344 AudienceNetwork.FBInterstitialAdBridgeErrorCallback AudienceNetwork.InterstitialAd::get_InterstitialAdDidFailWithError()
extern void InterstitialAd_get_InterstitialAdDidFailWithError_m86A66F13A9B01022C830DAB214674B9FF1F17FB1 (void);
// 0x00000345 System.Void AudienceNetwork.InterstitialAd::set_InterstitialAdDidFailWithError(AudienceNetwork.FBInterstitialAdBridgeErrorCallback)
extern void InterstitialAd_set_InterstitialAdDidFailWithError_m4B8F0E5A1FD4DAEE4C3DA8F8E544254ED24A681C (void);
// 0x00000346 AudienceNetwork.FBInterstitialAdBridgeCallback AudienceNetwork.InterstitialAd::get_InterstitialAdDidClick()
extern void InterstitialAd_get_InterstitialAdDidClick_mC16B274285D14D412293CA12B88E1B343383108C (void);
// 0x00000347 System.Void AudienceNetwork.InterstitialAd::set_InterstitialAdDidClick(AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAd_set_InterstitialAdDidClick_mC049945613B2B200B4892B3DA13AD219F7A0328E (void);
// 0x00000348 AudienceNetwork.FBInterstitialAdBridgeCallback AudienceNetwork.InterstitialAd::get_InterstitialAdWillClose()
extern void InterstitialAd_get_InterstitialAdWillClose_mEF531764761C134455DD517EDC10954BB3CC8901 (void);
// 0x00000349 AudienceNetwork.FBInterstitialAdBridgeCallback AudienceNetwork.InterstitialAd::get_InterstitialAdDidClose()
extern void InterstitialAd_get_InterstitialAdDidClose_mEA11D8098A581E106A47BBE022220321CB0263E6 (void);
// 0x0000034A System.Void AudienceNetwork.InterstitialAd::set_InterstitialAdDidClose(AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAd_set_InterstitialAdDidClose_m3FA1CA4A93710E641DB8D4B3D26035063FCDAA1E (void);
// 0x0000034B AudienceNetwork.FBInterstitialAdBridgeCallback AudienceNetwork.InterstitialAd::get_InterstitialAdActivityDestroyed()
extern void InterstitialAd_get_InterstitialAdActivityDestroyed_mBFD62998A660D42427F60CF4076E879C5808AAF5 (void);
// 0x0000034C System.Void AudienceNetwork.InterstitialAd::.ctor(System.String)
extern void InterstitialAd__ctor_m239D8B61321594216EC608E2153460629BB910BA (void);
// 0x0000034D System.Void AudienceNetwork.InterstitialAd::Finalize()
extern void InterstitialAd_Finalize_m0A6235FC25EE5880D347D61CF8B54799CAE772E6 (void);
// 0x0000034E System.Void AudienceNetwork.InterstitialAd::Dispose()
extern void InterstitialAd_Dispose_m0B61AAA5708A59E9B498AF26E7BCCD4587972527 (void);
// 0x0000034F System.Void AudienceNetwork.InterstitialAd::Dispose(System.Boolean)
extern void InterstitialAd_Dispose_mC77C628A99DB1417B1CB91655AED7770717E5DF8 (void);
// 0x00000350 System.String AudienceNetwork.InterstitialAd::ToString()
extern void InterstitialAd_ToString_m55AD308886B1FAF885A89F7AB978603FBA5B5C65 (void);
// 0x00000351 System.Void AudienceNetwork.InterstitialAd::Register(UnityEngine.GameObject)
extern void InterstitialAd_Register_m5939306629CA5D011A779590EB1DDC47CCD3F236 (void);
// 0x00000352 System.Void AudienceNetwork.InterstitialAd::LoadAd()
extern void InterstitialAd_LoadAd_m7A1B717DD13EFB6544FD95F50189D20DD02D6741 (void);
// 0x00000353 System.Boolean AudienceNetwork.InterstitialAd::IsValid()
extern void InterstitialAd_IsValid_mD7DB971066F9990CF7EB1585AB873946A28D4BB3 (void);
// 0x00000354 System.Void AudienceNetwork.InterstitialAd::LoadAdFromData()
extern void InterstitialAd_LoadAdFromData_m84B2424DDD5CB988DDCEA3189DD929D9E44C2D65 (void);
// 0x00000355 System.Boolean AudienceNetwork.InterstitialAd::Show()
extern void InterstitialAd_Show_m08B5B3AB2F4780C75437CA412B69D77C573C97BB (void);
// 0x00000356 System.Void AudienceNetwork.InterstitialAd::ExecuteOnMainThread(System.Action)
extern void InterstitialAd_ExecuteOnMainThread_m21D0D65976643939F72D64B312D41765BFCD8F32 (void);
// 0x00000357 System.Void AudienceNetwork.InterstitialAd::<LoadAdFromData>b__44_0()
extern void InterstitialAd_U3CLoadAdFromDataU3Eb__44_0_m127FF710F7CA3739591A73D693B2DB3516D7F0CE (void);
// 0x00000358 System.Int32 AudienceNetwork.IInterstitialAdBridge::Create(System.String,AudienceNetwork.InterstitialAd)
// 0x00000359 System.Int32 AudienceNetwork.IInterstitialAdBridge::Load(System.Int32)
// 0x0000035A System.Boolean AudienceNetwork.IInterstitialAdBridge::IsValid(System.Int32)
// 0x0000035B System.Boolean AudienceNetwork.IInterstitialAdBridge::Show(System.Int32)
// 0x0000035C System.Void AudienceNetwork.IInterstitialAdBridge::Release(System.Int32)
// 0x0000035D System.Void AudienceNetwork.IInterstitialAdBridge::OnLoad(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
// 0x0000035E System.Void AudienceNetwork.IInterstitialAdBridge::OnImpression(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
// 0x0000035F System.Void AudienceNetwork.IInterstitialAdBridge::OnClick(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
// 0x00000360 System.Void AudienceNetwork.IInterstitialAdBridge::OnError(System.Int32,AudienceNetwork.FBInterstitialAdBridgeErrorCallback)
// 0x00000361 System.Void AudienceNetwork.IInterstitialAdBridge::OnWillClose(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
// 0x00000362 System.Void AudienceNetwork.IInterstitialAdBridge::OnDidClose(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
// 0x00000363 System.Void AudienceNetwork.IInterstitialAdBridge::OnActivityDestroyed(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
// 0x00000364 System.Void AudienceNetwork.InterstitialAdBridge::.ctor()
extern void InterstitialAdBridge__ctor_mC4498DE9D762B281CB4E125020358406C4A5F015 (void);
// 0x00000365 System.Void AudienceNetwork.InterstitialAdBridge::.cctor()
extern void InterstitialAdBridge__cctor_m1DEB8E74E51311FDED5F46D39A73B4E20A2A7226 (void);
// 0x00000366 AudienceNetwork.IInterstitialAdBridge AudienceNetwork.InterstitialAdBridge::CreateInstance()
extern void InterstitialAdBridge_CreateInstance_m5C1686CB90CC41687C9D68C4022701612B45E45A (void);
// 0x00000367 System.Int32 AudienceNetwork.InterstitialAdBridge::Create(System.String,AudienceNetwork.InterstitialAd)
extern void InterstitialAdBridge_Create_mB13EE8844678509CEC60991B4A0CC5916537B05D (void);
// 0x00000368 System.Int32 AudienceNetwork.InterstitialAdBridge::Load(System.Int32)
extern void InterstitialAdBridge_Load_mD58B08578D4CE4FB477F210CDE28CE2160D30816 (void);
// 0x00000369 System.Boolean AudienceNetwork.InterstitialAdBridge::IsValid(System.Int32)
extern void InterstitialAdBridge_IsValid_m376B6FA7D6EE4FCCE7136AEE402A30A3035F4EF9 (void);
// 0x0000036A System.Boolean AudienceNetwork.InterstitialAdBridge::Show(System.Int32)
extern void InterstitialAdBridge_Show_m8F5CB88811F6CD9816B890B36A49230A18FAB4A1 (void);
// 0x0000036B System.Void AudienceNetwork.InterstitialAdBridge::Release(System.Int32)
extern void InterstitialAdBridge_Release_mBB6613D132F7693E9D1F631653990BEAF33526DA (void);
// 0x0000036C System.Void AudienceNetwork.InterstitialAdBridge::OnLoad(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAdBridge_OnLoad_m14B88CE3C1597EC88B58DA0FCD4148ADC4E76974 (void);
// 0x0000036D System.Void AudienceNetwork.InterstitialAdBridge::OnImpression(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAdBridge_OnImpression_m73C004F34821C8851FA924D55DEEC2643839E4DE (void);
// 0x0000036E System.Void AudienceNetwork.InterstitialAdBridge::OnClick(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAdBridge_OnClick_m2E42D22BEBB8956D3CACE868C631AD30D4E162F5 (void);
// 0x0000036F System.Void AudienceNetwork.InterstitialAdBridge::OnError(System.Int32,AudienceNetwork.FBInterstitialAdBridgeErrorCallback)
extern void InterstitialAdBridge_OnError_mF54A5BFC398CE066156A504412926F3784FC018B (void);
// 0x00000370 System.Void AudienceNetwork.InterstitialAdBridge::OnWillClose(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAdBridge_OnWillClose_mDD527746FC96D0751AC35EB0111558F557A9794A (void);
// 0x00000371 System.Void AudienceNetwork.InterstitialAdBridge::OnDidClose(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAdBridge_OnDidClose_mE44E38CCCE7BCEDF029AC0F04223EEFD3B02E06C (void);
// 0x00000372 System.Void AudienceNetwork.InterstitialAdBridge::OnActivityDestroyed(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAdBridge_OnActivityDestroyed_m9E46A949BCFB65E9CE0C158C003C2000F807C004 (void);
// 0x00000373 UnityEngine.AndroidJavaObject AudienceNetwork.InterstitialAdBridgeAndroid::InterstitialAdForuniqueId(System.Int32)
extern void InterstitialAdBridgeAndroid_InterstitialAdForuniqueId_m4C0EDD7D66BF62FE6F8881856C8323766560065F (void);
// 0x00000374 AudienceNetwork.InterstitialAdContainer AudienceNetwork.InterstitialAdBridgeAndroid::InterstitialAdContainerForuniqueId(System.Int32)
extern void InterstitialAdBridgeAndroid_InterstitialAdContainerForuniqueId_mEB1098B0DF87D2B1D829F6CCA1248A2A4CA353AE (void);
// 0x00000375 System.Int32 AudienceNetwork.InterstitialAdBridgeAndroid::Create(System.String,AudienceNetwork.InterstitialAd)
extern void InterstitialAdBridgeAndroid_Create_m8A0A62DD5E860E210F84D9C1805D1369524ABF29 (void);
// 0x00000376 System.Int32 AudienceNetwork.InterstitialAdBridgeAndroid::Load(System.Int32)
extern void InterstitialAdBridgeAndroid_Load_mBEBA7BBEA3CC3AFC1579B6319F87212F7EFDC168 (void);
// 0x00000377 System.Boolean AudienceNetwork.InterstitialAdBridgeAndroid::IsValid(System.Int32)
extern void InterstitialAdBridgeAndroid_IsValid_m04130F2CC2E2236D36839C889B00A8E3B6B55436 (void);
// 0x00000378 System.Boolean AudienceNetwork.InterstitialAdBridgeAndroid::Show(System.Int32)
extern void InterstitialAdBridgeAndroid_Show_m082052D33508BAE200EBCBAB9F0C3D898EA13700 (void);
// 0x00000379 System.Void AudienceNetwork.InterstitialAdBridgeAndroid::Release(System.Int32)
extern void InterstitialAdBridgeAndroid_Release_m40073243FA382356D5FC0A690E93E1102B9CA8D6 (void);
// 0x0000037A System.Void AudienceNetwork.InterstitialAdBridgeAndroid::OnLoad(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAdBridgeAndroid_OnLoad_m6A880B4E952604B2632E219A4E78F29ACDA9C464 (void);
// 0x0000037B System.Void AudienceNetwork.InterstitialAdBridgeAndroid::OnImpression(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAdBridgeAndroid_OnImpression_m0B98F118C95DF9A2C9EE8A4B6EA8492AB743E8A7 (void);
// 0x0000037C System.Void AudienceNetwork.InterstitialAdBridgeAndroid::OnClick(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAdBridgeAndroid_OnClick_mE27AB3D9F8840EEA2ACA4D817575594BA7020AE1 (void);
// 0x0000037D System.Void AudienceNetwork.InterstitialAdBridgeAndroid::OnError(System.Int32,AudienceNetwork.FBInterstitialAdBridgeErrorCallback)
extern void InterstitialAdBridgeAndroid_OnError_m81E40982D673123A5534736FBD6F14C0570064DC (void);
// 0x0000037E System.Void AudienceNetwork.InterstitialAdBridgeAndroid::OnWillClose(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAdBridgeAndroid_OnWillClose_mCAD0EFE2DBF5D9A996EA2E8E9ABD8B8F7D9B2E1E (void);
// 0x0000037F System.Void AudienceNetwork.InterstitialAdBridgeAndroid::OnDidClose(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAdBridgeAndroid_OnDidClose_mF1A238E668B7F580C5D3D7EE7AF825E3112EB278 (void);
// 0x00000380 System.Void AudienceNetwork.InterstitialAdBridgeAndroid::OnActivityDestroyed(System.Int32,AudienceNetwork.FBInterstitialAdBridgeCallback)
extern void InterstitialAdBridgeAndroid_OnActivityDestroyed_mEB7032270E5ABB4C288C5BDE5B979D9384C1FEB8 (void);
// 0x00000381 System.Void AudienceNetwork.InterstitialAdBridgeAndroid::.ctor()
extern void InterstitialAdBridgeAndroid__ctor_m460A72309A83E7AD2E9E5C2346B5C3B9A4DCABB3 (void);
// 0x00000382 System.Void AudienceNetwork.InterstitialAdBridgeAndroid::.cctor()
extern void InterstitialAdBridgeAndroid__cctor_m079828A9BAF1FDC2B67EF8DD6FE4A7C2A03EFA3D (void);
// 0x00000383 AudienceNetwork.InterstitialAd AudienceNetwork.InterstitialAdContainer::get_interstitialAd()
extern void InterstitialAdContainer_get_interstitialAd_mB67B6A0B101A0824B95B32D247601845F4109678 (void);
// 0x00000384 System.Void AudienceNetwork.InterstitialAdContainer::set_interstitialAd(AudienceNetwork.InterstitialAd)
extern void InterstitialAdContainer_set_interstitialAd_mA2B697E08CAAECD4C448BC564225A11354D9A0B1 (void);
// 0x00000385 AudienceNetwork.FBInterstitialAdBridgeCallback AudienceNetwork.InterstitialAdContainer::get_onLoad()
extern void InterstitialAdContainer_get_onLoad_m9F93DDB54D9899ED455FF641B4E04DBCBA6D9070 (void);
// 0x00000386 System.Void AudienceNetwork.InterstitialAdContainer::.ctor(AudienceNetwork.InterstitialAd)
extern void InterstitialAdContainer__ctor_m9285578CE597222E697741975B7A78DADCE265FF (void);
// 0x00000387 System.String AudienceNetwork.InterstitialAdContainer::ToString()
extern void InterstitialAdContainer_ToString_m3265D23A780A4C5D2149C78A12771DB437BC7A3A (void);
// 0x00000388 UnityEngine.AndroidJavaObject AudienceNetwork.InterstitialAdContainer::LoadAdConfig(System.String)
extern void InterstitialAdContainer_LoadAdConfig_mB1749ED116522293AE6727F5938E9865754B7C74 (void);
// 0x00000389 System.Void AudienceNetwork.InterstitialAdContainer::Load()
extern void InterstitialAdContainer_Load_m4E79DDE1F3DE129C1DFDAEE9A1A2127033A59780 (void);
// 0x0000038A System.Void AudienceNetwork.InterstitialAdContainer::Load(System.String)
extern void InterstitialAdContainer_Load_m9655CA8FE8FFE6BD73186FDAC2006815E7431748 (void);
// 0x0000038B System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy::.ctor(AudienceNetwork.InterstitialAd,UnityEngine.AndroidJavaObject)
extern void InterstitialAdBridgeListenerProxy__ctor_mEDC5994E2FB87877269C18A90F77A557B8E49A12 (void);
// 0x0000038C System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy::onError(UnityEngine.AndroidJavaObject,UnityEngine.AndroidJavaObject)
extern void InterstitialAdBridgeListenerProxy_onError_m3CDEC9A21F6B6174C929A858D01AA096185BA32B (void);
// 0x0000038D System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy::onAdLoaded(UnityEngine.AndroidJavaObject)
extern void InterstitialAdBridgeListenerProxy_onAdLoaded_m841EE1259F87047344A939CCAA0FB7C68A372C81 (void);
// 0x0000038E System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy::onAdClicked(UnityEngine.AndroidJavaObject)
extern void InterstitialAdBridgeListenerProxy_onAdClicked_m5B392F3E55F4267EDA75726A9D4483E49AAF2B44 (void);
// 0x0000038F System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy::onInterstitialDisplayed(UnityEngine.AndroidJavaObject)
extern void InterstitialAdBridgeListenerProxy_onInterstitialDisplayed_m739428DD4A06C7B90C9D69C8036D128A24F1D9E8 (void);
// 0x00000390 System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy::onInterstitialDismissed(UnityEngine.AndroidJavaObject)
extern void InterstitialAdBridgeListenerProxy_onInterstitialDismissed_m519B89D262E6C05A62369465C607A0138881C419 (void);
// 0x00000391 System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy::onLoggingImpression(UnityEngine.AndroidJavaObject)
extern void InterstitialAdBridgeListenerProxy_onLoggingImpression_m805133697A706BA21FDA0C87F4B1BFC15E976C8B (void);
// 0x00000392 System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy::onInterstitialActivityDestroyed()
extern void InterstitialAdBridgeListenerProxy_onInterstitialActivityDestroyed_mC047D9671AA86089E66372F8757EAD02B7DE602C (void);
// 0x00000393 System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy::<onAdClicked>b__5_0()
extern void InterstitialAdBridgeListenerProxy_U3ConAdClickedU3Eb__5_0_m581ABB927E5DEB5F0A4D7C4E0A1AAC9D525311A0 (void);
// 0x00000394 System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy::<onInterstitialDismissed>b__7_0()
extern void InterstitialAdBridgeListenerProxy_U3ConInterstitialDismissedU3Eb__7_0_m92362D7DFDDFA1FDAF75A331EEF152E6BCD47D51 (void);
// 0x00000395 System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy::<onLoggingImpression>b__8_0()
extern void InterstitialAdBridgeListenerProxy_U3ConLoggingImpressionU3Eb__8_0_mAE0853000EAF6AFC85807386F86E689EE11D03FA (void);
// 0x00000396 System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy::<onInterstitialActivityDestroyed>b__9_0()
extern void InterstitialAdBridgeListenerProxy_U3ConInterstitialActivityDestroyedU3Eb__9_0_m0BAF6003F12E42B6CC2089FE97DF1D551BB77009 (void);
// 0x00000397 System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m17A373D4CDD9E87FE5DDB3A91CDAFC85D8D01F3A (void);
// 0x00000398 System.Void AudienceNetwork.InterstitialAdBridgeListenerProxy/<>c__DisplayClass3_0::<onError>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3ConErrorU3Eb__0_mEC73CBD766584959C3C64646C811F7A0DFA36DF3 (void);
// 0x00000399 System.Void AudienceNetwork.FBRewardedVideoAdBridgeCallback::.ctor(System.Object,System.IntPtr)
extern void FBRewardedVideoAdBridgeCallback__ctor_mE29E98F4491DEE39534279F45BFD015E402E2CBB (void);
// 0x0000039A System.Void AudienceNetwork.FBRewardedVideoAdBridgeCallback::Invoke()
extern void FBRewardedVideoAdBridgeCallback_Invoke_m6C0DF3940C44A4CB144ACFEBEB2E7E1CC8AB3ACA (void);
// 0x0000039B System.Void AudienceNetwork.FBRewardedVideoAdBridgeErrorCallback::.ctor(System.Object,System.IntPtr)
extern void FBRewardedVideoAdBridgeErrorCallback__ctor_m2EB7F01D53D11BEA1A25DE429C9CDE833A73DB90 (void);
// 0x0000039C System.Void AudienceNetwork.FBRewardedVideoAdBridgeErrorCallback::Invoke(System.String)
extern void FBRewardedVideoAdBridgeErrorCallback_Invoke_m0E61ADCF56E4F80B6587C6F89A1D4A84A7865633 (void);
// 0x0000039D System.String AudienceNetwork.RewardData::get_UserId()
extern void RewardData_get_UserId_m65A779477569BCD25E48431426139A5D57D589D7 (void);
// 0x0000039E System.Void AudienceNetwork.RewardData::set_UserId(System.String)
extern void RewardData_set_UserId_m227738AF39EDD2099BC7ADFE9465F7F2547AF807 (void);
// 0x0000039F System.String AudienceNetwork.RewardData::get_Currency()
extern void RewardData_get_Currency_m33F70966C3056F904ACF66B7C5C4AE0222415C28 (void);
// 0x000003A0 System.Void AudienceNetwork.RewardData::set_Currency(System.String)
extern void RewardData_set_Currency_m724A4AC8CB31CDEFB23662AC67293212EC5D87DE (void);
// 0x000003A1 System.Void AudienceNetwork.RewardData::.ctor()
extern void RewardData__ctor_mC1FD98C75AAFF6E5A223B254D728B5FEA532B79A (void);
// 0x000003A2 System.String AudienceNetwork.RewardedVideoAd::get_PlacementId()
extern void RewardedVideoAd_get_PlacementId_m79203F67638150A9462933CDF9BA5BF5A96BA4D4 (void);
// 0x000003A3 System.Void AudienceNetwork.RewardedVideoAd::set_PlacementId(System.String)
extern void RewardedVideoAd_set_PlacementId_m53135355B631289F71D0A0F219FD33C05D8C9651 (void);
// 0x000003A4 AudienceNetwork.RewardData AudienceNetwork.RewardedVideoAd::get_RewardData()
extern void RewardedVideoAd_get_RewardData_mF5E3C31190ECDF4033CBD22CFDA4371B733CCF31 (void);
// 0x000003A5 System.Void AudienceNetwork.RewardedVideoAd::set_RewardData(AudienceNetwork.RewardData)
extern void RewardedVideoAd_set_RewardData_mEF09A8875685C2406BF78450FB1422AB837C5556 (void);
// 0x000003A6 AudienceNetwork.FBRewardedVideoAdBridgeCallback AudienceNetwork.RewardedVideoAd::get_RewardedVideoAdDidLoad()
extern void RewardedVideoAd_get_RewardedVideoAdDidLoad_m14DFCD4C4DCBFEDB7874AF9ECED60A0E6C5936F7 (void);
// 0x000003A7 System.Void AudienceNetwork.RewardedVideoAd::set_RewardedVideoAdDidLoad(AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAd_set_RewardedVideoAdDidLoad_mD3F05F7B0089C31E0C2CFD5E77A6F4943F9973B3 (void);
// 0x000003A8 AudienceNetwork.FBRewardedVideoAdBridgeCallback AudienceNetwork.RewardedVideoAd::get_RewardedVideoAdWillLogImpression()
extern void RewardedVideoAd_get_RewardedVideoAdWillLogImpression_mA7468F3067A4D2B844C4420F177F0B2B8B1A4E54 (void);
// 0x000003A9 System.Void AudienceNetwork.RewardedVideoAd::set_RewardedVideoAdWillLogImpression(AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAd_set_RewardedVideoAdWillLogImpression_mE2CE0A45C9F32625538A7DE145589CAD42A38D10 (void);
// 0x000003AA AudienceNetwork.FBRewardedVideoAdBridgeErrorCallback AudienceNetwork.RewardedVideoAd::get_RewardedVideoAdDidFailWithError()
extern void RewardedVideoAd_get_RewardedVideoAdDidFailWithError_mCB24562902B60225BA782845C1C9020C601F1377 (void);
// 0x000003AB System.Void AudienceNetwork.RewardedVideoAd::set_RewardedVideoAdDidFailWithError(AudienceNetwork.FBRewardedVideoAdBridgeErrorCallback)
extern void RewardedVideoAd_set_RewardedVideoAdDidFailWithError_mD31F0F4B5DD7213C4ACC9839426B5CCCD420F0FD (void);
// 0x000003AC AudienceNetwork.FBRewardedVideoAdBridgeCallback AudienceNetwork.RewardedVideoAd::get_RewardedVideoAdDidClick()
extern void RewardedVideoAd_get_RewardedVideoAdDidClick_mDDBB5168254A673D4DF48A827A18EAA9EB1A52B2 (void);
// 0x000003AD System.Void AudienceNetwork.RewardedVideoAd::set_RewardedVideoAdDidClick(AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAd_set_RewardedVideoAdDidClick_m711E4D99437375B38D0FF35DE52C70E966D8F590 (void);
// 0x000003AE AudienceNetwork.FBRewardedVideoAdBridgeCallback AudienceNetwork.RewardedVideoAd::get_RewardedVideoAdWillClose()
extern void RewardedVideoAd_get_RewardedVideoAdWillClose_m633DD0E6617B76A06B35201E9E1C1F142C025B55 (void);
// 0x000003AF AudienceNetwork.FBRewardedVideoAdBridgeCallback AudienceNetwork.RewardedVideoAd::get_RewardedVideoAdDidClose()
extern void RewardedVideoAd_get_RewardedVideoAdDidClose_m722737A392A87EAD0DA8259FE5AD59E934A5C5FB (void);
// 0x000003B0 System.Void AudienceNetwork.RewardedVideoAd::set_RewardedVideoAdDidClose(AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAd_set_RewardedVideoAdDidClose_m56D05187E56AA6707A1B65BE0AF64324D270B768 (void);
// 0x000003B1 AudienceNetwork.FBRewardedVideoAdBridgeCallback AudienceNetwork.RewardedVideoAd::get_RewardedVideoAdComplete()
extern void RewardedVideoAd_get_RewardedVideoAdComplete_m462ABD29ECE953884337436F9BB93D1740F16E5B (void);
// 0x000003B2 AudienceNetwork.FBRewardedVideoAdBridgeCallback AudienceNetwork.RewardedVideoAd::get_RewardedVideoAdDidSucceed()
extern void RewardedVideoAd_get_RewardedVideoAdDidSucceed_mEB28E3FBDA25E71D6DFF414253E9D22946E53CBA (void);
// 0x000003B3 System.Void AudienceNetwork.RewardedVideoAd::set_RewardedVideoAdDidSucceed(AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAd_set_RewardedVideoAdDidSucceed_mA080BC9DE3C9C8862BA705ACE24E9B54849B069C (void);
// 0x000003B4 AudienceNetwork.FBRewardedVideoAdBridgeCallback AudienceNetwork.RewardedVideoAd::get_RewardedVideoAdDidFail()
extern void RewardedVideoAd_get_RewardedVideoAdDidFail_m5B1123EE9B0853395F2AD15F60F4F05D6E0CDEF3 (void);
// 0x000003B5 System.Void AudienceNetwork.RewardedVideoAd::set_RewardedVideoAdDidFail(AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAd_set_RewardedVideoAdDidFail_mD1B1F7678B360A3F3C2F05C848B2B892C4D0A432 (void);
// 0x000003B6 AudienceNetwork.FBRewardedVideoAdBridgeCallback AudienceNetwork.RewardedVideoAd::get_RewardedVideoAdActivityDestroyed()
extern void RewardedVideoAd_get_RewardedVideoAdActivityDestroyed_m246DC2E953D15F6B222829B7D06790A20080B26E (void);
// 0x000003B7 System.Void AudienceNetwork.RewardedVideoAd::set_RewardedVideoAdActivityDestroyed(AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAd_set_RewardedVideoAdActivityDestroyed_m1C01E72CAE28FFA9AC1FBFB83A0CFE244B498DEE (void);
// 0x000003B8 System.Void AudienceNetwork.RewardedVideoAd::.ctor(System.String)
extern void RewardedVideoAd__ctor_m206D7E56B15CA641B5F114D76A901BE66FB9BDDE (void);
// 0x000003B9 System.Void AudienceNetwork.RewardedVideoAd::.ctor(System.String,AudienceNetwork.RewardData)
extern void RewardedVideoAd__ctor_mA9B4DA4CC7D8848C14A5DCB592DB8FF36EE4819A (void);
// 0x000003BA System.Void AudienceNetwork.RewardedVideoAd::Finalize()
extern void RewardedVideoAd_Finalize_m22C43594B72471B683B85FB42576142F5F96FFBA (void);
// 0x000003BB System.Void AudienceNetwork.RewardedVideoAd::Dispose()
extern void RewardedVideoAd_Dispose_m0AB77D357A5D1D349C2A61C950582CF5B4DC5E39 (void);
// 0x000003BC System.Void AudienceNetwork.RewardedVideoAd::Dispose(System.Boolean)
extern void RewardedVideoAd_Dispose_m8C89561619EB587D27F15D234292381492DC3429 (void);
// 0x000003BD System.String AudienceNetwork.RewardedVideoAd::ToString()
extern void RewardedVideoAd_ToString_mD8FBAC53DB38CD43A5485AE5F10267518FAB80C2 (void);
// 0x000003BE System.Void AudienceNetwork.RewardedVideoAd::Register(UnityEngine.GameObject)
extern void RewardedVideoAd_Register_m35674944BBC1078A4F3ED0EDD97D9A0122AC6F12 (void);
// 0x000003BF System.Void AudienceNetwork.RewardedVideoAd::LoadAd()
extern void RewardedVideoAd_LoadAd_m897ED21843B26D25877E65B5CD87039A09BE27AC (void);
// 0x000003C0 System.Boolean AudienceNetwork.RewardedVideoAd::IsValid()
extern void RewardedVideoAd_IsValid_m8AC704631039C4E0EAA441844AF581BAAD7079C1 (void);
// 0x000003C1 System.Void AudienceNetwork.RewardedVideoAd::LoadAdFromData()
extern void RewardedVideoAd_LoadAdFromData_m144FEDBCA9CA8193453599AFE4F6B99F7E004E06 (void);
// 0x000003C2 System.Boolean AudienceNetwork.RewardedVideoAd::Show()
extern void RewardedVideoAd_Show_mC78512D2669EF8B78885DB70C9DE055ED6812585 (void);
// 0x000003C3 System.Void AudienceNetwork.RewardedVideoAd::ExecuteOnMainThread(System.Action)
extern void RewardedVideoAd_ExecuteOnMainThread_m99D031E76CF4A8F01149AB49ABD035716F24C5CC (void);
// 0x000003C4 System.Void AudienceNetwork.RewardedVideoAd::<LoadAdFromData>b__61_0()
extern void RewardedVideoAd_U3CLoadAdFromDataU3Eb__61_0_m113C89C79D2E074C458EF1789C2A6D0509C1A566 (void);
// 0x000003C5 System.Int32 AudienceNetwork.IRewardedVideoAdBridge::Create(System.String,AudienceNetwork.RewardData,AudienceNetwork.RewardedVideoAd)
// 0x000003C6 System.Int32 AudienceNetwork.IRewardedVideoAdBridge::Load(System.Int32)
// 0x000003C7 System.Boolean AudienceNetwork.IRewardedVideoAdBridge::IsValid(System.Int32)
// 0x000003C8 System.Boolean AudienceNetwork.IRewardedVideoAdBridge::Show(System.Int32)
// 0x000003C9 System.Void AudienceNetwork.IRewardedVideoAdBridge::Release(System.Int32)
// 0x000003CA System.Void AudienceNetwork.IRewardedVideoAdBridge::OnLoad(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
// 0x000003CB System.Void AudienceNetwork.IRewardedVideoAdBridge::OnImpression(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
// 0x000003CC System.Void AudienceNetwork.IRewardedVideoAdBridge::OnClick(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
// 0x000003CD System.Void AudienceNetwork.IRewardedVideoAdBridge::OnError(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeErrorCallback)
// 0x000003CE System.Void AudienceNetwork.IRewardedVideoAdBridge::OnWillClose(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
// 0x000003CF System.Void AudienceNetwork.IRewardedVideoAdBridge::OnDidClose(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
// 0x000003D0 System.Void AudienceNetwork.IRewardedVideoAdBridge::OnComplete(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
// 0x000003D1 System.Void AudienceNetwork.IRewardedVideoAdBridge::OnDidSucceed(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
// 0x000003D2 System.Void AudienceNetwork.IRewardedVideoAdBridge::OnDidFail(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
// 0x000003D3 System.Void AudienceNetwork.IRewardedVideoAdBridge::OnActivityDestroyed(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
// 0x000003D4 System.Void AudienceNetwork.RewardedVideoAdBridge::.ctor()
extern void RewardedVideoAdBridge__ctor_m7604E29BADB7C0685B83F95ABDC2058B104FBC33 (void);
// 0x000003D5 System.Void AudienceNetwork.RewardedVideoAdBridge::.cctor()
extern void RewardedVideoAdBridge__cctor_m778C43C866232250CE1E5C9C90D9750DBC6493C7 (void);
// 0x000003D6 AudienceNetwork.IRewardedVideoAdBridge AudienceNetwork.RewardedVideoAdBridge::CreateInstance()
extern void RewardedVideoAdBridge_CreateInstance_m4925538DF21DCE6091452FBE0BBDB2B49F8BC1BD (void);
// 0x000003D7 System.Int32 AudienceNetwork.RewardedVideoAdBridge::Create(System.String,AudienceNetwork.RewardData,AudienceNetwork.RewardedVideoAd)
extern void RewardedVideoAdBridge_Create_m99D85C52AFF1D290CA11CA0FF10C138320CF582A (void);
// 0x000003D8 System.Int32 AudienceNetwork.RewardedVideoAdBridge::Load(System.Int32)
extern void RewardedVideoAdBridge_Load_mDB69D7A91891B09FC908B651A2C36B9DD90FB583 (void);
// 0x000003D9 System.Boolean AudienceNetwork.RewardedVideoAdBridge::IsValid(System.Int32)
extern void RewardedVideoAdBridge_IsValid_m0D1E4251E4221DF09D93754E82D03A82DE2DE1B2 (void);
// 0x000003DA System.Boolean AudienceNetwork.RewardedVideoAdBridge::Show(System.Int32)
extern void RewardedVideoAdBridge_Show_m02B33DA6E0E075748540728653D122CCD0CC9180 (void);
// 0x000003DB System.Void AudienceNetwork.RewardedVideoAdBridge::Release(System.Int32)
extern void RewardedVideoAdBridge_Release_mE58B8ACC66C0A2A01C3EC0E002137774A45F3F62 (void);
// 0x000003DC System.Void AudienceNetwork.RewardedVideoAdBridge::OnLoad(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridge_OnLoad_m3809EA5A58DE011EFFC22BE822D8169FD15D8C84 (void);
// 0x000003DD System.Void AudienceNetwork.RewardedVideoAdBridge::OnImpression(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridge_OnImpression_m48FF53D78E76BC2374CFD85A172126DF37A81846 (void);
// 0x000003DE System.Void AudienceNetwork.RewardedVideoAdBridge::OnClick(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridge_OnClick_m96006AB463F9391090C3FB3C5583C4F5519FC487 (void);
// 0x000003DF System.Void AudienceNetwork.RewardedVideoAdBridge::OnError(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeErrorCallback)
extern void RewardedVideoAdBridge_OnError_m77FB4FC98AB36EBE5417B09B46850103F51159E7 (void);
// 0x000003E0 System.Void AudienceNetwork.RewardedVideoAdBridge::OnWillClose(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridge_OnWillClose_mEE1D54D1A99EC68C0A2950EFFF90B1B7188F8B98 (void);
// 0x000003E1 System.Void AudienceNetwork.RewardedVideoAdBridge::OnDidClose(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridge_OnDidClose_m3669C081E894BFDCD94E1F70EBA95297B87F0750 (void);
// 0x000003E2 System.Void AudienceNetwork.RewardedVideoAdBridge::OnComplete(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridge_OnComplete_m5240E0BE8C516470C84CA5152511AAB17FABBFFD (void);
// 0x000003E3 System.Void AudienceNetwork.RewardedVideoAdBridge::OnDidSucceed(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridge_OnDidSucceed_m9831C4C163332DE8C0C9780DC03DF16C6C21025F (void);
// 0x000003E4 System.Void AudienceNetwork.RewardedVideoAdBridge::OnDidFail(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridge_OnDidFail_m7E7C1B81D4BA4D9DB8D35AD67D369ED3B78E2EDB (void);
// 0x000003E5 System.Void AudienceNetwork.RewardedVideoAdBridge::OnActivityDestroyed(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridge_OnActivityDestroyed_m781010FE9596C28F1FE2FF9716B11D3151314737 (void);
// 0x000003E6 UnityEngine.AndroidJavaObject AudienceNetwork.RewardedVideoAdBridgeAndroid::RewardedVideoAdForUniqueId(System.Int32)
extern void RewardedVideoAdBridgeAndroid_RewardedVideoAdForUniqueId_mCF7526C86DE10ED2B75E68C7125C76812F3B5B45 (void);
// 0x000003E7 AudienceNetwork.RewardedVideoAdContainer AudienceNetwork.RewardedVideoAdBridgeAndroid::RewardedVideoAdContainerForUniqueId(System.Int32)
extern void RewardedVideoAdBridgeAndroid_RewardedVideoAdContainerForUniqueId_mEBAE25181BBE2474759D9F6820DDC025AF7CBB4E (void);
// 0x000003E8 System.Int32 AudienceNetwork.RewardedVideoAdBridgeAndroid::Create(System.String,AudienceNetwork.RewardData,AudienceNetwork.RewardedVideoAd)
extern void RewardedVideoAdBridgeAndroid_Create_mECA447FDE335FACE5955E834EF89F95A7DA5DC99 (void);
// 0x000003E9 System.Int32 AudienceNetwork.RewardedVideoAdBridgeAndroid::Load(System.Int32)
extern void RewardedVideoAdBridgeAndroid_Load_m6DF53C6A3629A9B7BAB5AC5B736454A74E487651 (void);
// 0x000003EA System.Boolean AudienceNetwork.RewardedVideoAdBridgeAndroid::IsValid(System.Int32)
extern void RewardedVideoAdBridgeAndroid_IsValid_mABEA2E745E004A988F9D1E54A44AEEC88FE33E3C (void);
// 0x000003EB System.Boolean AudienceNetwork.RewardedVideoAdBridgeAndroid::Show(System.Int32)
extern void RewardedVideoAdBridgeAndroid_Show_m66E7441A75B5D14CD20DAB07ACC601BF40DD88BA (void);
// 0x000003EC System.Void AudienceNetwork.RewardedVideoAdBridgeAndroid::Release(System.Int32)
extern void RewardedVideoAdBridgeAndroid_Release_m312D1DE90460C974B58FA8569DA48AD4C1460E1E (void);
// 0x000003ED System.Void AudienceNetwork.RewardedVideoAdBridgeAndroid::OnLoad(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridgeAndroid_OnLoad_m8943CBDD14EDCFE684860B2AEC92DCD40460473F (void);
// 0x000003EE System.Void AudienceNetwork.RewardedVideoAdBridgeAndroid::OnImpression(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridgeAndroid_OnImpression_m7F74A87458FFF2101F5C063B675B39D506A75EC6 (void);
// 0x000003EF System.Void AudienceNetwork.RewardedVideoAdBridgeAndroid::OnClick(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridgeAndroid_OnClick_m38EA89E5A2A434E28C18CC4A7096B9694486430F (void);
// 0x000003F0 System.Void AudienceNetwork.RewardedVideoAdBridgeAndroid::OnError(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeErrorCallback)
extern void RewardedVideoAdBridgeAndroid_OnError_m5BC36305EBD0F8FA26668EEBB76C6A409575B95A (void);
// 0x000003F1 System.Void AudienceNetwork.RewardedVideoAdBridgeAndroid::OnWillClose(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridgeAndroid_OnWillClose_m2DBD46E62DD2DB027AA4AD6A751362A0BB2E83CA (void);
// 0x000003F2 System.Void AudienceNetwork.RewardedVideoAdBridgeAndroid::OnDidClose(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridgeAndroid_OnDidClose_mDBFD095DB9C7A43C1EB6FBFC748EFBB26D02A87D (void);
// 0x000003F3 System.Void AudienceNetwork.RewardedVideoAdBridgeAndroid::OnActivityDestroyed(System.Int32,AudienceNetwork.FBRewardedVideoAdBridgeCallback)
extern void RewardedVideoAdBridgeAndroid_OnActivityDestroyed_m6689D87D40FABEF2194AC708EB18CA2325F56397 (void);
// 0x000003F4 System.Void AudienceNetwork.RewardedVideoAdBridgeAndroid::.ctor()
extern void RewardedVideoAdBridgeAndroid__ctor_m182C87E5DC2793CD48C108AE8F902B4C94D95BEA (void);
// 0x000003F5 System.Void AudienceNetwork.RewardedVideoAdBridgeAndroid::.cctor()
extern void RewardedVideoAdBridgeAndroid__cctor_m5A3C36B1886DAB1A6EA67807231376A105F041C5 (void);
// 0x000003F6 System.Void AudienceNetwork.RewardedVideoAdBridgeAndroid/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m75C3B64F838AA94122176BE075E4D06F23466F6F (void);
// 0x000003F7 System.Void AudienceNetwork.RewardedVideoAdBridgeAndroid/<>c__DisplayClass10_0::<Show>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CShowU3Eb__0_m844C438B2600B7FCBB3E982272DE01502B9E6124 (void);
// 0x000003F8 AudienceNetwork.RewardedVideoAd AudienceNetwork.RewardedVideoAdContainer::get_rewardedVideoAd()
extern void RewardedVideoAdContainer_get_rewardedVideoAd_m68C0CB84DAA674C467D1DF65788AB3361889F39F (void);
// 0x000003F9 System.Void AudienceNetwork.RewardedVideoAdContainer::set_rewardedVideoAd(AudienceNetwork.RewardedVideoAd)
extern void RewardedVideoAdContainer_set_rewardedVideoAd_m1B0F28CA41F608909F511BEFB6D4CF224B0903C5 (void);
// 0x000003FA AudienceNetwork.FBRewardedVideoAdBridgeCallback AudienceNetwork.RewardedVideoAdContainer::get_onLoad()
extern void RewardedVideoAdContainer_get_onLoad_mB2E4719AF636CE6DFB1E45633474FFA5F4D3D1F6 (void);
// 0x000003FB System.Void AudienceNetwork.RewardedVideoAdContainer::.ctor(AudienceNetwork.RewardedVideoAd)
extern void RewardedVideoAdContainer__ctor_mC1D05DF8718F12B908BFD9D16F2047C9C7BD4AA5 (void);
// 0x000003FC System.String AudienceNetwork.RewardedVideoAdContainer::ToString()
extern void RewardedVideoAdContainer_ToString_m31FA708066DC5372098F0B2357CC965AE5A3965A (void);
// 0x000003FD UnityEngine.AndroidJavaObject AudienceNetwork.RewardedVideoAdContainer::LoadAdConfig(System.String)
extern void RewardedVideoAdContainer_LoadAdConfig_m66E6D2B778B011620203ACE21213548B615AA264 (void);
// 0x000003FE System.Void AudienceNetwork.RewardedVideoAdContainer::Load()
extern void RewardedVideoAdContainer_Load_mCC9D273C25650E07308E2FCAF765474E4B6FB8FF (void);
// 0x000003FF System.Void AudienceNetwork.RewardedVideoAdContainer::Load(System.String)
extern void RewardedVideoAdContainer_Load_mA533DDF6D3CB0770051586A37F400B36603F3686 (void);
// 0x00000400 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::.ctor(AudienceNetwork.RewardedVideoAd,UnityEngine.AndroidJavaObject)
extern void RewardedVideoAdBridgeListenerProxy__ctor_mBC1A5F16FDED756049E689FBEA193426E117792D (void);
// 0x00000401 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::onError(UnityEngine.AndroidJavaObject,UnityEngine.AndroidJavaObject)
extern void RewardedVideoAdBridgeListenerProxy_onError_mE30FA6850454A0A4B2173941A7377DEAA8CE94A4 (void);
// 0x00000402 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::onAdLoaded(UnityEngine.AndroidJavaObject)
extern void RewardedVideoAdBridgeListenerProxy_onAdLoaded_m6EDEFE1300C03E9258C3B1B907CEAA69804240EE (void);
// 0x00000403 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::onAdClicked(UnityEngine.AndroidJavaObject)
extern void RewardedVideoAdBridgeListenerProxy_onAdClicked_mAFF12D9D6FD62F830E5EC986CF7B2DAF0A524A93 (void);
// 0x00000404 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::onRewardedVideoDisplayed(UnityEngine.AndroidJavaObject)
extern void RewardedVideoAdBridgeListenerProxy_onRewardedVideoDisplayed_m3FA5B655E42232E0F9AAFEB79DDCB679CE5ABB5B (void);
// 0x00000405 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::onRewardedVideoClosed()
extern void RewardedVideoAdBridgeListenerProxy_onRewardedVideoClosed_m7AF118B3A9B70ED0DBE781DB024DBC0772FF28D6 (void);
// 0x00000406 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::onRewardedVideoCompleted()
extern void RewardedVideoAdBridgeListenerProxy_onRewardedVideoCompleted_mB6A855DC4859C85FE6480624B10E642D7A6B9D0C (void);
// 0x00000407 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::onRewardServerSuccess()
extern void RewardedVideoAdBridgeListenerProxy_onRewardServerSuccess_m16A0ECAB52A49FDA48D00EF1257B81B69A79D14F (void);
// 0x00000408 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::onRewardServerFailed()
extern void RewardedVideoAdBridgeListenerProxy_onRewardServerFailed_mC6F89C8B01EA197A6952E8CCE624BA0AFCACB83F (void);
// 0x00000409 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::onLoggingImpression(UnityEngine.AndroidJavaObject)
extern void RewardedVideoAdBridgeListenerProxy_onLoggingImpression_m96585A5DC18A6126BD3159FF31A11D839E6C8DCB (void);
// 0x0000040A System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::onRewardedVideoActivityDestroyed()
extern void RewardedVideoAdBridgeListenerProxy_onRewardedVideoActivityDestroyed_m2F93E87F08AFEABEEBBA8A18A0A661C42C21470B (void);
// 0x0000040B System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::<onAdClicked>b__5_0()
extern void RewardedVideoAdBridgeListenerProxy_U3ConAdClickedU3Eb__5_0_mB842815494CA7A853DD880303F59F3DF60B894CD (void);
// 0x0000040C System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::<onRewardedVideoDisplayed>b__6_0()
extern void RewardedVideoAdBridgeListenerProxy_U3ConRewardedVideoDisplayedU3Eb__6_0_mD12182F2C5859E5F60D8570075BFA97CC89CE39A (void);
// 0x0000040D System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::<onRewardedVideoClosed>b__7_0()
extern void RewardedVideoAdBridgeListenerProxy_U3ConRewardedVideoClosedU3Eb__7_0_m5933FF023CB75079BA91B778DF07CFBAB4A1BF8C (void);
// 0x0000040E System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::<onRewardedVideoCompleted>b__8_0()
extern void RewardedVideoAdBridgeListenerProxy_U3ConRewardedVideoCompletedU3Eb__8_0_mB339FE1BDD19A08DA4AA0310A37855FD5F2DA26E (void);
// 0x0000040F System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::<onRewardServerSuccess>b__9_0()
extern void RewardedVideoAdBridgeListenerProxy_U3ConRewardServerSuccessU3Eb__9_0_m70128105A8391D3025BDEAE469442A246F5B48AC (void);
// 0x00000410 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::<onRewardServerFailed>b__10_0()
extern void RewardedVideoAdBridgeListenerProxy_U3ConRewardServerFailedU3Eb__10_0_mF277208F423DD348B08D90EC7BD0003CC2E22008 (void);
// 0x00000411 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::<onLoggingImpression>b__11_0()
extern void RewardedVideoAdBridgeListenerProxy_U3ConLoggingImpressionU3Eb__11_0_m052C94762C225F204FD5037217F73B07DBFAAEDC (void);
// 0x00000412 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy::<onRewardedVideoActivityDestroyed>b__12_0()
extern void RewardedVideoAdBridgeListenerProxy_U3ConRewardedVideoActivityDestroyedU3Eb__12_0_m17EA0E3457A2CED3D832554E28C3ABCDAEDD1884 (void);
// 0x00000413 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mAB5059429A7096CD23555B020E480D6FF0D0D4B1 (void);
// 0x00000414 System.Void AudienceNetwork.RewardedVideoAdBridgeListenerProxy/<>c__DisplayClass3_0::<onError>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3ConErrorU3Eb__0_m8F38B16208E0801A4DB6D2B96BB0813D291C02ED (void);
// 0x00000415 System.String AudienceNetwork.SdkVersion::get_Build()
extern void SdkVersion_get_Build_mBEAADE9EE4890BF3603882F1E0791477CB0E9D1B (void);
// 0x00000416 System.Double AudienceNetwork.Utility.AdUtility::Width()
extern void AdUtility_Width_m53FB022B8EA642F02E4EC7A4E45C738EE221FEC7 (void);
// 0x00000417 System.Double AudienceNetwork.Utility.AdUtility::Height()
extern void AdUtility_Height_m5E7CFB82BF859E590117210844CC41598A7B017E (void);
// 0x00000418 System.Void AudienceNetwork.Utility.AdUtility::Prepare()
extern void AdUtility_Prepare_mE7B4059FE922EA4228F4D852096E841F4908E489 (void);
// 0x00000419 System.Double AudienceNetwork.Utility.IAdUtilityBridge::Width()
// 0x0000041A System.Double AudienceNetwork.Utility.IAdUtilityBridge::Height()
// 0x0000041B System.Void AudienceNetwork.Utility.IAdUtilityBridge::Prepare()
// 0x0000041C System.Void AudienceNetwork.Utility.AdUtilityBridge::.ctor()
extern void AdUtilityBridge__ctor_m0A963823A15E6591C458EB110DFAE4AFC536EDFA (void);
// 0x0000041D System.Void AudienceNetwork.Utility.AdUtilityBridge::.cctor()
extern void AdUtilityBridge__cctor_m71D6AFC011E1FEE3FF56B2D7CF6764E78A7FCC80 (void);
// 0x0000041E AudienceNetwork.Utility.IAdUtilityBridge AudienceNetwork.Utility.AdUtilityBridge::CreateInstance()
extern void AdUtilityBridge_CreateInstance_m291203ADD4AF8294B08979D27560137B8C7C98D4 (void);
// 0x0000041F System.Double AudienceNetwork.Utility.AdUtilityBridge::Width()
extern void AdUtilityBridge_Width_m46FD7B57BE4376B1799EC4F7877C6FC765E6FA64 (void);
// 0x00000420 System.Double AudienceNetwork.Utility.AdUtilityBridge::Height()
extern void AdUtilityBridge_Height_m35EA150473621FE40192348E0F0D4D691AFF363B (void);
// 0x00000421 System.Double AudienceNetwork.Utility.AdUtilityBridge::Convert(System.Double)
extern void AdUtilityBridge_Convert_m9D472E757903B9804CC0B7712E63A6CE50B30D8A (void);
// 0x00000422 System.Void AudienceNetwork.Utility.AdUtilityBridge::Prepare()
extern void AdUtilityBridge_Prepare_m9DEE605AFDED59E31902574D8AB3B43A8A3EA479 (void);
// 0x00000423 T AudienceNetwork.Utility.AdUtilityBridgeAndroid::GetPropertyOfDisplayMetrics(System.String)
// 0x00000424 System.Double AudienceNetwork.Utility.AdUtilityBridgeAndroid::Density()
extern void AdUtilityBridgeAndroid_Density_m0ED8E4A7E92E75A9A716FFC238BD6798331FEEEE (void);
// 0x00000425 System.Double AudienceNetwork.Utility.AdUtilityBridgeAndroid::Width()
extern void AdUtilityBridgeAndroid_Width_m184FCB1F858DDAA07B53347F4E1D312DC31A33FD (void);
// 0x00000426 System.Double AudienceNetwork.Utility.AdUtilityBridgeAndroid::Height()
extern void AdUtilityBridgeAndroid_Height_mB65AD7B91DCCDA0ABDBA05DB35CBEC2A24EFB1DE (void);
// 0x00000427 System.Double AudienceNetwork.Utility.AdUtilityBridgeAndroid::Convert(System.Double)
extern void AdUtilityBridgeAndroid_Convert_mDC09CA70FB26CA578470C21DAA185C970D1E6DE8 (void);
// 0x00000428 System.Void AudienceNetwork.Utility.AdUtilityBridgeAndroid::Prepare()
extern void AdUtilityBridgeAndroid_Prepare_m329280744DEEC835F388F4E9830F7763C260EEB7 (void);
// 0x00000429 System.Void AudienceNetwork.Utility.AdUtilityBridgeAndroid::.ctor()
extern void AdUtilityBridgeAndroid__ctor_mCAB48FA91CF8DD82CA7A8FC4ABAC10C41F096AD9 (void);
static Il2CppMethodPointer s_methodPointers[1065] = 
{
	AdViewScene_OnDestroy_mFDE4D1F8CB0ADB1CC6CB53A5DFCD825C8CFC0C93,
	AdViewScene_Awake_mDDB0BDCEC4EE214804CAB81785B2F371E1AECF1E,
	AdViewScene_SetLoadAddButtonText_m10EBCAD8E76B326AA5CEE0B40641F4CF65C96D14,
	AdViewScene_LoadBanner_m7D94454CE74145472E2861E9D0A5320C441CDFA7,
	AdViewScene_ChangeBannerSize_m2D5D1F243F1DEB1E5EA03F721E828B7FA28A9966,
	AdViewScene_NextScene_m761AF4305EEFEE3C7A87F26ADF31A772D9A24972,
	AdViewScene_ChangePosition_mD5EF1589A80F3FC7C68641A474F5FA54C214BADC,
	AdViewScene_OnRectTransformDimensionsChange_m797CBD3BA29C7A92983F493B3F239DD5DE67CD86,
	AdViewScene_SetAdViewPosition_m29CF9A2B17ACE1461C7053D7C66A282CCBB9B86C,
	AdViewScene__ctor_m14C350DCAFD8B4217322E71A1FE3B9B9F5DA3AC0,
	AdViewScene_U3CLoadBannerU3Eb__10_0_mA83AD65C2C2144D86D8885E07036347B2E0C1F53,
	AdViewScene_U3CLoadBannerU3Eb__10_1_mA4370BABEFDB30C9A2EA915EDFC3EEBE6902D425,
	AdViewScene_U3CLoadBannerU3Eb__10_2_m7B1A498B76D0942B988C8BAC7AC3FA6DB8C9D64F,
	AdViewScene_U3CLoadBannerU3Eb__10_3_m55C31A233C5126E8F5C3073B5E1E3EB0DEEC8297,
	BaseScene_Update_m1C3CCC3B5989D9087CE03C85F27CD68E710B60AA,
	BaseScene_LoadSettingsScene_m2A1E3C6C7DB676236BB911123B3F67FEAAE8CFB5,
	BaseScene__ctor_m0DA175FBEECCE8B0400ABC2B11946C430ABD33C8,
	InterstitialAdScene_Awake_m6850AD09E9EA945D26568E0CF2A698787C36DC6E,
	InterstitialAdScene_LoadInterstitial_mB899FE1C6F5FF014C68BFC6A8FB2D39C42838EA9,
	InterstitialAdScene_ShowInterstitial_m909152BBA6FC6DE38AE6E7BC4EA32B8C054562F1,
	InterstitialAdScene_OnDestroy_mEAF7361B8C02C19B7B0A27423D96E6759C3F280E,
	InterstitialAdScene_NextScene_m521266A0DF5FC2A2750A798C6879AAE64FF5D7D9,
	InterstitialAdScene__ctor_m2ADB34A047E3CAEE213C1B98A323078971BA9B64,
	InterstitialAdScene_U3CLoadInterstitialU3Eb__5_0_mC11D6CAD99B51A5BBBF05C4088ADEB4EF1A86656,
	InterstitialAdScene_U3CLoadInterstitialU3Eb__5_1_m36C94989FAF7352364A7207325456FB47539137E,
	InterstitialAdScene_U3CLoadInterstitialU3Eb__5_4_m134A19820032496839350C04344EBE9EC288F927,
	InterstitialAdScene_U3CLoadInterstitialU3Eb__5_5_mC4D72386836014E3063A67FB5DC1E9CC2B99DEAB,
	U3CU3Ec__cctor_m202176C53DA0551139774A1E7928FB3BCB9BCAC7,
	U3CU3Ec__ctor_mBDFC2D25849802054C8D07A549F066BB067E3399,
	U3CU3Ec_U3CLoadInterstitialU3Eb__5_2_mABECD1E4EBA4B0BA97193A3A50FB985767A03F82,
	U3CU3Ec_U3CLoadInterstitialU3Eb__5_3_m5A158F650EC2717B7DDFDCB4552A66E62BFCD667,
	RewardedVideoAdScene_Awake_m21E541319C8B5A972111B5905E6A2922363BAF84,
	RewardedVideoAdScene_LoadRewardedVideo_mC8DD3F08F1C3886ED769889158145BCA71DA0143,
	RewardedVideoAdScene_ShowRewardedVideo_mF6FAFB4F03B477B1D98857037E96533860188926,
	RewardedVideoAdScene_OnDestroy_m6BA0043E387F39B485D204C3FAD798D615A1AAA9,
	RewardedVideoAdScene_NextScene_m1E9DC5EF6A25C93C60B08837785ADF2494C5A2EF,
	RewardedVideoAdScene__ctor_m866B77407BC24A40D4F03F8243ABBA41145166B5,
	RewardedVideoAdScene_U3CLoadRewardedVideoU3Eb__5_0_mBD89716729D68AE731B29BDC4980848D97CC959A,
	RewardedVideoAdScene_U3CLoadRewardedVideoU3Eb__5_1_mBD72EA520B885A0784CB8774E0BA771F11EE5962,
	RewardedVideoAdScene_U3CLoadRewardedVideoU3Eb__5_6_mD48FA2B95BFB085FC1833F08DD49BDC2968986D2,
	RewardedVideoAdScene_U3CLoadRewardedVideoU3Eb__5_7_m448537BA94EA1EFD65B6BC5AB90826D12AB5A279,
	U3CU3Ec__cctor_mF3A1661195C2C8FA97BB7B2E21DC71D17961B8AA,
	U3CU3Ec__ctor_mB6AB83FA5860B334F0EDF19AF9941AA5875B0ACB,
	U3CU3Ec_U3CLoadRewardedVideoU3Eb__5_2_mB80AF20222FED26CA1E672DE5D7CE4C9CE130767,
	U3CU3Ec_U3CLoadRewardedVideoU3Eb__5_3_mFA35B8C3D1E4383262CA7F88F8A0F5615A701292,
	U3CU3Ec_U3CLoadRewardedVideoU3Eb__5_4_m149D60C443F55ABD4C875C64AF4548B665C4CD49,
	U3CU3Ec_U3CLoadRewardedVideoU3Eb__5_5_mEFA33F75A9C6684B114EC7AD23CB91C5C9320033,
	SettingsScene_InitializeSettings_m23FB96519EA306C061CF13CE68D4480B63BAB4B3,
	SettingsScene_Start_m5FE2B554EC58551D11E2E2CA8828368B3162C80A,
	SettingsScene_OnEditEnd_m0E5A66610CA3427ABB5EB8D25B024B61F2474D94,
	SettingsScene_SaveSettings_mCCB083C36CD2FD0B8B51D964301D8D995FD355F3,
	SettingsScene_AdViewScene_mBEB858300F74422353E2C16A3E3AAD2076F283B8,
	SettingsScene_InterstitialAdScene_m24DE1E5107978C28D2ABCC173AA87BAF5719CA35,
	SettingsScene_RewardedVideoAdScene_mF08541790DEC73CE0289BC883B79C2D571077F41,
	SettingsScene__ctor_mA86D6D212F86C534FB12555AA7AB3794C976902A,
	SettingsScene__cctor_m4ACEA42A7778BB09CE3F9743214A670BE683E184,
	checkNet_Update_mD69F5038CCF13227F6CAC9DDA036EBC8617E4002,
	checkNet__ctor_mBEAC12478D70251D28CB97AF66B00676EC7C4A6C,
	Implementation_Awake_mE7092E8EC7973F025E4345AE29E04E11A6791E80,
	Implementation_Start_m872A0AABC891DB188C41ACB3588D593743943833,
	Implementation_ShawBanner_mA5FF1B6846CBE987D380374222790DDC3D1F9B43,
	Implementation_HideBanner_mECE627CA15F4CD25E0F63887B55AE3CCE0F4C69D,
	Implementation_ShowInterstitial_m5C3A8D90C9B75A04F6B9338AAEF7055A17CABFF3,
	Implementation_ShowRewardedVideo_m0E19095814347ED91ADA369B2F9EE8DE745A74D1,
	Implementation_Update_m61C084FC0932343F5C4A40813D13827423FDD52B,
	Implementation_CompleteMethod_mF633B329527BE164584B4AC33F61757346CE2832,
	Implementation__ctor_m9DEA8F8E497AD0F1D71927071A55F44AADDA5D08,
	TestAds_Start_mBFC5EFB231F100D4150439A360B738A2071F38EB,
	TestAds_OnGUI_m8F75FFEE6D481E5D3E474E8CAA1637F42FE9EFA2,
	TestAds_InterstitialClosed_m02D36B5311B6410E242EEFADBE3DE50160D5192B,
	TestAds_CompleteMethod_m627910A8EE161F3AFEDDC4322C76E7F1DFBF114F,
	TestAds__ctor_m320DAE171DF5ACB5C267183C19864BDF1AFDD2F0,
	Advertiser__ctor_mCB085210C61BEB5C605B44D76A52C9E325F15FD6,
	Advertisements_get_Instance_m0593407095ED68645A100F756BDCBE8362BFF975,
	Advertisements_SetUserConsent_m04862C0D945D3FFBDC589A32C0FD6E4E1974276D,
	Advertisements_SetCCPAConsent_m8EB5BC876CA39339B3A7413613C222BC54429E4E,
	Advertisements_GetConsent_m9729254344D396A779788AB128CBCE09B3DACC34,
	Advertisements_ConsentWasSet_m2AB9EA7FA4C406BD38631E35D00F3CDBAB92BD2A,
	Advertisements_GetUserConsent_m1A409021B05385D24A4E5169ED24906287EE30CD,
	Advertisements_GetCCPAConsent_m80C0C3F355C31726641AFFCA6E8B9BCEC81A70FE,
	Advertisements_UserConsentWasSet_m3164AD181A066E604B2B15848541E97E4752F58C,
	Advertisements_CCPAConsentWasSet_mF1B145AEB60B9B47F45F11382F7F24BFDAC99CC8,
	Advertisements_RemoveAds_mA11908EA558AAB8B158CE87650523034B261C0A2,
	Advertisements_CanShowAds_m04BE533F9F4541D18D8DA1BF67CBEA68662AE3AD,
	Advertisements_Initialize_m2C47C13B5808B687C06EA56F3EF9B389058FB25D,
	Advertisements_WaitForConsent_mFF2DFDF08DE97E3A70F448A84EDEEBAAEF1D5482,
	Advertisements_ContinueInitialization_m0CEA001408002747E388CB9097C90B57E91BEFFD,
	Advertisements_UpdateUserConsent_mC25FA5D05C21380B62238E4E63AFDC447CEA2D4E,
	Advertisements_ShowInterstitial_m8C1F65815E3C3CBDC70C66B4A8B0F7144025C7F7,
	Advertisements_ShowInterstitial_m0418A9FF2661E94BF2362CC8312A830304638320,
	Advertisements_ShowInterstitial_m7D11FB14B15B1090B2C8BAF7C60EDCFA3364C195,
	Advertisements_GetInterstitialAdvertiser_m86D3F0FE83387F1122219EFE5D6C9B9197E9216B,
	Advertisements_ShowRewardedVideo_mFC1BA0197C4B026B04842C6AE23D5087BF0D0FC3,
	Advertisements_ShowRewardedVideo_mD70F23B82E40CA215812646937EB016A0498D22C,
	Advertisements_ShowRewardedVideo_mA726AD68698919D965D9B6C596EBDB271BE59FDB,
	Advertisements_ShowBanner_m830434ABEF4F35E97B2BF37E01A17C8B1F374BC0,
	Advertisements_ShowBanner_m02D3E282D5E8784BDD720CBB17B5408FA74F8209,
	Advertisements_LoadBanner_m4671085C5860F9E361D6D43036BF23D031182731,
	Advertisements_BannerDisplayedResult_mBC146C2CD24754C50C7F272E7582D1D7914E4CE4,
	Advertisements_HideBanner_m7357FDDAC20A4D2D004DDA7C15E214CAE3595567,
	Advertisements_UsePercent_m3E3F9DA3D62D351CCFE7C5196DF0E95EC2010663,
	Advertisements_UseOrder_mD28D4E9C7A0E2358F4C69504FE9F1F67683F9BEB,
	Advertisements_LoadFile_mFA58B4C896E325346DBFB9EB61F08C2A9C09CF99,
	Advertisements_LoadFile_mA9B7B1205BDDDE223DA9F8F056AAC4349FF39736,
	Advertisements_UpdateSettings_m0409C3AD7D2458361FB656C4749BFDF2DF70C0B8,
	Advertisements_ApplySettings_mDC7E3C27FB48A8F29029D97C6031BFF908CD1C5A,
	Advertisements_IsRewardVideoAvailable_mBADE08BD1AC5EEDF6766D507553B4F2FA6E919DD,
	Advertisements_IsInterstitialAvailable_m524FF7F2F7F00BB7D4A50BDC4B990538AAA87A54,
	Advertisements_IsBannerAvailable_m8091A9EE50201CB5B0C20252D50559E41E5E6AA5,
	Advertisements_IsBannerOnScreen_m29F7588BC172A6C1720C707B3BEC27DFE1E742F2,
	Advertisements_DisplayAdvertisers_m123FAD8F505756B700A0BB2609F2F2318C37B0FE,
	Advertisements_GetAllAdvertisers_m298B08D6502964358FDB677CFC43BD1CDB2844DE,
	Advertisements_GetBannerAdvertisers_m53E7BF45B1D4C82835DD63924441E0F6FC569E65,
	Advertisements_GetInterstitialAdvertisers_m643B4FE8382767A54A6FC6C0BB333D09CCC5BB8D,
	Advertisements_GetRewardedAdvertisers_m25AE7047899E8F6FB55030BE6AEDBE8BBE2D38CB,
	Advertisements__ctor_m196FAB4C079F6174FF0FCAAA517DFD0B52535E9C,
	U3CWaitForConsentU3Ed__30__ctor_mEF3F558CC844D495511A3EE729299BAA7EA33661,
	U3CWaitForConsentU3Ed__30_System_IDisposable_Dispose_m1507FFF762E488F54036D4107F166B844750FCC6,
	U3CWaitForConsentU3Ed__30_MoveNext_m15A78578B7532EB5B8B88EFAAA88C83ED2AD4194,
	U3CWaitForConsentU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m461801BB3E622DFFA270CC42435ACCF242FCD8B5,
	U3CWaitForConsentU3Ed__30_System_Collections_IEnumerator_Reset_m23CF3803C5066B335D1E8A7F4354D0D4B83C6896,
	U3CWaitForConsentU3Ed__30_System_Collections_IEnumerator_get_Current_m147B4ED3C07166B678D1EE61F40677DC60DFF73B,
	U3CU3Ec__cctor_mD34C1990F3788A3D0EC714DA46314F714A729016,
	U3CU3Ec__ctor_m031DEFFEF2B73091CB187A978DDC232AE37EC98E,
	U3CU3Ec_U3CContinueInitializationU3Eb__31_0_mCB89CAEC2A808C5232A26CF7EC17AF0A541FEA8A,
	U3CU3Ec_U3CContinueInitializationU3Eb__31_1_m9477EE09452D2ECB262A580962A86DE1F2E1C3B5,
	U3CU3Ec_U3CContinueInitializationU3Eb__31_2_mA4E69678C0B8C1226FC05F85D873082F8D3CAF01,
	U3CU3Ec_U3CContinueInitializationU3Eb__31_3_mFA2DC3D2C04CBDBF16DBCB84ECEFEB6B2F2EEEE5,
	U3CU3Ec_U3CContinueInitializationU3Eb__31_4_m444DAEE6A32D52D7CB7D610EB209A1B900772443,
	U3CU3Ec_U3CContinueInitializationU3Eb__31_5_mC34D7D0FBF69A124EC430BF245DDF69860BE659C,
	U3CU3Ec_U3CContinueInitializationU3Eb__31_6_m292AED839A8536F19D7BEFFF1DB95FB219E37872,
	U3CU3Ec_U3CContinueInitializationU3Eb__31_7_m0AB7482887AC5B8C7FFDF6E295C64FCB5EDB2105,
	U3CU3Ec_U3CContinueInitializationU3Eb__31_8_mB7FA94A31346B422EE17CD0E33117054D271B437,
	U3CU3Ec_U3CContinueInitializationU3Eb__31_9_mE0582532A64498CBC93AC5BD74286C433A8C84D6,
	U3CU3Ec_U3CApplySettingsU3Eb__50_0_mAB00A38D6691A5020FDAE55671901C8E7BFB5F55,
	U3CU3Ec_U3CApplySettingsU3Eb__50_1_m6977D10B4F7EF2CB997836E5CF1A274A31465FF1,
	U3CU3Ec_U3CApplySettingsU3Eb__50_2_m8C1A288F56B381B1A76BA9A5A7DEE91783B1FD07,
	U3CU3Ec_U3CApplySettingsU3Eb__50_3_m48C572C6DBF8A65F5146E9FB1C7C349D9281A696,
	U3CU3Ec_U3CApplySettingsU3Eb__50_4_mE046FA8D25580F975F8B8B93A50F9B3E5FEBCBEF,
	U3CU3Ec_U3CApplySettingsU3Eb__50_5_m57BF3B032049434278BE031727C4DC0555F8A24F,
	U3CU3Ec__DisplayClass35_0__ctor_m5C77E13AAE022FDB6975B468A694DFF0D5801CFF,
	U3CU3Ec__DisplayClass35_0_U3CShowInterstitialU3Eb__0_m139D1121622D19575AF806B2DE399BC09DDD7DB8,
	U3CU3Ec__DisplayClass39_0__ctor_m71B1282F80EAB07BF4F93CD386DF2974426CC9A5,
	U3CU3Ec__DisplayClass39_0_U3CShowRewardedVideoU3Eb__0_m8C9E1FE69A461431CA0DCE7DA0F3E2E67015D6AD,
	U3CU3Ec__DisplayClass42_0__ctor_m9EB0EB779CDAA398FEA78300E97257108BD55FAD,
	U3CU3Ec__DisplayClass42_0_U3CLoadBannerU3Eb__0_m70A8730CBA7F3E6A25442E9535FD9202ECA45800,
	U3CLoadFileU3Ed__48__ctor_m9A70F7BB3CDF96EEA4CE0622FBD014203735A684,
	U3CLoadFileU3Ed__48_System_IDisposable_Dispose_m0A6B5CBCEADF845B7B982EE5015B047CE05DA483,
	U3CLoadFileU3Ed__48_MoveNext_m67F0D5B49BAD292E27179406E05B02714FC5F94C,
	U3CLoadFileU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC95F5BF2DB84451A1D5FE72C65C5D526ECCD3771,
	U3CLoadFileU3Ed__48_System_Collections_IEnumerator_Reset_m3F921F33B9753B579FD9B51D27717DF200AD0F66,
	U3CLoadFileU3Ed__48_System_Collections_IEnumerator_get_Current_m83EA6195A84C1BBF72F40406CCDC5509C43E0374,
	InitializePlaymakerAds__ctor_mD653B3674DD735ED852B909508C9AC9EE9C93E19,
	removeads_Start_m9DBC53EB280B692D317CC158B6C7DBC79FFDA535,
	removeads_openme_mDB2BEC05E9F009A65850BADDD2FB1C46FC47B5D6,
	removeads_closeme_m52DABD0C747CC2C8B87EEC7A7948F9265825B865,
	removeads_removeadsfn_m58E85A2F4746574FEF4EA0E1C1927A280CA0046A,
	removeads__ctor_mE08F00E8D152188F9EB9D211B75907F22E9EA4B3,
	ellisrusso_Start_mAA19486AF210433EDEAF04DCC2EF23BACCC363A8,
	ellisrusso_Update_mD5D3CB8AF0F7AF2B5E12AF46E0914B697DE4A94A,
	ellisrusso_chelseysamuel_m167984E96189DC33540E09D482C76F9042079BD0,
	ellisrusso_laurelhester_mB953CB0758D1840D4E94076619E9960FF4A84F79,
	ellisrusso_katinahiggins_mB70F985B9890427FFF4DE57531677868DCA04809,
	ellisrusso__ctor_mC2504CFCAF8B3965DAC3616CB51C7B832625E9DB,
	ellisrusso_U3CchelseysamuelU3Eb__9_0_mEB99490EB2C595B764E23E699B10FF57D74164B6,
	U3CkatinahigginsU3Ed__12__ctor_m221F3CC206C15B2BC10D8A08B0895FB66B77B795,
	U3CkatinahigginsU3Ed__12_System_IDisposable_Dispose_mFA634121E45EF868EBC2199C52FA92A58B65004C,
	U3CkatinahigginsU3Ed__12_MoveNext_mC051514349F400B8307966B9130DCE2AC514957F,
	U3CkatinahigginsU3Ed__12_U3CU3Em__Finally1_m9A6F28CA1D430D0CA31EE9DA588B7DDE99F623E4,
	U3CkatinahigginsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8C92D0F61EED7AB9F884626C64167B813043FDC,
	U3CkatinahigginsU3Ed__12_System_Collections_IEnumerator_Reset_mD4E6345F3CFEDE8C4296771D67A3ACAA38113659,
	U3CkatinahigginsU3Ed__12_System_Collections_IEnumerator_get_Current_mABD1E6DADEB8AEF39AD1F62010EA24785906D2DB,
	juniorrhoades_Start_mDA62011582D8C596421A780FCAC3FE7451D32E87,
	juniorrhoades_OnTokenReceived_m45F91ADF67328210C364B3B6A977C171539F61B9,
	juniorrhoades_OnMessageReceived_m5F9ED1B78E4FAE77618737957DD3ACC18E9A2B15,
	juniorrhoades__ctor_m74967CBF172466397AA576564705DDAA35B1BAFD,
	landonfoley_get_Instance_m65988A568853FDEEFDEDC640ED031936E2E453D7,
	landonfoley_LoadScene_m5EA718931FBA4CAFD6D4F91D97C18FAE7CCF89E4,
	landonfoley_CheckAds_m712F2A46894583FAB9923549A28DDCC0F0923E14,
	landonfoley_Awake_m04CD543EE8881BB369DE234C6796C9680F434BD9,
	landonfoley_Update_m24736DB383A081F5CF56575FC319DE9A591A37AA,
	landonfoley_ShowInterstitial_m145A1C65384417A37C670B0A19282BDE1678DA9D,
	landonfoley_chasityhatch_mDD85148460A80015280E19D40FF058A5E1D8CEEF,
	landonfoley_ShowAdMob_m30C8D4BD783DF8591C09AE3F11DA2F3408FB7805,
	landonfoley__ctor_m602F2FAFE1E64234B5806F62D1C66C00F4F82DDC,
	landonfoley__cctor_mAA998A45FCCEF1CF7A103FF63D11D1E949927077,
	landonfoley_U3CCheckAdsU3Eb__30_0_m4B77574100C0037A433FFF0EF383F919CFEBD0D2,
	U3CLoadSceneU3Ed__28__ctor_m7162211DB904170AF0F6A5E74D4B80CFB6B78BE3,
	U3CLoadSceneU3Ed__28_System_IDisposable_Dispose_m5367329A8D97B43B736AC695FC321F6D14D85859,
	U3CLoadSceneU3Ed__28_MoveNext_m32319F200F9E9B5B67A156539F2513C0E22AB888,
	U3CLoadSceneU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m054113F2BC0364B3927CA7FAB50430951DCF4867,
	U3CLoadSceneU3Ed__28_System_Collections_IEnumerator_Reset_m3F34F1B0A8E155FAB4EECAAD59B1746A4D2487F9,
	U3CLoadSceneU3Ed__28_System_Collections_IEnumerator_get_Current_m5FBDB7EE8B7B753ACA1574DEE492CC05F5ED11A4,
	U3CchasityhatchU3Ed__36__ctor_m6A4A1DCED6165560060269BC8531562A78267FAA,
	U3CchasityhatchU3Ed__36_System_IDisposable_Dispose_m61197209C1E485F52B43B86BC84DB4D7A6424C44,
	U3CchasityhatchU3Ed__36_MoveNext_m031F182BA9A449ED40C56C22D8EE2705226BC110,
	U3CchasityhatchU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE67C8305270CDFF3269681DBA5E1876625307B6,
	U3CchasityhatchU3Ed__36_System_Collections_IEnumerator_Reset_mB7DF68B57F7FFEF4AB96CC71CE421CF4C01136EA,
	U3CchasityhatchU3Ed__36_System_Collections_IEnumerator_get_Current_m3C0CCB71185D504B1A3413E702557B760E139842,
	shanerodriguez_get_instance_m8CD885A0840AB96677E119E8E2D907F4C179CA12,
	shanerodriguez_Awake_mFB2531ECE668171780C046C4B9A1D26B0773CE92,
	shanerodriguez_Start_mF3AC9066A78C4C7C2718B4C2451F55482286041D,
	shanerodriguez_christymoreno_m3A79CE029E97AA79E4CB54C1212D9C8615A559B7,
	shanerodriguez_monasuarez_mAC08D69CA3954CAEE4E50B05EB8FA9E8EF9584E4,
	shanerodriguez_lakeishakay_m0C69A92CB90E50B3A8EC33A1CF34E3023DE3174A,
	shanerodriguez_elisehaynes_mD75C8FCC19BECD6F5BFC6435309F22CCB8AB1654,
	shanerodriguez_rosalindamorrow_m2E49D25FA62C0466B858B3B8AD1AF215B7752DDD,
	shanerodriguez_candyhendrix_mDE3BC0FB742D5DF346BB1F210CF73962076AE297,
	shanerodriguez__ctor_m7BA03EF536646ECB0A82C19086D0AF24FFD915DC,
	albernal_aimeepratt_mA3C19D1BE13370DA4BDBE132923131D736479D65,
	albernal_deeedmonds_m3EEC312B36AA51DF87D0ACBD5052E31AE7E57DA8,
	albernal_luannratliff_mD2BA86DB037419A6FA999F7204C1397069071669,
	albernal_Update_m979A1AE239CE94413D39C084C8F8F9B9717F9133,
	albernal__ctor_m084C0E0B1005227D96971C8DB3C15B36DED8EBB6,
	aureliolyons_Start_m5E233214205A0728466C12ACBFB4D72D9B9E2829,
	aureliolyons_gloriadowning_m3397E9E6E72C319574A6657A7786111127CAC702,
	aureliolyons_Update_m25903EA39E2E3B14E741CB8530F92470A1F8DC6D,
	aureliolyons__ctor_mFF683DBF2F02F616E6DB53B36271F903CEA95F50,
	clintrizzo_Awake_m85CEB8EF082940B4CADA0E49C8FC3C22CB322A15,
	clintrizzo_Start_m077FA0AA9C532EBC5F7D0C6458725B99822ADD36,
	clintrizzo_bobbieestrada_m6723CD2FF581BF917844DAF92874ECA5E4CFC10F,
	clintrizzo_vickiramey_mD71DF03683E5100B4B9A665782EB5FEC6225229C,
	clintrizzo_maratrevino_mE488B02D4F651E21490A5F8850DF84E3FED737E3,
	clintrizzo_Update_mA8355138C2F8404A6D63B94B6E4B2D18A96EBC8D,
	clintrizzo_britneychilds_m599579AB0C0CC3457C77ACC74F12DB56107CCC26,
	clintrizzo_terriecrump_mD059A6409B47FDB133DBA4C3864D29BC6B559F97,
	clintrizzo_kenyahubbard_m05E6927B069FDD94105680A366979A27BDA2896B,
	clintrizzo_inesleblanc_mCEE487E09A11F52681FF897E126AB57285BFAB1C,
	clintrizzo_peggyalbrecht_mF263101B35585AFEA8E255A758A10CB03D9411EA,
	clintrizzo__ctor_m6449B90CA0C335B742F1D62F43DBD10A6E68367F,
	colintobin__ctor_mEF3FFA10311D668D8EBD701E326163D3BBD86ADB,
	darenprice_get_Instance_m0F2502836C5A89B889A3286639EF6470C40D9F72,
	darenprice_Awake_mAB50D272DB9C8A2513EF11766C66CD82E17B2D54,
	darenprice_Update_mCB8A23E3D7D526CECB466B075A711FA1767F2872,
	darenprice__ctor_m12C34ADA2664E12BFF01E418BDA285297E338B8F,
	Data__cctor_m30933DFE5404DED610EC6026AEF0BFF8DFC90961,
	eliortega_inesleblanc_mBE96BEACD2B7F1D066B0BB5DF20279BE8F42BF70,
	eliortega_iklan_mEEA8EF22690F8E50A1EBD4561046E9B986092132,
	eliortega_peggyalbrecht_m6A4EBF65E44C1D523747E4714211BDCBF9C88565,
	eliortega_gakaktif_m3D815580B91B9B47E80274D750DEC66F076D96C2,
	eliortega__ctor_mBB741ACDF0503CDAA74A26177F122340290ECBFE,
	fredrickhightower_Update_m13D00C598D28E5E23C6E99FD9C96EC7C2C933F34,
	fredrickhightower__ctor_m1A478A473AEC86DCA6D3FD4D5E15ABBF7D9B1219,
	iraskaggs_Awake_m3CDBA28555EAA354ADEAE698F304696D6F809EFB,
	iraskaggs__ctor_m90EBAF82385585E082E263E7D1D669576C820C73,
	jaimeescobedo__ctor_mDAE2EE491B3A48599084F086E95781A9B11FD460,
	reubenvigil_alejandrayee_mCD60D72DBAF2D7FF585570D0A28DB8CD5E62FB20,
	reubenvigil__ctor_m04F1C41528B0BF2471CCDFBA578A8CBC9360E605,
	ronaldwood_get_Instance_m5058552BF11FD3BE672C528CC856A3CACE7E1181,
	ronaldwood_Awake_m37B71210FAA5471C7C1B206B12B5BD9ACAD176F1,
	ronaldwood_Start_m8C68BF73AA2426FFCD4E82411634C1C1C64585CB,
	ronaldwood_ernestineserrano_m06E55A532B067B2001FDD8600E683DE8F2635379,
	ronaldwood__ctor_m95243039B12D7CFBA72E0B0B19C693D527A28D62,
	vincentsteele_Start_mF5797E3ED06599D6D254F6440EFB5E5C8817A3EC,
	vincentsteele_menu_m62EB2BD94214E2663A6B78566FADE3E29548A4DC,
	vincentsteele_WUI_Open_mC9CE9D0663CE98781DA402B90448E1AFEC5243C1,
	vincentsteele_btn_No_mD2CFC09D840CE88C81A3C0AE9335B2D8D7E5A725,
	vincentsteele_btn_yes_m8220F1F51BA9512AAC93AAE30692530B92E07C14,
	vincentsteele__ctor_m212071F132B945EAA6A619363570FA7F6CCDF17C,
	stevemyers_Start_mF4F2D049F003CB624AE53390E171DC644A26C20E,
	stevemyers__ctor_m082F8E57D18E0EB9C8384B224D9A89DBE86D669B,
	ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7,
	ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46,
	ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722,
	ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719,
	DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F,
	DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B,
	EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435,
	EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9,
	EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138,
	U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C,
	U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m3EF23BF40634D4262D8A2AE3DB14140FEFB4BF52,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA,
	TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9,
	TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0,
	TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3,
	TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D,
	TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3,
	TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C,
	TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82,
	TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5,
	TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C,
	TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3,
	TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908,
	TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427,
	TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D,
	TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF,
	TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052,
	TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1,
	TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A,
	TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6,
	TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189,
	TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475,
	TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33,
	TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6,
	TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02,
	TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB,
	CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E,
	SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963,
	WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2,
	LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2,
	LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC,
	Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9,
	Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4,
	U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D,
	U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m553F892690ED74A33F57B1359743D31F8BB93C2A,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD,
	Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA,
	Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB,
	U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC,
	U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC9F90586F057E3728D9F93BB0E12197C9B994EEA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A,
	Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29,
	Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1,
	Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D,
	Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6,
	Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90,
	Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27,
	CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788,
	CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5,
	CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F,
	CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F,
	CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878,
	ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72,
	ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695,
	ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A,
	ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371,
	ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88,
	ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56,
	ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B,
	U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71,
	U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1C76BF8EAC2CDC2BAC58755622763B9318DA51CA,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA,
	SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8,
	SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE,
	SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807,
	SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93,
	SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690,
	SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB,
	SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362,
	SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22,
	U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51,
	U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mB6C5974E8F57160AE544E1D2FD44621EEF3ACAB5,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5,
	TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D,
	TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6,
	TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0,
	U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2,
	U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9B7AEE80C1E70D2D2FF5811A54AFD6189CD7F5A9,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8,
	TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C,
	TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39,
	TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95,
	TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689,
	TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A,
	TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52,
	TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1,
	TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025,
	U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B,
	U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mD12057609EFCBCA8E7B61B0421D4A7C5A206C8C3,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186,
	U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5,
	U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mE5E0678716735BDF0D632FE43E392981E75A1C4D,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E,
	TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2,
	TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE,
	TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E,
	TextMeshProFloatingText__cctor_m272097816057A64A9FFE16F69C6844DCF88E9557,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_mD3C24C6814482113FD231827E550FBBCC91424A0,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m83285E807FA4462B99B68D1EB12B2360238C53EB,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m588E025C05E03684A11ABC91B50734A349D28CC8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2412DC176F8CA3096658EB0E27AC28218DAEC03A,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mCCE19093B7355F3E23834E27A8517661DF833797,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mE53E0B4DBE6AF5DAC110C3F626B34C5965845E54,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m1ECB51A93EE3B236301948784A3260FD72814923,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m461761745A9C5FF4F7995C3DB33DB43848AEB05B,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m1FC162511DF31A9CDBD0101083FBCB11380554C4,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A5E330ACDAD25422A7D642301F58E6C1EE1B041,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_m5A7148435B35A0A84329416FF765D45F6AA0F4E1,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m066140B8D4CD5DE3527A3A05183AE89B487B5D55,
	TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66,
	TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233,
	TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C,
	TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839,
	TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063,
	TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1,
	TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A,
	TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C,
	TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A,
	TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079,
	TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723,
	TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43,
	TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E,
	TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6,
	TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE,
	TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215,
	TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79,
	TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559,
	TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779,
	TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF,
	TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E,
	TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797,
	TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B,
	TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A,
	TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503,
	TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE,
	TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87,
	TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B,
	TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE,
	TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF,
	TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98,
	TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65,
	TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97,
	TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398,
	TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301,
	TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491,
	TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F,
	TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36,
	TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848,
	VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1,
	VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B,
	VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC,
	VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m319AC50F2DE1572FB7D7AF4F5F65958D01477899,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C,
	VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C,
	VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354,
	VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2,
	VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76,
	VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8,
	VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88,
	VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D,
	U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m15291DCCCEC264095634B26DD6F24D52360BDAF0,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9,
	VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867,
	VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304,
	VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23,
	VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E,
	VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77,
	VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38,
	VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB,
	U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m2F84864A089CBA0B878B7AC1EA39A49B82682A90,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B,
	VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E,
	VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21,
	VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5,
	VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1,
	VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D,
	VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F,
	VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198,
	U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5A5869FEFA67D5E9659F1145B83581D954550C1A,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0,
	VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0,
	VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47,
	VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47,
	VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB,
	VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7,
	VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0,
	VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422,
	U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A,
	U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDE5E71C88F5096FD70EB061287ADF0B847732821,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C,
	WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F,
	WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809,
	WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF,
	WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98,
	WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795,
	U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53,
	U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m07746C332D2D8CE5DEA59873C26F2FAD4B369B42,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19,
	AdSettings_GetAdvertiserSettings_m1E77A663C1F24B189AB61CE7558ABF9DCF78B132,
	AdSettings_GetPlaftormSettings_m5A8699B4ACC0E748222401F828A7F16913FA1A9E,
	AdSettings__ctor_mF0E5BEDDC27E4EF89F7C557645C3A4CAAAE13B1F,
	U3CU3Ec__DisplayClass12_0__ctor_mB1BAFA0E455B857F037E6260A9198E1D1B6DABF3,
	U3CU3Ec__DisplayClass12_0_U3CGetAdvertiserSettingsU3Eb__0_mF478B3EC97BF15937A2F1EF63B1D57D0CA8047D1,
	U3CU3Ec__DisplayClass13_0__ctor_m106E944E072268975F43D8F18F0D90F02F68B41D,
	U3CU3Ec__DisplayClass13_0_U3CGetPlaftormSettingsU3Eb__0_m5DDCE227765A7FBADC4419A599AB2D57D90BF930,
	AdTypeSettings_get_Order_m7D8BB39251BDE15275517EDDBF3927AF23FB32FC,
	AdTypeSettings_get_Weight_m6878581FE9D5AB11CF9D726E6A63F66F4DC5D8D1,
	AdvertiserId__ctor_m17D085AA1BD7D48FC86F61F7968E7D3E5DEC66B9,
	FileLoader_LoadFile_mDAFF2DE24948C9852577D2475240FDA399D4DB16,
	FileLoader_GetResult_m17165BAE89943A448A435AC18A79DD19B203309C,
	FileLoader__ctor_mC9963743FC9A76090B396F79A2613464755311FB,
	U3CLoadFileU3Ed__1__ctor_m970A3C6651804EABF68C71A9DFAF3992F663E737,
	U3CLoadFileU3Ed__1_System_IDisposable_Dispose_m8CB2DB10C98042C306DB5A8A1A72712139448952,
	U3CLoadFileU3Ed__1_MoveNext_mC47A3C5A697332862F015CB094302E180F803AAF,
	U3CLoadFileU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m568A7BF4EED5DEC7C2971C6D316803C56164C93E,
	U3CLoadFileU3Ed__1_System_Collections_IEnumerator_Reset_mE9AB34B42AE6397682F53FD2589818713774BEE5,
	U3CLoadFileU3Ed__1_System_Collections_IEnumerator_get_Current_m2A7F302E4AF8406B87F84CB59976566E6019AD0C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MediationSettings_GetAdvertiser_m64A2F2780B9BE42FD795383211072AC425D84A91,
	ScreenWriter_Write_m069355DA11F6B219BF69C5588060436B25B22944,
	ScreenWriter_OnGUI_m63D71634A254C362853678A686E5864E0CD80AF8,
	ScreenWriter__ctor_mA93415325844260A0C9B020F760A90336FC2B747,
	CustomAdColony_HideBanner_m67C77B89F76742D517E9CAC0EE6954CF33692CBC,
	CustomAdColony_InitializeAds_mAC559C88959F13937E8942C3E006034AC61D0280,
	CustomAdColony_IsBannerAvailable_m55202EA495D407A45D9BE123C1B9122BA110D0F7,
	CustomAdColony_IsInterstitialAvailable_m3728314E8872CA431B97C0A3298AB6F772827942,
	CustomAdColony_IsRewardVideoAvailable_mD23ACB4AB27DC04A3744A1C3835B56F7C0007738,
	CustomAdColony_ResetBannerUsage_mE2E1F4F3276223B6639C9889B6A40C1B7482AF83,
	CustomAdColony_BannerAlreadyUsed_m28C96185DE0DF642940A144A6F205FD3E366EA84,
	CustomAdColony_ShowBanner_mA5F76CF6CD1300D919F380FD58834050E6A381AC,
	CustomAdColony_ShowInterstitial_mD85A33BF0489ED7E3E5794224C67383994386C4C,
	CustomAdColony_ShowInterstitial_mAC10A520CE633A38B1702579A05855641EE47720,
	CustomAdColony_ShowRewardVideo_m1211295731298D34D73AB1F8A1F3993F4DAA9474,
	CustomAdColony_ShowRewardVideo_mB1B41D547286E7C02E6121D6BE3DA199B9C843DD,
	CustomAdColony_UpdateConsent_m7B52C3E965D0AE34D43CB93029147391DE238259,
	CustomAdColony__ctor_mB424FD71AAFF886B8C7F256A8D9430FDE15FDB55,
	CustomAdmob_InitializeAds_mD94633617C19A4410D925E5FFF2E98AE05206FEE,
	CustomAdmob_IsInterstitialAvailable_mE5BC6944670D120A567C98610A04FD62064667B2,
	CustomAdmob_IsRewardVideoAvailable_m0159130F2333318FE135E94A699FC8C6A0182E53,
	CustomAdmob_ShowInterstitial_m5F4CA0EED18DCD7D2F069AC46A65455472FE660A,
	CustomAdmob_ShowInterstitial_m096E97FBFC86DB92A23D03C943B9F31DDA9CABEE,
	CustomAdmob_ShowRewardVideo_mD73E1D238761E8D2A2712B5A1763DC718949A917,
	CustomAdmob_HideBanner_mE6A838E0DD344400C6FB35C589DF5489047F03DB,
	CustomAdmob_IsBannerAvailable_m9888FCC5F0043970D55AFB2023AF8090AD9579D4,
	CustomAdmob_ResetBannerUsage_m39DA0BD00AFC29B1990F4AFAA4C0E2335D684DA5,
	CustomAdmob_BannerAlreadyUsed_m9B9CAA130322C5593567A4F4FF66B25FE9EAD499,
	CustomAdmob_ShowBanner_m69DB8B4C5ED7391C765B54E927BC8186E87C938F,
	CustomAdmob_ShowRewardVideo_mA5DF20A3FD9A6AE0C17AEDA5A479719B08E3C430,
	CustomAdmob_UpdateConsent_m047AB4CE80E08EFA72E7D302659101CE124C4E81,
	CustomAdmob__ctor_m579D71179E7AFAB559F57EB2F1EF7384E8FACE85,
	CustomAppLovin_InitializeAds_m0BB3CD522BF6726FC7B7F6E67028ACC1E200191D,
	CustomAppLovin_UpdateConsent_m30BE118B1AEEBF318A0A464F6EDD24A59BC719A6,
	CustomAppLovin_IsBannerAvailable_m2F5F25216EB9EEDCB759AE834197EBF596FA7379,
	CustomAppLovin_ShowBanner_m0E50800F94F991D0E9212026F175C5CB9C2E57A8,
	CustomAppLovin_HideBanner_m1D5209AFA4EB1DB8373E385FB563A14CBC8E070F,
	CustomAppLovin_ResetBannerUsage_m2601123941B452988E9B8F5557880259BC27342F,
	CustomAppLovin_BannerAlreadyUsed_mC1998E02D0CA95ACAECD731D0D2E8341AB453078,
	CustomAppLovin_IsInterstitialAvailable_m79BA5B3F3693EA770F91EC8D99CB7BD32C907737,
	CustomAppLovin_ShowInterstitial_m8C6B3C7CCA6DE40B82D6FFD271CFB9E6A02557AA,
	CustomAppLovin_ShowInterstitial_m9A137650350497EB33FC36B46CDDAB2B9226DEA6,
	CustomAppLovin_IsRewardVideoAvailable_mF9AF2DEC963A890ADEBDF1434BF7D10929695565,
	CustomAppLovin_ShowRewardVideo_m7A6D6AA733FED4DC53CEB45D2EE8BB10647605D5,
	CustomAppLovin_ShowRewardVideo_m2E187E6A02874B3A45BA33B30E181D1F0963BBA7,
	CustomAppLovin__ctor_m30A6378ED182F9C5183C8EECF4775AC39ED9390E,
	CustomChartboost_HideBanner_mCF05919C7039563916824B384567050E70E720BF,
	CustomChartboost_InitializeAds_m13AA8E4F09F872D689E7DD3FAFA4B0C91F04270D,
	CustomChartboost_IsBannerAvailable_mA4BC1381E83A057E0C5AE10DE42A99AA872EFD59,
	CustomChartboost_ResetBannerUsage_m809A953549381A94686F4D24439A08D6E5FC967A,
	CustomChartboost_BannerAlreadyUsed_m2C51D9FB9B14E4E4712CE4BFE6745C5D256F6A39,
	CustomChartboost_IsInterstitialAvailable_m9EB774903EBA3B09D1DCB751B834F683A1548B76,
	CustomChartboost_IsRewardVideoAvailable_mCD06B13782196EC0EC77E4BFFEE1224E85CFCF5B,
	CustomChartboost_ShowBanner_m7FD4E320FEA43DBB06EDE795CEE8F2FADA74DF8C,
	CustomChartboost_ShowInterstitial_m15330E7DB53F7CEA0314882168A30C85775CB836,
	CustomChartboost_ShowInterstitial_m7AB171DDA8F7850431519C3E5DBC10EC21228060,
	CustomChartboost_ShowRewardVideo_m112711ED7C93B0DDE8481FC817447C92F53B3DF8,
	CustomChartboost_ShowRewardVideo_m4FFACF4951DC2413F44EB9A4EF2E83979A5E6216,
	CustomChartboost_UpdateConsent_mBC54F1F4E6EEAE01414B3D148012F0F97762C78D,
	CustomChartboost__ctor_mE73B46B5BAB0E42557D1F23CF2069F1D1CF54D5A,
	CustomFacebook_BannerAlreadyUsed_mBB7EAC381AD5CA798C0778A25E919812C5ED732F,
	CustomFacebook_HideBanner_m82178BDF66D1FEB158D6F2B1B23FE74FD9976CDB,
	CustomFacebook_InitializeAds_m9E930892F1BE1D6CFE0C38B6F1F0A9D84D233482,
	CustomFacebook_IsBannerAvailable_m1CDBEFC83CFE385542F74C6A7A7074FB914F2057,
	CustomFacebook_IsInterstitialAvailable_m541DF91EA4D5F5DF7B953EEEC9C8294935373804,
	CustomFacebook_IsRewardVideoAvailable_mC6B9F6E4AFDBF242D037427DC611D79C56582EDB,
	CustomFacebook_ResetBannerUsage_m167655BB42B8C41DC54A995B1E43312BB6F5A52B,
	CustomFacebook_ShowBanner_m1ED7F4F8821FE5065975484CEEEA4996DDA85581,
	CustomFacebook_ShowInterstitial_mC3280B1966245225E21666E109344FD24F9B510F,
	CustomFacebook_ShowInterstitial_m90F1CD5D16129CA0F70F64BB52BFA31306CD2D6C,
	CustomFacebook_ShowRewardVideo_mFF3AE431E7279BB711A8537763E49D6265900348,
	CustomFacebook_ShowRewardVideo_m46EC138CC20BDD67A5840DB0EECDF50668DED0E7,
	CustomFacebook_UpdateConsent_mF5ED957C5C108E53AC5E8C23A53E3F742EC8957B,
	CustomFacebook__ctor_m1EB10359D2E1E52280C7FDD1CDD277ED18CCDA6B,
	CustomHeyzap_HideBanner_mC51C233CFCDB7549ABBFF5297E5EEDC85252E03F,
	CustomHeyzap_InitializeAds_m70CD061B5D5E08B3EB255039133BDA1CCBC9A8DB,
	CustomHeyzap_IsBannerAvailable_mB306F799769A2C3B87A1DB77EB30A0963D454939,
	CustomHeyzap_ResetBannerUsage_mAEE75F74D364A7D0006B87EA1BAC0C02C9B2F409,
	CustomHeyzap_BannerAlreadyUsed_m4C68730C2A12C2DF3ADCD8982CD585363BBCCE75,
	CustomHeyzap_IsInterstitialAvailable_mC2C20599E12682C72F11EEC2D0CB821E2ACF49D6,
	CustomHeyzap_IsRewardVideoAvailable_m4746B157E86BAB6E3B9763ACBD90AEB8DA4E6041,
	CustomHeyzap_ShowBanner_mC121591CF89DF5044A54AE30A443C0C80CEB6788,
	CustomHeyzap_ShowInterstitial_m71661788F1513FF5C3EAA7FDE2201F24777CC561,
	CustomHeyzap_ShowInterstitial_mEED68C3F969F59269980FC41CB44B9D5665FF6CD,
	CustomHeyzap_ShowRewardVideo_mC3A0B071A8A71E2E33674ACBBA2974EB4F83F7E8,
	CustomHeyzap_ShowRewardVideo_mB2744537F37DA9FBEF69F779C8F6679FCE0A43A9,
	CustomHeyzap_UpdateConsent_m5D859125EE517A053ADE1D7DDBC71E53A4FA7794,
	CustomHeyzap__ctor_mD06C0AE8A9BC7CF13E201E317A5E3F05EFA51BB7,
	CustomIronSource_BannerAlreadyUsed_m5200D6AFCF21E47A94ABCCD375A621B41D024646,
	CustomIronSource_HideBanner_mDEDCC1436C331E5AA2209D474C5A46021DD79E02,
	CustomIronSource_InitializeAds_m05E00D52BF0D5C5A523D6E963B9FEE108DA57230,
	CustomIronSource_IsBannerAvailable_m1BB38927A86FB665C02897B1B261FAD6A1252E05,
	CustomIronSource_IsInterstitialAvailable_mC384A984D44916F43473056CA69C046BE19264D7,
	CustomIronSource_IsRewardVideoAvailable_m56EC0AB71C0E7D4AA13D6FBB009DEFB853A7EF95,
	CustomIronSource_ResetBannerUsage_m62B6E25A645DF6B2FDE5CA780DAB0849E0C6E43A,
	CustomIronSource_ShowBanner_m4FCC85857ED07A37F26F3D35C3228CD0311C09F3,
	CustomIronSource_ShowInterstitial_m8F8B63159225E947756F1F51D081E3B8D8FF2873,
	CustomIronSource_ShowInterstitial_mC7F8425FE69CF8465F01AD4883DBF11F2757EC74,
	CustomIronSource_ShowRewardVideo_m8D4561F5039FC9DC46FA2E93AD867684BA3FC287,
	CustomIronSource_ShowRewardVideo_mF56231EE7B84F7C65A58D1F787CE32513F03F6BD,
	CustomIronSource_UpdateConsent_m95A3688C073195CDEFF8F2EC62F3A38B924A1BC0,
	CustomIronSource__ctor_m2534961FF616A935C5E3E4DF41858B9EEA5F0683,
	CustomMoPub_BannerAlreadyUsed_mE47384CE8C7FD0968BFEA1C06840977BEE253D3C,
	CustomMoPub_HideBanner_mBF4E8DB48335DDD5E4F957ED306ACF245CE83BCC,
	CustomMoPub_InitializeAds_m8922631807A2B7247C47C365D68A74BDCFCFA05B,
	CustomMoPub_IsBannerAvailable_mA8025F7C6C0AA2B673A3CB989B1AB0B5BB9B178A,
	CustomMoPub_IsInterstitialAvailable_mDC65CC333362BED3B5F4BC0B55F7998C89DEF9D8,
	CustomMoPub_IsRewardVideoAvailable_mACBA0298B71F1A9DB9B9AFB1425AC4BCAB5B9609,
	CustomMoPub_ResetBannerUsage_m210E3DEEB1CA7CE2D07929F64E7C71BCF816B097,
	CustomMoPub_ShowBanner_m503DEE7AF9581F89D76BD8DC34E1FAF492DDEB0F,
	CustomMoPub_ShowInterstitial_m7989DEDB8A65ECBA8F2EE8CB4EC193AE2E6BA715,
	CustomMoPub_ShowInterstitial_mA949C1605C5DEB2F009641DF58D6570BA5DABAB9,
	CustomMoPub_ShowRewardVideo_m56C1688567736F1392DEF53F6B66CD6834803AE1,
	CustomMoPub_ShowRewardVideo_mF4F8783B111741F7D0271551A42E9E90FFD38F3F,
	CustomMoPub_UpdateConsent_m70153BFB4F0291458E39E37A1779D98F6EFD98CE,
	CustomMoPub__ctor_m966364C41B91C225D0A267263294F5B6A7D317D1,
	CustomUnityAds_InitializeAds_m7EDB62658A42799F791F11758DC0D7EBE9151825,
	CustomUnityAds_UpdateConsent_m5B308531B63CA201A803BBB37E5D104339D51F74,
	CustomUnityAds_OnInitializationComplete_m54A63A944BA0E473F680DC5DB3D78D3F746B7C42,
	CustomUnityAds_OnInitializationFailed_mA17CEE610E200C2292488EF29B3C56AF16AE3E81,
	CustomUnityAds_IsInterstitialAvailable_m9AEADFED526D379C65FABBF16778FF36BD013BDE,
	CustomUnityAds_LoadInterstitialAd_mA5495BA7D84931A1707E51BB2BB696D167E73CF6,
	CustomUnityAds_ShowInterstitial_mAC0BB9A1ED0B2E25EA7F274906B6E7A77259F9F0,
	CustomUnityAds_ShowInterstitial_m2438EE3456BE7EE236B61492E2492E0D2D72A36A,
	CustomUnityAds_IsRewardVideoAvailable_m5E45604CC646DD9E26E297C79C48BC0A29D88558,
	CustomUnityAds_LoadRewardedVideo_m51A3E35695493DBD7AD1C1B7C3F6C1A52882B8B0,
	CustomUnityAds_ShowRewardVideo_m9CAF177D09CCC9E941DC26065BEEC04F040CC20F,
	CustomUnityAds_ShowRewardVideo_m306596F70EE30024436E86CEB840B27565ACE456,
	CustomUnityAds_OnUnityAdsAdLoaded_m2FEA529318E8EDB0602F2B96519EBC836DC8C7CD,
	CustomUnityAds_OnUnityAdsFailedToLoad_mCA36574A6E4F033770B7E507D1470ABCE68BF840,
	CustomUnityAds_OnUnityAdsShowFailure_mF6A11A2837FB31A46B9CCCFE5C97D391DB8A77D7,
	CustomUnityAds_OnUnityAdsShowStart_m914DC9FDE3999866F50C0E02AF15FA460D599869,
	CustomUnityAds_OnUnityAdsShowClick_m1EF62371F2374020AE559055B797F8BC934F6DB5,
	CustomUnityAds_OnUnityAdsShowComplete_mDCC998C3000DE03C5C02548E681BED22C923DE82,
	CustomUnityAds_IsBannerAvailable_m87D5C224168BC39A63B019020DC28749309DDB5D,
	CustomUnityAds_ResetBannerUsage_m96E1D790E6CDB971907914D3D1C2A80819B13EF6,
	CustomUnityAds_BannerAlreadyUsed_m1C15E006270341620FAA97327BF982024E6DA956,
	CustomUnityAds_ShowBanner_m7F85B13044939763C6A664F09763346EB376F550,
	CustomUnityAds_BannerLoadSuccess_m52CBCEB7B964B2186F26633752858799688AA259,
	CustomUnityAds_BannerLoadFailed_m1ADB483C0759DFBFF6682C6068F556E4E659BE2B,
	CustomUnityAds_BanerDisplayed_m3E2F8D295B5D0AF0E1198CB4C308EEA8C2111D0C,
	CustomUnityAds_BannerHidded_m8A71C0D1741BE0662EC11B6138AFD8D54B92B236,
	CustomUnityAds_HideBanner_m7EA9F46F93D8624C0A55A4185944B5A7236C5EBC,
	CustomUnityAds__ctor_m89D1595DC90A673AF78961A433344067F15FB00D,
	U3CU3Ec__cctor_mC50B844DD3247AD830FAB618C55697412ED20CED,
	U3CU3Ec__ctor_mCB38407397A4908EDEBDD0D0FCFF3FDF69727FB2,
	U3CU3Ec_U3CInitializeAdsU3Eb__19_0_mA30A1CEFD7AB06B23A48FA9C4F25F09DD970893B,
	CustomVungle_HideBanner_m5A6EB2EEBE04D84A478EC6D9BD889B0C70D45BF2,
	CustomVungle_InitializeAds_m8B26BB356972E521985A11DAD40A4324C8C36F7E,
	CustomVungle_ResetBannerUsage_mF81AD6EFB3C29E4D81358CE26DCD96116BC21D41,
	CustomVungle_BannerAlreadyUsed_mA723D513D6DBB72E967D1535B0500F5555CAE5FB,
	CustomVungle_IsBannerAvailable_mA51536DC2E4913F94A9E322104BF4B9E2116885C,
	CustomVungle_IsInterstitialAvailable_mC6DC043A03C68CCE7CAF3FF6E48FE97013685E0C,
	CustomVungle_IsRewardVideoAvailable_m6D82CDB3D74DBC9C754236C0A4005D6BFAA17CD5,
	CustomVungle_ShowBanner_m0244A5BD9B0EC0BC1469A3D99BA4131093788AFA,
	CustomVungle_ShowInterstitial_mB50AF8F6D184EACFDECFE293C1211022B9926094,
	CustomVungle_ShowInterstitial_mF1C7FA63E5498F0FD5181F88B2B2E244FBD492C8,
	CustomVungle_ShowRewardVideo_mE785558EC0FA9153A5621AE68B0B5BBE13755AA7,
	CustomVungle_ShowRewardVideo_m73ECBFD7962E4F176AD18C3F4286F568601FFFA5,
	CustomVungle_UpdateConsent_m7930EBA34A2FEC778851DCFB9555EB3AEF7DF072,
	CustomVungle__ctor_mAD2F0189FD486D9EEFA057B653822E27E1B40ECA,
	AdOrder__ctor_m04AB006BBE638B915DB3B5D02BC1AB12757444D7,
	AdHandler_ExecuteOnMainThread_mB5A3D62F559F7722558B4D2910E78E82CEC9DD8D,
	AdHandler_Update_mDD20B10318EF0F298A2C38D074752307240935DB,
	AdHandler_RemoveFromParent_m4227A457574B9EED99906159CCCED8EA84A713EC,
	AdHandler__ctor_mEA69C635E80E5F942993D8EEE06C28CF1E4B9503,
	AdHandler__cctor_mF1FB384195F295BB6CF7E37277844F40887505D8,
	AdSettings_SetUrlPrefix_mD9CD316170FECFE681B7A1A3AAC12D80FB173567,
	NULL,
	AdSettingsBridge__ctor_m167DBFC3BF6DA8D8B7B689EEDCA061B9D46EFEF3,
	AdSettingsBridge__cctor_m40590CBE42B65388696A19E86811CED2DD1284E9,
	AdSettingsBridge_CreateInstance_m65495959DF4344EA50B936DD51A9E3F70D6E46D3,
	AdSettingsBridge_SetUrlPrefix_m259B2820C3B9A4145140C7F48A2BE896A2E6DFA5,
	AdSettingsBridgeAndroid_SetUrlPrefix_m08FE29A152BE1723AC91501562A8B3CEF6D31AB1,
	AdSettingsBridgeAndroid_GetAdSettingsObject_m3AE4E6BB1C80066D4E32FC15A90E2B881FFCFB9B,
	AdSettingsBridgeAndroid__ctor_m1F64478199F03FC0E03D4CEB1E0CC2B8A5FC5D4C,
	FBAdViewBridgeCallback__ctor_m6710F53F2806B18989E0D5E0735DC79AE7995603,
	FBAdViewBridgeCallback_Invoke_m6A30B46359E32849493AC52F3A4A28B5A18E62D7,
	FBAdViewBridgeErrorCallback__ctor_m10F4B984486537D850D8CE04A02A270BE7D461B8,
	FBAdViewBridgeErrorCallback_Invoke_mC305B139F7C2B563BD839A976F5226A7C6501C32,
	AdView_get_PlacementId_mEDE23B31999A8FD01DED8BAEED1BE1928899E273,
	AdView_set_PlacementId_m19FCF6CE81E080B58DFC68AEAA56B98E5971432D,
	AdView_get_AdViewDidLoad_m9A5FDB200522996BFA60437A71139BF6E8E3F925,
	AdView_set_AdViewDidLoad_mBD127C9EF81C596B73E4C4343A062E2717FFDF98,
	AdView_get_AdViewWillLogImpression_mCFAF35C01F2ED04BAF6007FE5EB4D63FC0DF0B49,
	AdView_set_AdViewWillLogImpression_mDE32879CEBEC50C7342EA1240CE30214B520C029,
	AdView_get_AdViewDidFailWithError_m4B5A5230BF887E968F26F012F4D606BC94B17378,
	AdView_set_AdViewDidFailWithError_m30469B5CAE7CEE6B40B1B357FB2D3C92FAC5BC79,
	AdView_get_AdViewDidClick_mE7E5F6BC71E6F33E7E321E3D463E18C009AD1609,
	AdView_set_AdViewDidClick_mADFDE1A9F8C233CB8C15EDB76E0E5F2FC972EA42,
	AdView_get_AdViewDidFinishClick_m9799B996F1564840DC99570DF3AAB4F75CCA28F5,
	AdView__ctor_m46F11F5DDA003405240B3AE57EE3553E38BD78E8,
	AdView_Finalize_m0426F0F3B7D9DE2A23CD69DCE7B93DCD12FA4F3F,
	AdView_Dispose_m2463F1476326B80455743D0D1D5FF32842E51D7A,
	AdView_Dispose_m4776B74E40250D31A3475FD0F8C331BDF8CEF0B3,
	AdView_ToString_m351063BB697CB0B749ADDEFEAC97965BDD668BC7,
	AdView_Register_m0C63ACD6BBF4CD7724E1528D58B4680A932C03CC,
	AdView_LoadAd_m166AC4D5EB49FF2061D7E0F06BAB11BFFB5C6006,
	AdView_IsValid_m81A2AC325D900213AE176732B4321B7A485A21C4,
	AdView_LoadAdFromData_m3E40E0C4ED9411A654EDFF5E386A1A8157F21CEB,
	AdView_HeightFromType_m2662ACAE43FB8A763CFFFEA9F39785B51D24F80A,
	AdView_Show_m9E4B30BA17205CBFCB1B6EDB84C35631E41FD63C,
	AdView_Show_mB4616203352A95F93F7819ECF35848E35825F3E8,
	AdView_Show_mCEC1DD5DC80D5D157DEDE4F076DB4F92BB39A4E1,
	AdView_Show_m0CC5324E36CDF6938F768FB77DB1D6292C0DE36E,
	AdView_ExecuteOnMainThread_m6F15C2D493BDBF814BA82FD3CE41916EE9CA7566,
	AdView_op_Implicit_mC7FC0D167FBE703863652F01D91F2B06D52173AF,
	AdView_U3CLoadAdFromDataU3Eb__37_0_m94E18D515027B305FDD5C96270B407DCEF7F5400,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AdViewBridge__ctor_mF0D5C16A14D9B72FE4F3CE7E31218A45E605C442,
	AdViewBridge__cctor_m109B52BBADD726870D53DD87168BAF7E9DF75290,
	AdViewBridge_CreateInstance_m83986ED51584DF6E056B91340B99FF2CDAA12069,
	AdViewBridge_Create_mBE61431F616EAA9DA324937C71165A8EC1AC2B2B,
	AdViewBridge_Load_mC114B465F86C6BC492772BA5ACA9C87299293C45,
	AdViewBridge_IsValid_mDF146998DE98CD4B18D6930D8063976793770856,
	AdViewBridge_Show_m2E2D895F1FB4A89E8E5920452FFD3E8A94DB9F47,
	AdViewBridge_Release_mE35B55B75CB0043D0A530BD6ADD48C664531EDF9,
	AdViewBridge_OnLoad_mBEFFBA7157C7A769D0834103845DDD299346FF6F,
	AdViewBridge_OnImpression_mDA0B1D87136001D4D906456A0E2B2BC7850D9D6F,
	AdViewBridge_OnClick_m9369BC976EC9ACF94608840DB0C7CF6D07559CBB,
	AdViewBridge_OnError_m473E7FF95AD73CE72A093117B4D5EE91F4D0CA33,
	AdViewBridge_OnFinishedClick_m4BEDC9B638BBE0233F9811CE666D4660CC679582,
	AdViewBridgeAndroid_AdViewForAdViewId_m8ACD675C88EE8642600572F8E6A54262C746424F,
	AdViewBridgeAndroid_AdViewContainerForAdViewId_mA318B16474C653E7840AA9BE36F349E1877075A3,
	AdViewBridgeAndroid_JavaAdSizeFromAdSize_m5D1E1136009966DC3C7DB749322570854040D4A1,
	AdViewBridgeAndroid_Create_mF082BBD9880B87D613A142AB0249FD4F162E3623,
	AdViewBridgeAndroid_Load_m1A96128BD57C5772C2DF5F814BF7C60850EF501F,
	AdViewBridgeAndroid_IsValid_mBE070BD4898B6C0E4E5E640C11EEC6D7D1BB84DF,
	AdViewBridgeAndroid_Show_m0A1B2A36285BFD26C2C53D2AC1B5B0CD577A16E3,
	AdViewBridgeAndroid_Release_m40CB2C4825728AAD170561F551E9455F09D6D998,
	AdViewBridgeAndroid_OnLoad_m8EE502E51A5F7F549BE87624B2C53AC0DA6F9AB4,
	AdViewBridgeAndroid_OnImpression_mC06C1F6D3CB48795CB184D48BEAD0F30A5DCE693,
	AdViewBridgeAndroid_OnClick_m4335F566FD7AE7296A734253ADC6966CF441AD4D,
	AdViewBridgeAndroid_OnError_mFAC616016446C6CA08C83C973E000F358CE4996E,
	AdViewBridgeAndroid_OnFinishedClick_m5AD411FAF8BD4AA0D5674A8351FF3857BF32C65F,
	AdViewBridgeAndroid__ctor_m482A854667EA4A6C7679DDD616D17233CECE48D5,
	AdViewBridgeAndroid__cctor_m8082021F23CD0DBEC60DCD65B0118B3F597177EB,
	U3CU3Ec__DisplayClass11_0__ctor_mE2E09395E81909A176BFD52A868620E528B36698,
	U3CU3Ec__DisplayClass11_0_U3CShowU3Eb__0_mCC1227BB138DFBC93108B9F6B47320CE88F2A6DE,
	U3CU3Ec__DisplayClass13_0__ctor_m53AF55B5CCB0DE1EAB0B968BA395EDEE81F5A8D6,
	U3CU3Ec__DisplayClass13_0_U3CReleaseU3Eb__0_mAFAD6BBF32E46A39AD0D8272BE87CB5FDC3C36A1,
	AdViewContainer_get_adView_mF173B99D00FDF979B8E88AEB2C89EB66BC63EA0B,
	AdViewContainer_set_adView_m4C8C90B38B7BEFB20372EA3B62CED435E889274C,
	AdViewContainer_get_onLoad_mAD329E912D44818B5EE355E4D6349D6C84725336,
	AdViewContainer__ctor_m83E84C60F2CA567DABD05A755594BDC95F5D2AF3,
	AdViewContainer_ToString_mAD9C8EB6E1AB3F7C3C09FCCE8A5F04209BB77BCA,
	AdViewContainer_LoadAdConfig_m39A628320358AAABC53BEFA609545A1C41571CD5,
	AdViewContainer_Load_m21EC7E647E95F8A1345E0DE395C0A06508AC4738,
	AdViewContainer_Load_mF388040489C124FACB014FAD493FD575250D19EF,
	AdViewBridgeListenerProxy__ctor_mCC5976FF4B692779997B74F01F4388BAC1D74223,
	AdViewBridgeListenerProxy_onError_m441EB8D3ECFDCEF7271843C0BC14E32B501B58B0,
	AdViewBridgeListenerProxy_onAdLoaded_m21CD45E711594099143EF260399275F9FD81898E,
	AdViewBridgeListenerProxy_onAdClicked_m739335F8101138FC8974A777BBE2B31126173E82,
	AdViewBridgeListenerProxy_onLoggingImpression_mA6C018DC458CAF831EFC20473236B11D71A2E134,
	AdViewBridgeListenerProxy_U3ConAdClickedU3Eb__5_0_m1FE6C08A128953255E5D837BD36E48718BAB0C40,
	AdViewBridgeListenerProxy_U3ConLoggingImpressionU3Eb__6_0_mFEA2510D1C0596F697A36A49664843F234A4B40D,
	U3CU3Ec__DisplayClass3_0__ctor_m4CB6FC4ABA853AF2D3B10279925AF211EC5D4788,
	U3CU3Ec__DisplayClass3_0_U3ConErrorU3Eb__0_m510A4AD2002C4E8DB1DE203EF84FC86F111AE154,
	AudienceNetworkAds_Initialize_m9B76FC498D44CE550EDBB6075706D138C7B14B5F,
	AudienceNetworkAds_IsInitialized_m3ABD939BB17990224EA7DEB8AA847914D48AD23B,
	FBInterstitialAdBridgeCallback__ctor_m84E5805C989B36CE9DD6B598491602D9BFE031CA,
	FBInterstitialAdBridgeCallback_Invoke_m3F30ABD6169AB25047BB628CE40340B13014D65F,
	FBInterstitialAdBridgeErrorCallback__ctor_mB1F2197CB65BE16BBBE9E2D47CF0A606E4BB8A18,
	FBInterstitialAdBridgeErrorCallback_Invoke_m81FC8F00C6229EBBDDB5FD87D85F838113E2B2BB,
	InterstitialAd_get_PlacementId_mC1BD108C594367099DEE08B1B85529E84329B117,
	InterstitialAd_set_PlacementId_m491B0D3ABB52877889D50EFAAE0683879429540E,
	InterstitialAd_get_InterstitialAdDidLoad_mE9EA30A35E2D33E084214A94E99A7C18EB90851B,
	InterstitialAd_set_InterstitialAdDidLoad_m706CF90FCC23551C07FC0E26701E04D461D805B2,
	InterstitialAd_get_InterstitialAdWillLogImpression_mBE1AF8E676EF850F9E86A8ED2309E5B554E0A220,
	InterstitialAd_set_InterstitialAdWillLogImpression_mE819096A17363F18315886DFA335AC957CEDEF41,
	InterstitialAd_get_InterstitialAdDidFailWithError_m86A66F13A9B01022C830DAB214674B9FF1F17FB1,
	InterstitialAd_set_InterstitialAdDidFailWithError_m4B8F0E5A1FD4DAEE4C3DA8F8E544254ED24A681C,
	InterstitialAd_get_InterstitialAdDidClick_mC16B274285D14D412293CA12B88E1B343383108C,
	InterstitialAd_set_InterstitialAdDidClick_mC049945613B2B200B4892B3DA13AD219F7A0328E,
	InterstitialAd_get_InterstitialAdWillClose_mEF531764761C134455DD517EDC10954BB3CC8901,
	InterstitialAd_get_InterstitialAdDidClose_mEA11D8098A581E106A47BBE022220321CB0263E6,
	InterstitialAd_set_InterstitialAdDidClose_m3FA1CA4A93710E641DB8D4B3D26035063FCDAA1E,
	InterstitialAd_get_InterstitialAdActivityDestroyed_mBFD62998A660D42427F60CF4076E879C5808AAF5,
	InterstitialAd__ctor_m239D8B61321594216EC608E2153460629BB910BA,
	InterstitialAd_Finalize_m0A6235FC25EE5880D347D61CF8B54799CAE772E6,
	InterstitialAd_Dispose_m0B61AAA5708A59E9B498AF26E7BCCD4587972527,
	InterstitialAd_Dispose_mC77C628A99DB1417B1CB91655AED7770717E5DF8,
	InterstitialAd_ToString_m55AD308886B1FAF885A89F7AB978603FBA5B5C65,
	InterstitialAd_Register_m5939306629CA5D011A779590EB1DDC47CCD3F236,
	InterstitialAd_LoadAd_m7A1B717DD13EFB6544FD95F50189D20DD02D6741,
	InterstitialAd_IsValid_mD7DB971066F9990CF7EB1585AB873946A28D4BB3,
	InterstitialAd_LoadAdFromData_m84B2424DDD5CB988DDCEA3189DD929D9E44C2D65,
	InterstitialAd_Show_m08B5B3AB2F4780C75437CA412B69D77C573C97BB,
	InterstitialAd_ExecuteOnMainThread_m21D0D65976643939F72D64B312D41765BFCD8F32,
	InterstitialAd_U3CLoadAdFromDataU3Eb__44_0_m127FF710F7CA3739591A73D693B2DB3516D7F0CE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InterstitialAdBridge__ctor_mC4498DE9D762B281CB4E125020358406C4A5F015,
	InterstitialAdBridge__cctor_m1DEB8E74E51311FDED5F46D39A73B4E20A2A7226,
	InterstitialAdBridge_CreateInstance_m5C1686CB90CC41687C9D68C4022701612B45E45A,
	InterstitialAdBridge_Create_mB13EE8844678509CEC60991B4A0CC5916537B05D,
	InterstitialAdBridge_Load_mD58B08578D4CE4FB477F210CDE28CE2160D30816,
	InterstitialAdBridge_IsValid_m376B6FA7D6EE4FCCE7136AEE402A30A3035F4EF9,
	InterstitialAdBridge_Show_m8F5CB88811F6CD9816B890B36A49230A18FAB4A1,
	InterstitialAdBridge_Release_mBB6613D132F7693E9D1F631653990BEAF33526DA,
	InterstitialAdBridge_OnLoad_m14B88CE3C1597EC88B58DA0FCD4148ADC4E76974,
	InterstitialAdBridge_OnImpression_m73C004F34821C8851FA924D55DEEC2643839E4DE,
	InterstitialAdBridge_OnClick_m2E42D22BEBB8956D3CACE868C631AD30D4E162F5,
	InterstitialAdBridge_OnError_mF54A5BFC398CE066156A504412926F3784FC018B,
	InterstitialAdBridge_OnWillClose_mDD527746FC96D0751AC35EB0111558F557A9794A,
	InterstitialAdBridge_OnDidClose_mE44E38CCCE7BCEDF029AC0F04223EEFD3B02E06C,
	InterstitialAdBridge_OnActivityDestroyed_m9E46A949BCFB65E9CE0C158C003C2000F807C004,
	InterstitialAdBridgeAndroid_InterstitialAdForuniqueId_m4C0EDD7D66BF62FE6F8881856C8323766560065F,
	InterstitialAdBridgeAndroid_InterstitialAdContainerForuniqueId_mEB1098B0DF87D2B1D829F6CCA1248A2A4CA353AE,
	InterstitialAdBridgeAndroid_Create_m8A0A62DD5E860E210F84D9C1805D1369524ABF29,
	InterstitialAdBridgeAndroid_Load_mBEBA7BBEA3CC3AFC1579B6319F87212F7EFDC168,
	InterstitialAdBridgeAndroid_IsValid_m04130F2CC2E2236D36839C889B00A8E3B6B55436,
	InterstitialAdBridgeAndroid_Show_m082052D33508BAE200EBCBAB9F0C3D898EA13700,
	InterstitialAdBridgeAndroid_Release_m40073243FA382356D5FC0A690E93E1102B9CA8D6,
	InterstitialAdBridgeAndroid_OnLoad_m6A880B4E952604B2632E219A4E78F29ACDA9C464,
	InterstitialAdBridgeAndroid_OnImpression_m0B98F118C95DF9A2C9EE8A4B6EA8492AB743E8A7,
	InterstitialAdBridgeAndroid_OnClick_mE27AB3D9F8840EEA2ACA4D817575594BA7020AE1,
	InterstitialAdBridgeAndroid_OnError_m81E40982D673123A5534736FBD6F14C0570064DC,
	InterstitialAdBridgeAndroid_OnWillClose_mCAD0EFE2DBF5D9A996EA2E8E9ABD8B8F7D9B2E1E,
	InterstitialAdBridgeAndroid_OnDidClose_mF1A238E668B7F580C5D3D7EE7AF825E3112EB278,
	InterstitialAdBridgeAndroid_OnActivityDestroyed_mEB7032270E5ABB4C288C5BDE5B979D9384C1FEB8,
	InterstitialAdBridgeAndroid__ctor_m460A72309A83E7AD2E9E5C2346B5C3B9A4DCABB3,
	InterstitialAdBridgeAndroid__cctor_m079828A9BAF1FDC2B67EF8DD6FE4A7C2A03EFA3D,
	InterstitialAdContainer_get_interstitialAd_mB67B6A0B101A0824B95B32D247601845F4109678,
	InterstitialAdContainer_set_interstitialAd_mA2B697E08CAAECD4C448BC564225A11354D9A0B1,
	InterstitialAdContainer_get_onLoad_m9F93DDB54D9899ED455FF641B4E04DBCBA6D9070,
	InterstitialAdContainer__ctor_m9285578CE597222E697741975B7A78DADCE265FF,
	InterstitialAdContainer_ToString_m3265D23A780A4C5D2149C78A12771DB437BC7A3A,
	InterstitialAdContainer_LoadAdConfig_mB1749ED116522293AE6727F5938E9865754B7C74,
	InterstitialAdContainer_Load_m4E79DDE1F3DE129C1DFDAEE9A1A2127033A59780,
	InterstitialAdContainer_Load_m9655CA8FE8FFE6BD73186FDAC2006815E7431748,
	InterstitialAdBridgeListenerProxy__ctor_mEDC5994E2FB87877269C18A90F77A557B8E49A12,
	InterstitialAdBridgeListenerProxy_onError_m3CDEC9A21F6B6174C929A858D01AA096185BA32B,
	InterstitialAdBridgeListenerProxy_onAdLoaded_m841EE1259F87047344A939CCAA0FB7C68A372C81,
	InterstitialAdBridgeListenerProxy_onAdClicked_m5B392F3E55F4267EDA75726A9D4483E49AAF2B44,
	InterstitialAdBridgeListenerProxy_onInterstitialDisplayed_m739428DD4A06C7B90C9D69C8036D128A24F1D9E8,
	InterstitialAdBridgeListenerProxy_onInterstitialDismissed_m519B89D262E6C05A62369465C607A0138881C419,
	InterstitialAdBridgeListenerProxy_onLoggingImpression_m805133697A706BA21FDA0C87F4B1BFC15E976C8B,
	InterstitialAdBridgeListenerProxy_onInterstitialActivityDestroyed_mC047D9671AA86089E66372F8757EAD02B7DE602C,
	InterstitialAdBridgeListenerProxy_U3ConAdClickedU3Eb__5_0_m581ABB927E5DEB5F0A4D7C4E0A1AAC9D525311A0,
	InterstitialAdBridgeListenerProxy_U3ConInterstitialDismissedU3Eb__7_0_m92362D7DFDDFA1FDAF75A331EEF152E6BCD47D51,
	InterstitialAdBridgeListenerProxy_U3ConLoggingImpressionU3Eb__8_0_mAE0853000EAF6AFC85807386F86E689EE11D03FA,
	InterstitialAdBridgeListenerProxy_U3ConInterstitialActivityDestroyedU3Eb__9_0_m0BAF6003F12E42B6CC2089FE97DF1D551BB77009,
	U3CU3Ec__DisplayClass3_0__ctor_m17A373D4CDD9E87FE5DDB3A91CDAFC85D8D01F3A,
	U3CU3Ec__DisplayClass3_0_U3ConErrorU3Eb__0_mEC73CBD766584959C3C64646C811F7A0DFA36DF3,
	FBRewardedVideoAdBridgeCallback__ctor_mE29E98F4491DEE39534279F45BFD015E402E2CBB,
	FBRewardedVideoAdBridgeCallback_Invoke_m6C0DF3940C44A4CB144ACFEBEB2E7E1CC8AB3ACA,
	FBRewardedVideoAdBridgeErrorCallback__ctor_m2EB7F01D53D11BEA1A25DE429C9CDE833A73DB90,
	FBRewardedVideoAdBridgeErrorCallback_Invoke_m0E61ADCF56E4F80B6587C6F89A1D4A84A7865633,
	RewardData_get_UserId_m65A779477569BCD25E48431426139A5D57D589D7,
	RewardData_set_UserId_m227738AF39EDD2099BC7ADFE9465F7F2547AF807,
	RewardData_get_Currency_m33F70966C3056F904ACF66B7C5C4AE0222415C28,
	RewardData_set_Currency_m724A4AC8CB31CDEFB23662AC67293212EC5D87DE,
	RewardData__ctor_mC1FD98C75AAFF6E5A223B254D728B5FEA532B79A,
	RewardedVideoAd_get_PlacementId_m79203F67638150A9462933CDF9BA5BF5A96BA4D4,
	RewardedVideoAd_set_PlacementId_m53135355B631289F71D0A0F219FD33C05D8C9651,
	RewardedVideoAd_get_RewardData_mF5E3C31190ECDF4033CBD22CFDA4371B733CCF31,
	RewardedVideoAd_set_RewardData_mEF09A8875685C2406BF78450FB1422AB837C5556,
	RewardedVideoAd_get_RewardedVideoAdDidLoad_m14DFCD4C4DCBFEDB7874AF9ECED60A0E6C5936F7,
	RewardedVideoAd_set_RewardedVideoAdDidLoad_mD3F05F7B0089C31E0C2CFD5E77A6F4943F9973B3,
	RewardedVideoAd_get_RewardedVideoAdWillLogImpression_mA7468F3067A4D2B844C4420F177F0B2B8B1A4E54,
	RewardedVideoAd_set_RewardedVideoAdWillLogImpression_mE2CE0A45C9F32625538A7DE145589CAD42A38D10,
	RewardedVideoAd_get_RewardedVideoAdDidFailWithError_mCB24562902B60225BA782845C1C9020C601F1377,
	RewardedVideoAd_set_RewardedVideoAdDidFailWithError_mD31F0F4B5DD7213C4ACC9839426B5CCCD420F0FD,
	RewardedVideoAd_get_RewardedVideoAdDidClick_mDDBB5168254A673D4DF48A827A18EAA9EB1A52B2,
	RewardedVideoAd_set_RewardedVideoAdDidClick_m711E4D99437375B38D0FF35DE52C70E966D8F590,
	RewardedVideoAd_get_RewardedVideoAdWillClose_m633DD0E6617B76A06B35201E9E1C1F142C025B55,
	RewardedVideoAd_get_RewardedVideoAdDidClose_m722737A392A87EAD0DA8259FE5AD59E934A5C5FB,
	RewardedVideoAd_set_RewardedVideoAdDidClose_m56D05187E56AA6707A1B65BE0AF64324D270B768,
	RewardedVideoAd_get_RewardedVideoAdComplete_m462ABD29ECE953884337436F9BB93D1740F16E5B,
	RewardedVideoAd_get_RewardedVideoAdDidSucceed_mEB28E3FBDA25E71D6DFF414253E9D22946E53CBA,
	RewardedVideoAd_set_RewardedVideoAdDidSucceed_mA080BC9DE3C9C8862BA705ACE24E9B54849B069C,
	RewardedVideoAd_get_RewardedVideoAdDidFail_m5B1123EE9B0853395F2AD15F60F4F05D6E0CDEF3,
	RewardedVideoAd_set_RewardedVideoAdDidFail_mD1B1F7678B360A3F3C2F05C848B2B892C4D0A432,
	RewardedVideoAd_get_RewardedVideoAdActivityDestroyed_m246DC2E953D15F6B222829B7D06790A20080B26E,
	RewardedVideoAd_set_RewardedVideoAdActivityDestroyed_m1C01E72CAE28FFA9AC1FBFB83A0CFE244B498DEE,
	RewardedVideoAd__ctor_m206D7E56B15CA641B5F114D76A901BE66FB9BDDE,
	RewardedVideoAd__ctor_mA9B4DA4CC7D8848C14A5DCB592DB8FF36EE4819A,
	RewardedVideoAd_Finalize_m22C43594B72471B683B85FB42576142F5F96FFBA,
	RewardedVideoAd_Dispose_m0AB77D357A5D1D349C2A61C950582CF5B4DC5E39,
	RewardedVideoAd_Dispose_m8C89561619EB587D27F15D234292381492DC3429,
	RewardedVideoAd_ToString_mD8FBAC53DB38CD43A5485AE5F10267518FAB80C2,
	RewardedVideoAd_Register_m35674944BBC1078A4F3ED0EDD97D9A0122AC6F12,
	RewardedVideoAd_LoadAd_m897ED21843B26D25877E65B5CD87039A09BE27AC,
	RewardedVideoAd_IsValid_m8AC704631039C4E0EAA441844AF581BAAD7079C1,
	RewardedVideoAd_LoadAdFromData_m144FEDBCA9CA8193453599AFE4F6B99F7E004E06,
	RewardedVideoAd_Show_mC78512D2669EF8B78885DB70C9DE055ED6812585,
	RewardedVideoAd_ExecuteOnMainThread_m99D031E76CF4A8F01149AB49ABD035716F24C5CC,
	RewardedVideoAd_U3CLoadAdFromDataU3Eb__61_0_m113C89C79D2E074C458EF1789C2A6D0509C1A566,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RewardedVideoAdBridge__ctor_m7604E29BADB7C0685B83F95ABDC2058B104FBC33,
	RewardedVideoAdBridge__cctor_m778C43C866232250CE1E5C9C90D9750DBC6493C7,
	RewardedVideoAdBridge_CreateInstance_m4925538DF21DCE6091452FBE0BBDB2B49F8BC1BD,
	RewardedVideoAdBridge_Create_m99D85C52AFF1D290CA11CA0FF10C138320CF582A,
	RewardedVideoAdBridge_Load_mDB69D7A91891B09FC908B651A2C36B9DD90FB583,
	RewardedVideoAdBridge_IsValid_m0D1E4251E4221DF09D93754E82D03A82DE2DE1B2,
	RewardedVideoAdBridge_Show_m02B33DA6E0E075748540728653D122CCD0CC9180,
	RewardedVideoAdBridge_Release_mE58B8ACC66C0A2A01C3EC0E002137774A45F3F62,
	RewardedVideoAdBridge_OnLoad_m3809EA5A58DE011EFFC22BE822D8169FD15D8C84,
	RewardedVideoAdBridge_OnImpression_m48FF53D78E76BC2374CFD85A172126DF37A81846,
	RewardedVideoAdBridge_OnClick_m96006AB463F9391090C3FB3C5583C4F5519FC487,
	RewardedVideoAdBridge_OnError_m77FB4FC98AB36EBE5417B09B46850103F51159E7,
	RewardedVideoAdBridge_OnWillClose_mEE1D54D1A99EC68C0A2950EFFF90B1B7188F8B98,
	RewardedVideoAdBridge_OnDidClose_m3669C081E894BFDCD94E1F70EBA95297B87F0750,
	RewardedVideoAdBridge_OnComplete_m5240E0BE8C516470C84CA5152511AAB17FABBFFD,
	RewardedVideoAdBridge_OnDidSucceed_m9831C4C163332DE8C0C9780DC03DF16C6C21025F,
	RewardedVideoAdBridge_OnDidFail_m7E7C1B81D4BA4D9DB8D35AD67D369ED3B78E2EDB,
	RewardedVideoAdBridge_OnActivityDestroyed_m781010FE9596C28F1FE2FF9716B11D3151314737,
	RewardedVideoAdBridgeAndroid_RewardedVideoAdForUniqueId_mCF7526C86DE10ED2B75E68C7125C76812F3B5B45,
	RewardedVideoAdBridgeAndroid_RewardedVideoAdContainerForUniqueId_mEBAE25181BBE2474759D9F6820DDC025AF7CBB4E,
	RewardedVideoAdBridgeAndroid_Create_mECA447FDE335FACE5955E834EF89F95A7DA5DC99,
	RewardedVideoAdBridgeAndroid_Load_m6DF53C6A3629A9B7BAB5AC5B736454A74E487651,
	RewardedVideoAdBridgeAndroid_IsValid_mABEA2E745E004A988F9D1E54A44AEEC88FE33E3C,
	RewardedVideoAdBridgeAndroid_Show_m66E7441A75B5D14CD20DAB07ACC601BF40DD88BA,
	RewardedVideoAdBridgeAndroid_Release_m312D1DE90460C974B58FA8569DA48AD4C1460E1E,
	RewardedVideoAdBridgeAndroid_OnLoad_m8943CBDD14EDCFE684860B2AEC92DCD40460473F,
	RewardedVideoAdBridgeAndroid_OnImpression_m7F74A87458FFF2101F5C063B675B39D506A75EC6,
	RewardedVideoAdBridgeAndroid_OnClick_m38EA89E5A2A434E28C18CC4A7096B9694486430F,
	RewardedVideoAdBridgeAndroid_OnError_m5BC36305EBD0F8FA26668EEBB76C6A409575B95A,
	RewardedVideoAdBridgeAndroid_OnWillClose_m2DBD46E62DD2DB027AA4AD6A751362A0BB2E83CA,
	RewardedVideoAdBridgeAndroid_OnDidClose_mDBFD095DB9C7A43C1EB6FBFC748EFBB26D02A87D,
	RewardedVideoAdBridgeAndroid_OnActivityDestroyed_m6689D87D40FABEF2194AC708EB18CA2325F56397,
	RewardedVideoAdBridgeAndroid__ctor_m182C87E5DC2793CD48C108AE8F902B4C94D95BEA,
	RewardedVideoAdBridgeAndroid__cctor_m5A3C36B1886DAB1A6EA67807231376A105F041C5,
	U3CU3Ec__DisplayClass10_0__ctor_m75C3B64F838AA94122176BE075E4D06F23466F6F,
	U3CU3Ec__DisplayClass10_0_U3CShowU3Eb__0_m844C438B2600B7FCBB3E982272DE01502B9E6124,
	RewardedVideoAdContainer_get_rewardedVideoAd_m68C0CB84DAA674C467D1DF65788AB3361889F39F,
	RewardedVideoAdContainer_set_rewardedVideoAd_m1B0F28CA41F608909F511BEFB6D4CF224B0903C5,
	RewardedVideoAdContainer_get_onLoad_mB2E4719AF636CE6DFB1E45633474FFA5F4D3D1F6,
	RewardedVideoAdContainer__ctor_mC1D05DF8718F12B908BFD9D16F2047C9C7BD4AA5,
	RewardedVideoAdContainer_ToString_m31FA708066DC5372098F0B2357CC965AE5A3965A,
	RewardedVideoAdContainer_LoadAdConfig_m66E6D2B778B011620203ACE21213548B615AA264,
	RewardedVideoAdContainer_Load_mCC9D273C25650E07308E2FCAF765474E4B6FB8FF,
	RewardedVideoAdContainer_Load_mA533DDF6D3CB0770051586A37F400B36603F3686,
	RewardedVideoAdBridgeListenerProxy__ctor_mBC1A5F16FDED756049E689FBEA193426E117792D,
	RewardedVideoAdBridgeListenerProxy_onError_mE30FA6850454A0A4B2173941A7377DEAA8CE94A4,
	RewardedVideoAdBridgeListenerProxy_onAdLoaded_m6EDEFE1300C03E9258C3B1B907CEAA69804240EE,
	RewardedVideoAdBridgeListenerProxy_onAdClicked_mAFF12D9D6FD62F830E5EC986CF7B2DAF0A524A93,
	RewardedVideoAdBridgeListenerProxy_onRewardedVideoDisplayed_m3FA5B655E42232E0F9AAFEB79DDCB679CE5ABB5B,
	RewardedVideoAdBridgeListenerProxy_onRewardedVideoClosed_m7AF118B3A9B70ED0DBE781DB024DBC0772FF28D6,
	RewardedVideoAdBridgeListenerProxy_onRewardedVideoCompleted_mB6A855DC4859C85FE6480624B10E642D7A6B9D0C,
	RewardedVideoAdBridgeListenerProxy_onRewardServerSuccess_m16A0ECAB52A49FDA48D00EF1257B81B69A79D14F,
	RewardedVideoAdBridgeListenerProxy_onRewardServerFailed_mC6F89C8B01EA197A6952E8CCE624BA0AFCACB83F,
	RewardedVideoAdBridgeListenerProxy_onLoggingImpression_m96585A5DC18A6126BD3159FF31A11D839E6C8DCB,
	RewardedVideoAdBridgeListenerProxy_onRewardedVideoActivityDestroyed_m2F93E87F08AFEABEEBBA8A18A0A661C42C21470B,
	RewardedVideoAdBridgeListenerProxy_U3ConAdClickedU3Eb__5_0_mB842815494CA7A853DD880303F59F3DF60B894CD,
	RewardedVideoAdBridgeListenerProxy_U3ConRewardedVideoDisplayedU3Eb__6_0_mD12182F2C5859E5F60D8570075BFA97CC89CE39A,
	RewardedVideoAdBridgeListenerProxy_U3ConRewardedVideoClosedU3Eb__7_0_m5933FF023CB75079BA91B778DF07CFBAB4A1BF8C,
	RewardedVideoAdBridgeListenerProxy_U3ConRewardedVideoCompletedU3Eb__8_0_mB339FE1BDD19A08DA4AA0310A37855FD5F2DA26E,
	RewardedVideoAdBridgeListenerProxy_U3ConRewardServerSuccessU3Eb__9_0_m70128105A8391D3025BDEAE469442A246F5B48AC,
	RewardedVideoAdBridgeListenerProxy_U3ConRewardServerFailedU3Eb__10_0_mF277208F423DD348B08D90EC7BD0003CC2E22008,
	RewardedVideoAdBridgeListenerProxy_U3ConLoggingImpressionU3Eb__11_0_m052C94762C225F204FD5037217F73B07DBFAAEDC,
	RewardedVideoAdBridgeListenerProxy_U3ConRewardedVideoActivityDestroyedU3Eb__12_0_m17EA0E3457A2CED3D832554E28C3ABCDAEDD1884,
	U3CU3Ec__DisplayClass3_0__ctor_mAB5059429A7096CD23555B020E480D6FF0D0D4B1,
	U3CU3Ec__DisplayClass3_0_U3ConErrorU3Eb__0_m8F38B16208E0801A4DB6D2B96BB0813D291C02ED,
	SdkVersion_get_Build_mBEAADE9EE4890BF3603882F1E0791477CB0E9D1B,
	AdUtility_Width_m53FB022B8EA642F02E4EC7A4E45C738EE221FEC7,
	AdUtility_Height_m5E7CFB82BF859E590117210844CC41598A7B017E,
	AdUtility_Prepare_mE7B4059FE922EA4228F4D852096E841F4908E489,
	NULL,
	NULL,
	NULL,
	AdUtilityBridge__ctor_m0A963823A15E6591C458EB110DFAE4AFC536EDFA,
	AdUtilityBridge__cctor_m71D6AFC011E1FEE3FF56B2D7CF6764E78A7FCC80,
	AdUtilityBridge_CreateInstance_m291203ADD4AF8294B08979D27560137B8C7C98D4,
	AdUtilityBridge_Width_m46FD7B57BE4376B1799EC4F7877C6FC765E6FA64,
	AdUtilityBridge_Height_m35EA150473621FE40192348E0F0D4D691AFF363B,
	AdUtilityBridge_Convert_m9D472E757903B9804CC0B7712E63A6CE50B30D8A,
	AdUtilityBridge_Prepare_m9DEE605AFDED59E31902574D8AB3B43A8A3EA479,
	NULL,
	AdUtilityBridgeAndroid_Density_m0ED8E4A7E92E75A9A716FFC238BD6798331FEEEE,
	AdUtilityBridgeAndroid_Width_m184FCB1F858DDAA07B53347F4E1D312DC31A33FD,
	AdUtilityBridgeAndroid_Height_mB65AD7B91DCCDA0ABDBA05DB35CBEC2A24EFB1DE,
	AdUtilityBridgeAndroid_Convert_mDC09CA70FB26CA578470C21DAA185C970D1E6DE8,
	AdUtilityBridgeAndroid_Prepare_m329280744DEEC835F388F4E9830F7763C260EEB7,
	AdUtilityBridgeAndroid__ctor_mCAB48FA91CF8DD82CA7A8FC4ABAC10C41F096AD9,
};
static const int32_t s_InvokerIndices[1065] = 
{
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	3889,
	4803,
	4803,
	3912,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	3912,
	4803,
	4803,
	7334,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	3912,
	4803,
	4803,
	7334,
	4803,
	4803,
	4803,
	4803,
	4803,
	7334,
	4803,
	3912,
	4803,
	4803,
	4803,
	4803,
	4803,
	7334,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	3837,
	4803,
	4803,
	4803,
	3912,
	1837,
	4803,
	1256,
	7307,
	3837,
	3837,
	3253,
	2778,
	4665,
	4665,
	4616,
	4616,
	3837,
	4616,
	4803,
	3465,
	4803,
	4803,
	3912,
	3912,
	2041,
	4691,
	3912,
	3912,
	2041,
	2019,
	1198,
	828,
	1158,
	4803,
	1753,
	1753,
	4803,
	3465,
	3912,
	4803,
	4616,
	4616,
	4616,
	4616,
	3912,
	4691,
	4691,
	4691,
	4691,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	7334,
	4803,
	2778,
	2778,
	2778,
	2778,
	2778,
	2778,
	2778,
	2778,
	2778,
	2778,
	3253,
	3253,
	3253,
	3253,
	3253,
	3253,
	4803,
	2778,
	4803,
	2778,
	4803,
	2778,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	1754,
	4803,
	3912,
	3889,
	4803,
	4616,
	4803,
	4691,
	4803,
	4691,
	4803,
	2219,
	2219,
	4803,
	7307,
	4691,
	4803,
	4803,
	4803,
	4803,
	4691,
	4803,
	4803,
	7334,
	3912,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	7307,
	4803,
	4803,
	4803,
	4803,
	2219,
	4803,
	4803,
	4803,
	4803,
	3912,
	3912,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	3912,
	4803,
	4803,
	7307,
	4803,
	4803,
	4803,
	7334,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	7307,
	4803,
	4803,
	3889,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	3912,
	4803,
	4803,
	4803,
	4803,
	4691,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	1115,
	4803,
	1115,
	4803,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	4803,
	4803,
	3912,
	3912,
	2261,
	2261,
	1235,
	1235,
	1254,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4691,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	4691,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4691,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	4803,
	4803,
	4803,
	4803,
	4803,
	3465,
	4691,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	4803,
	4691,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	4803,
	4803,
	4803,
	4803,
	3912,
	3465,
	3465,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	4803,
	4803,
	4691,
	4691,
	4803,
	7334,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	4803,
	4803,
	4803,
	3889,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	3889,
	4803,
	4803,
	4803,
	2261,
	2261,
	1235,
	1235,
	1254,
	4803,
	4803,
	4803,
	4803,
	3912,
	3912,
	4803,
	4803,
	4803,
	4803,
	3912,
	4803,
	3912,
	3912,
	3912,
	3912,
	3889,
	4803,
	4803,
	4803,
	4803,
	3889,
	4803,
	4803,
	4803,
	4691,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	4803,
	4803,
	4803,
	4803,
	3912,
	4691,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	4803,
	4803,
	4803,
	4803,
	3912,
	4691,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	4803,
	4803,
	4803,
	4803,
	3912,
	4691,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	4803,
	4803,
	4803,
	4803,
	3912,
	4691,
	4803,
	4803,
	1576,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	4803,
	4803,
	3465,
	4691,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	3461,
	3461,
	4803,
	4803,
	2778,
	4803,
	2778,
	4665,
	4665,
	4803,
	1750,
	4691,
	4803,
	3889,
	4803,
	4616,
	4691,
	4803,
	4691,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4665,
	7212,
	4803,
	4803,
	4803,
	1199,
	4616,
	4616,
	4616,
	4803,
	4616,
	1199,
	3912,
	3912,
	3912,
	3912,
	2019,
	4803,
	1199,
	4616,
	4616,
	3912,
	3912,
	3912,
	4803,
	4616,
	4803,
	4616,
	1199,
	3912,
	2019,
	4803,
	1199,
	2019,
	4616,
	1199,
	4803,
	4803,
	4616,
	4616,
	3912,
	3912,
	4616,
	3912,
	3912,
	4803,
	4803,
	1199,
	4616,
	4803,
	4616,
	4616,
	4616,
	1199,
	3912,
	3912,
	3912,
	3912,
	2019,
	4803,
	4616,
	4803,
	1199,
	4616,
	4616,
	4616,
	4803,
	1199,
	3912,
	3912,
	3912,
	3912,
	2019,
	4803,
	4803,
	1199,
	4616,
	4803,
	4616,
	4616,
	4616,
	1199,
	3912,
	3912,
	3912,
	3912,
	2019,
	4803,
	4616,
	4803,
	1199,
	4616,
	4616,
	4616,
	4803,
	1199,
	3912,
	3912,
	3912,
	3912,
	2019,
	4803,
	4616,
	4803,
	1199,
	4616,
	4616,
	4616,
	4803,
	1199,
	3912,
	3912,
	3912,
	3912,
	2019,
	4803,
	1199,
	2019,
	4803,
	2041,
	4616,
	4803,
	3912,
	3912,
	4616,
	4803,
	3912,
	3912,
	3912,
	1238,
	1238,
	3912,
	3912,
	2214,
	4616,
	4803,
	4616,
	1199,
	4803,
	3912,
	4803,
	4803,
	4803,
	4803,
	7334,
	4803,
	2778,
	4803,
	1199,
	4803,
	4616,
	4616,
	4616,
	4616,
	1199,
	3912,
	3912,
	3912,
	3912,
	2019,
	4803,
	4803,
	3912,
	4803,
	4803,
	4803,
	7334,
	7212,
	0,
	4803,
	7334,
	7307,
	3912,
	3912,
	4691,
	4803,
	2216,
	4803,
	2216,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	2214,
	4803,
	4803,
	3837,
	4691,
	3912,
	4803,
	4616,
	4803,
	6329,
	2753,
	2732,
	1361,
	513,
	3912,
	6788,
	4803,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4803,
	7334,
	7307,
	1052,
	3231,
	2753,
	287,
	3889,
	2041,
	2041,
	2041,
	2041,
	2041,
	3461,
	3461,
	3461,
	1052,
	3231,
	2753,
	287,
	3889,
	2041,
	2041,
	2041,
	2041,
	2041,
	4803,
	7334,
	4803,
	4803,
	4803,
	4803,
	4691,
	3912,
	4691,
	3912,
	4691,
	3465,
	4803,
	3912,
	2219,
	2219,
	3912,
	3912,
	3912,
	4803,
	4803,
	4803,
	4803,
	7334,
	7286,
	2216,
	4803,
	2216,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	4691,
	3912,
	4691,
	3912,
	4803,
	4803,
	3837,
	4691,
	3912,
	4803,
	4616,
	4803,
	4616,
	3912,
	4803,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4803,
	7334,
	7307,
	1598,
	3231,
	2753,
	2753,
	3889,
	2041,
	2041,
	2041,
	2041,
	2041,
	2041,
	2041,
	3461,
	3461,
	1598,
	3231,
	2753,
	2753,
	3889,
	2041,
	2041,
	2041,
	2041,
	2041,
	2041,
	2041,
	4803,
	7334,
	4691,
	3912,
	4691,
	3912,
	4691,
	3465,
	4803,
	3912,
	2219,
	2219,
	3912,
	3912,
	3912,
	3912,
	3912,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	2216,
	4803,
	2216,
	3912,
	4691,
	3912,
	4691,
	3912,
	4803,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	4691,
	3912,
	4691,
	4691,
	3912,
	4691,
	3912,
	4691,
	3912,
	3912,
	2219,
	4803,
	4803,
	3837,
	4691,
	3912,
	4803,
	4616,
	4803,
	4616,
	3912,
	4803,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4803,
	7334,
	7307,
	1053,
	3231,
	2753,
	2753,
	3889,
	2041,
	2041,
	2041,
	2041,
	2041,
	2041,
	2041,
	2041,
	2041,
	2041,
	3461,
	3461,
	1053,
	3231,
	2753,
	2753,
	3889,
	2041,
	2041,
	2041,
	2041,
	2041,
	2041,
	2041,
	4803,
	7334,
	4803,
	4803,
	4691,
	3912,
	4691,
	3912,
	4691,
	3465,
	4803,
	3912,
	2219,
	2219,
	3912,
	3912,
	3912,
	4803,
	4803,
	4803,
	4803,
	3912,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	7307,
	7294,
	7294,
	7334,
	0,
	0,
	0,
	4803,
	7334,
	7307,
	4636,
	4636,
	3017,
	4803,
	0,
	4636,
	4636,
	4636,
	3017,
	4803,
	4803,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000423, { 0, 1 } },
};
extern const uint32_t g_rgctx_AndroidJavaObject_Get_TisT_tBD95D49DAB3C6C993F1BA0B99066A3F5E43B826E_mE58A37D4E60DFA3CC51332465CA294120D3DEF0B;
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AndroidJavaObject_Get_TisT_tBD95D49DAB3C6C993F1BA0B99066A3F5E43B826E_mE58A37D4E60DFA3CC51332465CA294120D3DEF0B },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1065,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
