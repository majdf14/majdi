﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.Advertisements.Advertisement::.cctor()
extern void Advertisement__cctor_mCC6272BAAF27A9D700E13D1DE3EF7CCA19A6BA48 (void);
// 0x00000002 System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void Advertisement_Initialize_mA83C553537C87F2C2B12FCBAE4B9EB75EC9DA343 (void);
// 0x00000003 System.Void UnityEngine.Advertisements.Advertisement::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void Advertisement_Load_m57620941EB371D6E9EFC283698511946CA9BA0A5 (void);
// 0x00000004 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Advertisement_Show_m8F5AB716E9FB227B3FCD267DBCA7BCCE63C06C6B (void);
// 0x00000005 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Advertisement_Show_mD97A736E31BC882EAA93655679EFFFA9482F591F (void);
// 0x00000006 System.Void UnityEngine.Advertisements.Advertisement::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Advertisement_SetMetaData_m9BF33875888AB1DAB282065CE1563F0C1F78EF71 (void);
// 0x00000007 UnityEngine.Advertisements.Platform.IPlatform UnityEngine.Advertisements.Advertisement::CreatePlatform()
extern void Advertisement_CreatePlatform_mFFCC7E5D24C35AF45F2C3FE61A20D60102FF5293 (void);
// 0x00000008 System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_mC55582B1B0C8277EDCCBCCF14627435D8354DB0E (void);
// 0x00000009 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_m89B35F72104AF5747936DD1FC58E5C13704075D3 (void);
// 0x0000000A System.Void UnityEngine.Advertisements.Advertisement/Banner::Hide(System.Boolean)
extern void Banner_Hide_mE40A4CE81B2F66D874A0995CEB0BF595D9BE09DC (void);
// 0x0000000B System.Void UnityEngine.Advertisements.Advertisement/Banner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void Banner_SetPosition_m69680FE1B23F22DC2212F1C37B0890C6833645E8 (void);
// 0x0000000C UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Banner::get_UnityLifecycleManager()
extern void Banner_get_UnityLifecycleManager_m5BB7A4F09AB21EDDF9E77A499D91C03A68B7E82E (void);
// 0x0000000D System.Boolean UnityEngine.Advertisements.Banner::get_ShowAfterLoad()
extern void Banner_get_ShowAfterLoad_m0F5758A87EC97CB00326193263FA91BAF27DB9A4 (void);
// 0x0000000E System.Void UnityEngine.Advertisements.Banner::set_ShowAfterLoad(System.Boolean)
extern void Banner_set_ShowAfterLoad_m4748C54A24F9F34CDCFA582DC64D5D539DCD83CF (void);
// 0x0000000F System.Void UnityEngine.Advertisements.Banner::.ctor(UnityEngine.Advertisements.INativeBanner,UnityEngine.Advertisements.Utilities.IUnityLifecycleManager)
extern void Banner__ctor_m55A8737759701A4D4E16CC7667CEA599A1C39D39 (void);
// 0x00000010 System.Void UnityEngine.Advertisements.Banner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_mF9AF0805B1312D1430FF8C9B034C81068F453289 (void);
// 0x00000011 System.Void UnityEngine.Advertisements.Banner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_m661CD517BDB0ED56DA8E07145C09877DE6C4E2BD (void);
// 0x00000012 System.Void UnityEngine.Advertisements.Banner::Hide(System.Boolean)
extern void Banner_Hide_mA4CAE84741A6D00B02A270661712FF2AF11C79E3 (void);
// 0x00000013 System.Void UnityEngine.Advertisements.Banner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void Banner_SetPosition_m4FDEDF04AF2BE1D18B2943B3F790DE780E65510A (void);
// 0x00000014 UnityEngine.Advertisements.BannerLoadOptions/LoadCallback UnityEngine.Advertisements.BannerLoadOptions::get_loadCallback()
extern void BannerLoadOptions_get_loadCallback_mECEEB150276C0E8C1744CB508B6AFF72A0796077 (void);
// 0x00000015 System.Void UnityEngine.Advertisements.BannerLoadOptions::set_loadCallback(UnityEngine.Advertisements.BannerLoadOptions/LoadCallback)
extern void BannerLoadOptions_set_loadCallback_m219703CC2E16DAFA77481374046BD19298B27BF0 (void);
// 0x00000016 UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback UnityEngine.Advertisements.BannerLoadOptions::get_errorCallback()
extern void BannerLoadOptions_get_errorCallback_mB9B595EB059D86CA406DCC7F7D4B9EF59A8AB589 (void);
// 0x00000017 System.Void UnityEngine.Advertisements.BannerLoadOptions::set_errorCallback(UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback)
extern void BannerLoadOptions_set_errorCallback_m73A7C9AEA0F79A4963538CE38D3AAE5AB69AF6E2 (void);
// 0x00000018 System.Void UnityEngine.Advertisements.BannerLoadOptions::.ctor()
extern void BannerLoadOptions__ctor_m866C2FA6DEA20A35F7FBE25536D96BD563D1CFE7 (void);
// 0x00000019 System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::.ctor(System.Object,System.IntPtr)
extern void LoadCallback__ctor_m82C20B862B58A068187B9E090BA53E0F12C2E3F1 (void);
// 0x0000001A System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::Invoke()
extern void LoadCallback_Invoke_mFEB8CEEF9C2B011B55E8B61D80B7FC98AA000420 (void);
// 0x0000001B System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::.ctor(System.Object,System.IntPtr)
extern void ErrorCallback__ctor_m6C3D9F85150EFC4700A624E3946C061BA25849A9 (void);
// 0x0000001C System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::Invoke(System.String)
extern void ErrorCallback_Invoke_m8D78A1ACBA0018E30AC198C4C179E74F1F5E0377 (void);
// 0x0000001D UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_showCallback()
extern void BannerOptions_get_showCallback_m0367F7A8058FA64DD2C114DC2423481F33FF531B (void);
// 0x0000001E System.Void UnityEngine.Advertisements.BannerOptions::set_showCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_showCallback_m14C693B20F26D364ECB7B9B90F3C455D0B361357 (void);
// 0x0000001F UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_hideCallback()
extern void BannerOptions_get_hideCallback_mBED58861170D9007A1F6C4883ED906C151AD852A (void);
// 0x00000020 System.Void UnityEngine.Advertisements.BannerOptions::set_hideCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_hideCallback_m9877994E49979C868E5331BA9708D52DE2F18424 (void);
// 0x00000021 UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_clickCallback()
extern void BannerOptions_get_clickCallback_m7E35A638D6D69CE003CEB93C25DB33BA194F3AF0 (void);
// 0x00000022 System.Void UnityEngine.Advertisements.BannerOptions::.ctor()
extern void BannerOptions__ctor_m5D0194C2660940617F25552DB827D41D10878A60 (void);
// 0x00000023 System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::.ctor(System.Object,System.IntPtr)
extern void BannerCallback__ctor_m54F3DA4DFF8463122BC222D9B3F49A6A2EF26A46 (void);
// 0x00000024 System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::Invoke()
extern void BannerCallback_Invoke_m1826BD07878E285004632455BF5C2226B26C4236 (void);
// 0x00000025 System.Void UnityEngine.Advertisements.UnityAdsInitializationListenerMainDispatch::.ctor(UnityEngine.Advertisements.IUnityAdsInitializationListener,UnityEngine.Advertisements.Utilities.IUnityLifecycleManager)
extern void UnityAdsInitializationListenerMainDispatch__ctor_mF2ABB02A80CBE4A3172B19B9EC0B5DE8884E1B37 (void);
// 0x00000026 System.Void UnityEngine.Advertisements.UnityAdsInitializationListenerMainDispatch::OnInitializationComplete()
extern void UnityAdsInitializationListenerMainDispatch_OnInitializationComplete_m3F574F08BDCC7C37F77E7A23DBD4A87ABFEBD516 (void);
// 0x00000027 System.Void UnityEngine.Advertisements.UnityAdsInitializationListenerMainDispatch::OnInitializationFailed(UnityEngine.Advertisements.UnityAdsInitializationError,System.String)
extern void UnityAdsInitializationListenerMainDispatch_OnInitializationFailed_m85CA948CB74E5EA43C6DD91BD292F2926D8455F6 (void);
// 0x00000028 System.Void UnityEngine.Advertisements.UnityAdsInitializationListenerMainDispatch::<OnInitializationComplete>b__3_0()
extern void UnityAdsInitializationListenerMainDispatch_U3COnInitializationCompleteU3Eb__3_0_mFF9220DE776EC5910A9D7AFB00A7CFD322DB6E51 (void);
// 0x00000029 System.Void UnityEngine.Advertisements.UnityAdsInitializationListenerMainDispatch/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m5E055A48B47F27C369B62EDD5AA6C4CB38B5C5D4 (void);
// 0x0000002A System.Void UnityEngine.Advertisements.UnityAdsInitializationListenerMainDispatch/<>c__DisplayClass4_0::<OnInitializationFailed>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3COnInitializationFailedU3Eb__0_m98AB7A0529A5CCE24EEAE6BF06BF2BF2FC7A7659 (void);
// 0x0000002B System.Void UnityEngine.Advertisements.UnityAdsLoadListenerMainDispatch::.ctor(UnityEngine.Advertisements.IUnityAdsLoadListener,UnityEngine.Advertisements.Utilities.IUnityLifecycleManager)
extern void UnityAdsLoadListenerMainDispatch__ctor_mB24396B2BAFBB8B8D1CD308623A7FEA521118ACB (void);
// 0x0000002C System.Void UnityEngine.Advertisements.UnityAdsLoadListenerMainDispatch::OnUnityAdsAdLoaded(System.String)
extern void UnityAdsLoadListenerMainDispatch_OnUnityAdsAdLoaded_mF2B5A49C9AA3F474864CFED5053F2A2B482962C9 (void);
// 0x0000002D System.Void UnityEngine.Advertisements.UnityAdsLoadListenerMainDispatch::OnUnityAdsFailedToLoad(System.String,UnityEngine.Advertisements.UnityAdsLoadError,System.String)
extern void UnityAdsLoadListenerMainDispatch_OnUnityAdsFailedToLoad_m99373259D9E6FFA7A00FA79966F71FA66A776823 (void);
// 0x0000002E System.Void UnityEngine.Advertisements.UnityAdsLoadListenerMainDispatch/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB906CFF2F656859346B52B729E63E6DE39E12B85 (void);
// 0x0000002F System.Void UnityEngine.Advertisements.UnityAdsLoadListenerMainDispatch/<>c__DisplayClass3_0::<OnUnityAdsAdLoaded>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3COnUnityAdsAdLoadedU3Eb__0_m5EE4BF9DAEC86927DA84958582315885D55FC6D0 (void);
// 0x00000030 System.Void UnityEngine.Advertisements.UnityAdsLoadListenerMainDispatch/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mE5FF6BC01880890F5D28D80BDCDCD5FCB4A03E2B (void);
// 0x00000031 System.Void UnityEngine.Advertisements.UnityAdsLoadListenerMainDispatch/<>c__DisplayClass4_0::<OnUnityAdsFailedToLoad>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3COnUnityAdsFailedToLoadU3Eb__0_m4CA70EC534C1864180AB34F897EAE2F7072E8646 (void);
// 0x00000032 System.Void UnityEngine.Advertisements.UnityAdsShowListenerMainDispatch::.ctor(UnityEngine.Advertisements.IUnityAdsShowListener,UnityEngine.Advertisements.Utilities.IUnityLifecycleManager)
extern void UnityAdsShowListenerMainDispatch__ctor_mC91CA102BE0AFAF3B37F48FF57FA4506CE076E62 (void);
// 0x00000033 System.Void UnityEngine.Advertisements.UnityAdsShowListenerMainDispatch::OnUnityAdsShowFailure(System.String,UnityEngine.Advertisements.UnityAdsShowError,System.String)
extern void UnityAdsShowListenerMainDispatch_OnUnityAdsShowFailure_mA172EEDE4E56C03CDD44C9B8B89EE310E65C6787 (void);
// 0x00000034 System.Void UnityEngine.Advertisements.UnityAdsShowListenerMainDispatch::OnUnityAdsShowStart(System.String)
extern void UnityAdsShowListenerMainDispatch_OnUnityAdsShowStart_m50D0D64B5AAC5633641700C4B87234C271DEB6AE (void);
// 0x00000035 System.Void UnityEngine.Advertisements.UnityAdsShowListenerMainDispatch::OnUnityAdsShowClick(System.String)
extern void UnityAdsShowListenerMainDispatch_OnUnityAdsShowClick_m8AB08D3F969664C46E7DBDADDD2E11253C0E5585 (void);
// 0x00000036 System.Void UnityEngine.Advertisements.UnityAdsShowListenerMainDispatch::OnUnityAdsShowComplete(System.String,UnityEngine.Advertisements.UnityAdsShowCompletionState)
extern void UnityAdsShowListenerMainDispatch_OnUnityAdsShowComplete_m4AC0B79B50A55D077DF28C759DE051DE591D10CB (void);
// 0x00000037 System.Void UnityEngine.Advertisements.UnityAdsShowListenerMainDispatch/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m3A7281913C593B0472B7BE84390ADC5522C63117 (void);
// 0x00000038 System.Void UnityEngine.Advertisements.UnityAdsShowListenerMainDispatch/<>c__DisplayClass3_0::<OnUnityAdsShowFailure>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3COnUnityAdsShowFailureU3Eb__0_m962272B8C13209EB182591F825BBFA897E7C45D9 (void);
// 0x00000039 System.Void UnityEngine.Advertisements.UnityAdsShowListenerMainDispatch/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mEF502DD172075C7FECF90811DE82E7C4D98EF4BE (void);
// 0x0000003A System.Void UnityEngine.Advertisements.UnityAdsShowListenerMainDispatch/<>c__DisplayClass4_0::<OnUnityAdsShowStart>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3COnUnityAdsShowStartU3Eb__0_m4628C13D4C4EDB481680D18FFE87F81B2E293528 (void);
// 0x0000003B System.Void UnityEngine.Advertisements.UnityAdsShowListenerMainDispatch/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m4628F07EAEB71AD9CB29B08AA7AE4A569ECA2D51 (void);
// 0x0000003C System.Void UnityEngine.Advertisements.UnityAdsShowListenerMainDispatch/<>c__DisplayClass5_0::<OnUnityAdsShowClick>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3COnUnityAdsShowClickU3Eb__0_m20AA6CCE43A0C8735DABE6C74EABD69E5E6C15E5 (void);
// 0x0000003D System.Void UnityEngine.Advertisements.UnityAdsShowListenerMainDispatch/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m05C92017BD1C1B0579C2D74A1169320776321F37 (void);
// 0x0000003E System.Void UnityEngine.Advertisements.UnityAdsShowListenerMainDispatch/<>c__DisplayClass6_0::<OnUnityAdsShowComplete>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3COnUnityAdsShowCompleteU3Eb__0_m4702F0FAB42972493A239E6987533D29F6F0C0B1 (void);
// 0x0000003F UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.IBanner::get_UnityLifecycleManager()
// 0x00000040 System.Boolean UnityEngine.Advertisements.IBanner::get_ShowAfterLoad()
// 0x00000041 System.Void UnityEngine.Advertisements.IBanner::set_ShowAfterLoad(System.Boolean)
// 0x00000042 System.Void UnityEngine.Advertisements.IBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x00000043 System.Void UnityEngine.Advertisements.IBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000044 System.Void UnityEngine.Advertisements.IBanner::Hide(System.Boolean)
// 0x00000045 System.Void UnityEngine.Advertisements.IBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
// 0x00000046 System.Void UnityEngine.Advertisements.INativeBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
// 0x00000047 System.Void UnityEngine.Advertisements.INativeBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x00000048 System.Void UnityEngine.Advertisements.INativeBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000049 System.Void UnityEngine.Advertisements.INativeBanner::Hide(System.Boolean)
// 0x0000004A System.Void UnityEngine.Advertisements.INativeBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
// 0x0000004B System.Void UnityEngine.Advertisements.IUnityAdsInitializationListener::OnInitializationComplete()
// 0x0000004C System.Void UnityEngine.Advertisements.IUnityAdsInitializationListener::OnInitializationFailed(UnityEngine.Advertisements.UnityAdsInitializationError,System.String)
// 0x0000004D System.Void UnityEngine.Advertisements.IUnityAdsLoadListener::OnUnityAdsAdLoaded(System.String)
// 0x0000004E System.Void UnityEngine.Advertisements.IUnityAdsLoadListener::OnUnityAdsFailedToLoad(System.String,UnityEngine.Advertisements.UnityAdsLoadError,System.String)
// 0x0000004F System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowFailure(System.String,UnityEngine.Advertisements.UnityAdsShowError,System.String)
// 0x00000050 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowStart(System.String)
// 0x00000051 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowClick(System.String)
// 0x00000052 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowComplete(System.String,UnityEngine.Advertisements.UnityAdsShowCompletionState)
// 0x00000053 System.String UnityEngine.Advertisements.MetaData::get_category()
extern void MetaData_get_category_mE48DB3D38C741ECA2787261D7FDA8AF99D78680A (void);
// 0x00000054 System.Void UnityEngine.Advertisements.MetaData::set_category(System.String)
extern void MetaData_set_category_mC0C94CD8085C0D11051DC82BBC6C897428954DF6 (void);
// 0x00000055 System.Void UnityEngine.Advertisements.MetaData::.ctor(System.String)
extern void MetaData__ctor_mA3E0768F48492D2C006E7C19F8D182D71EA57034 (void);
// 0x00000056 System.Void UnityEngine.Advertisements.MetaData::Set(System.String,System.Object)
extern void MetaData_Set_m25CF8E1FC4E0775019FCD58C43ED471CA65BD539 (void);
// 0x00000057 System.Collections.Generic.IDictionary`2<System.String,System.Object> UnityEngine.Advertisements.MetaData::Values()
extern void MetaData_Values_mC60E8FEDBF17C5ACA56B847F3ACFD75208F9A5DD (void);
// 0x00000058 System.Void UnityEngine.Advertisements.AndroidInitializationListener::.ctor(UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void AndroidInitializationListener__ctor_mD8397CD48024E1B7377D72AA2D10D36AD4A3352F (void);
// 0x00000059 System.Void UnityEngine.Advertisements.AndroidInitializationListener::onInitializationComplete()
extern void AndroidInitializationListener_onInitializationComplete_mEA1FF2313E39C2428AB8927EC20BE16A54354224 (void);
// 0x0000005A System.Void UnityEngine.Advertisements.AndroidInitializationListener::onInitializationFailed(UnityEngine.AndroidJavaObject,System.String)
extern void AndroidInitializationListener_onInitializationFailed_mC67E18394023D0EC62A082EBA31F50092549FF1C (void);
// 0x0000005B System.Void UnityEngine.Advertisements.AndroidLoadListener::.ctor(UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void AndroidLoadListener__ctor_mE4A5C2D2C504D65DEE14F507DB6E9C830E39D4FF (void);
// 0x0000005C System.Void UnityEngine.Advertisements.AndroidLoadListener::onUnityAdsAdLoaded(System.String)
extern void AndroidLoadListener_onUnityAdsAdLoaded_m773EFB625F840DF3DF34A6D2E5501756C8499171 (void);
// 0x0000005D System.Void UnityEngine.Advertisements.AndroidLoadListener::onUnityAdsFailedToLoad(System.String,UnityEngine.AndroidJavaObject,System.String)
extern void AndroidLoadListener_onUnityAdsFailedToLoad_mEF9AC0598A47727AF499E0B9BFA186A268383668 (void);
// 0x0000005E System.Void UnityEngine.Advertisements.AndroidShowListener::.ctor(UnityEngine.Advertisements.Platform.IPlatform,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void AndroidShowListener__ctor_mDA962C9A0AE9098F61C0FB811C4B5F3475F69C3C (void);
// 0x0000005F System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowFailure(System.String,UnityEngine.AndroidJavaObject,System.String)
extern void AndroidShowListener_onUnityAdsShowFailure_mBB96E70EFC1A7C4462C52CFA3FB5DCC3C78EF8CD (void);
// 0x00000060 System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowStart(System.String)
extern void AndroidShowListener_onUnityAdsShowStart_mBFF0BF90241AE06FF430321832475231271F4B49 (void);
// 0x00000061 System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowClick(System.String)
extern void AndroidShowListener_onUnityAdsShowClick_m9F36659AF386DBAF68A19A3690241F20262573F6 (void);
// 0x00000062 System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowComplete(System.String,UnityEngine.AndroidJavaObject)
extern void AndroidShowListener_onUnityAdsShowComplete_mE8F9996F082B81F7CD29D3420A1EBE5890A690C5 (void);
// 0x00000063 System.Void UnityEngine.Advertisements.INativePlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
// 0x00000064 System.Void UnityEngine.Advertisements.INativePlatform::Initialize(System.String,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
// 0x00000065 System.Void UnityEngine.Advertisements.INativePlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
// 0x00000066 System.Void UnityEngine.Advertisements.INativePlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
// 0x00000067 System.Void UnityEngine.Advertisements.INativePlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
// 0x00000068 System.String UnityEngine.Advertisements.INativePlatform::GetVersion()
// 0x00000069 System.Boolean UnityEngine.Advertisements.INativePlatform::IsInitialized()
// 0x0000006A System.String UnityEngine.Advertisements.ShowOptions::get_gamerSid()
extern void ShowOptions_get_gamerSid_m99E60DFA556B5CE71165C06A54D7BA9039E44918 (void);
// 0x0000006B System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::add_OnApplicationQuitEventHandler(UnityEngine.Events.UnityAction)
extern void ApplicationQuit_add_OnApplicationQuitEventHandler_m39398EF751CC1915AE7EBBD6ED2FFF87570B998B (void);
// 0x0000006C System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::remove_OnApplicationQuitEventHandler(UnityEngine.Events.UnityAction)
extern void ApplicationQuit_remove_OnApplicationQuitEventHandler_m4CE5EDB7A1EFB57CB6FEE7AC659836ACAE5A430E (void);
// 0x0000006D System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::OnApplicationQuit()
extern void ApplicationQuit_OnApplicationQuit_m67876A4AA945EB02636BE04B62A095514FD6B6EA (void);
// 0x0000006E System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::.ctor()
extern void ApplicationQuit__ctor_m9871D183A5361C34A864EC6B2D64C057F9A4A0DD (void);
// 0x0000006F System.Void UnityEngine.Advertisements.Utilities.CoroutineExecutor::Update()
extern void CoroutineExecutor_Update_mFFBB1E15F39D09DB1C71B318E56FE477E49C9492 (void);
// 0x00000070 System.Void UnityEngine.Advertisements.Utilities.CoroutineExecutor::.ctor()
extern void CoroutineExecutor__ctor_m82424D7D43C52BB4FA3EE7284AFB4002BB7E74F6 (void);
// 0x00000071 T UnityEngine.Advertisements.Utilities.EnumUtilities::GetEnumFromAndroidJavaObject(UnityEngine.AndroidJavaObject,T)
// 0x00000072 System.Void UnityEngine.Advertisements.Utilities.IUnityLifecycleManager::Post(System.Action)
// 0x00000073 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::.ctor()
extern void UnityLifecycleManager__ctor_m7F3CBFB7673A8D646BF6913FBC851C7769037EBB (void);
// 0x00000074 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Initialize()
extern void UnityLifecycleManager_Initialize_m164E5816365E0F68B093C3EE54A47C314A3C3468 (void);
// 0x00000075 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Post(System.Action)
extern void UnityLifecycleManager_Post_m2FB7AC24D84CAB9C180BD9A753721A816FF75A89 (void);
// 0x00000076 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Dispose()
extern void UnityLifecycleManager_Dispose_m7BFA140FE6714EB97AC5EED19B6B8531073A8E81 (void);
// 0x00000077 UnityEngine.Advertisements.IBanner UnityEngine.Advertisements.Platform.IPlatform::get_Banner()
// 0x00000078 UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Platform.IPlatform::get_UnityLifecycleManager()
// 0x00000079 System.Void UnityEngine.Advertisements.Platform.IPlatform::Initialize(System.String,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
// 0x0000007A System.Void UnityEngine.Advertisements.Platform.IPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
// 0x0000007B System.Void UnityEngine.Advertisements.Platform.IPlatform::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
// 0x0000007C System.Void UnityEngine.Advertisements.Platform.IPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
// 0x0000007D System.Void UnityEngine.Advertisements.Platform.IPlatform::OnUnityAdsShowFailure(System.String,UnityEngine.Advertisements.UnityAdsShowError,System.String)
// 0x0000007E System.Void UnityEngine.Advertisements.Platform.IPlatform::OnUnityAdsShowStart(System.String)
// 0x0000007F System.Void UnityEngine.Advertisements.Platform.IPlatform::OnUnityAdsShowClick(System.String)
// 0x00000080 System.Void UnityEngine.Advertisements.Platform.IPlatform::OnUnityAdsShowComplete(System.String,UnityEngine.Advertisements.UnityAdsShowCompletionState)
// 0x00000081 UnityEngine.Advertisements.IBanner UnityEngine.Advertisements.Platform.Platform::get_Banner()
extern void Platform_get_Banner_m44590C390B95E49FADD385B2C832A5FDE64CCC03 (void);
// 0x00000082 UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Platform.Platform::get_UnityLifecycleManager()
extern void Platform_get_UnityLifecycleManager_mCDB6D41026E6FDC7A32DE457E2464BB32D40F5FA (void);
// 0x00000083 UnityEngine.Advertisements.INativePlatform UnityEngine.Advertisements.Platform.Platform::get_NativePlatform()
extern void Platform_get_NativePlatform_mFD8A6BC4C9E4B1A10B2144E968785BE6855A15CE (void);
// 0x00000084 System.Boolean UnityEngine.Advertisements.Platform.Platform::get_IsInitialized()
extern void Platform_get_IsInitialized_m32F3455A6A3158FE972AC503E41D07121773F216 (void);
// 0x00000085 System.Boolean UnityEngine.Advertisements.Platform.Platform::get_IsShowing()
extern void Platform_get_IsShowing_m6E94928BB3D7E2720B6CDF2C25801158586BCDD7 (void);
// 0x00000086 System.Void UnityEngine.Advertisements.Platform.Platform::set_IsShowing(System.Boolean)
extern void Platform_set_IsShowing_m6FBC75C2EC6907BAC638CCAA2E339998DC05100C (void);
// 0x00000087 System.String UnityEngine.Advertisements.Platform.Platform::get_Version()
extern void Platform_get_Version_mF1750D1013A32B8297AE7EC8DDB442CF979E310C (void);
// 0x00000088 System.Void UnityEngine.Advertisements.Platform.Platform::.ctor(UnityEngine.Advertisements.INativePlatform,UnityEngine.Advertisements.IBanner,UnityEngine.Advertisements.Utilities.IUnityLifecycleManager)
extern void Platform__ctor_mA912D44D83BDA67AA6F469E6138BCB9816F33EB0 (void);
// 0x00000089 System.Void UnityEngine.Advertisements.Platform.Platform::Initialize(System.String,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void Platform_Initialize_m4B16C65F2EB6A9E82233A728C04DA46361CE6024 (void);
// 0x0000008A System.Void UnityEngine.Advertisements.Platform.Platform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void Platform_Load_m92B551A85073A8F911E5C60DA0D37CC603EAE938 (void);
// 0x0000008B System.Void UnityEngine.Advertisements.Platform.Platform::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Platform_Show_m222476A3E1855E064835A59D39725D53C924A335 (void);
// 0x0000008C System.Void UnityEngine.Advertisements.Platform.Platform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Platform_SetMetaData_mFC5274052CA90924B6F13C607E3322535750F9D1 (void);
// 0x0000008D System.Void UnityEngine.Advertisements.Platform.Platform::OnUnityAdsShowFailure(System.String,UnityEngine.Advertisements.UnityAdsShowError,System.String)
extern void Platform_OnUnityAdsShowFailure_mDCCA09E6801CF0A7651FC9DDB16D9C242F1EDB04 (void);
// 0x0000008E System.Void UnityEngine.Advertisements.Platform.Platform::OnUnityAdsShowStart(System.String)
extern void Platform_OnUnityAdsShowStart_mF1EDD719E846D414DE7058AB3648B4EDC1E80598 (void);
// 0x0000008F System.Void UnityEngine.Advertisements.Platform.Platform::OnUnityAdsShowClick(System.String)
extern void Platform_OnUnityAdsShowClick_m9604A0686C3D0061371A9E12262B640CE2E0A95C (void);
// 0x00000090 System.Void UnityEngine.Advertisements.Platform.Platform::OnUnityAdsShowComplete(System.String,UnityEngine.Advertisements.UnityAdsShowCompletionState)
extern void Platform_OnUnityAdsShowComplete_m0FD304BDBCA98D10B1E8E65A00D260AE94854CCB (void);
// 0x00000091 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
extern void UnsupportedBanner_SetupBanner_mABEF7CC1B545E9B38BAAC23F746BDDD3E7CB1CB9 (void);
// 0x00000092 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void UnsupportedBanner_Load_mF977D0B894335D5B47A78BD0AF35503D7BA9083B (void);
// 0x00000093 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void UnsupportedBanner_Show_m5E4EBD678692FF29482D48FBF98A95F9CB116295 (void);
// 0x00000094 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Hide(System.Boolean)
extern void UnsupportedBanner_Hide_m84DC685029818C61BB2D3B74AF562BF96CB1A146 (void);
// 0x00000095 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void UnsupportedBanner_SetPosition_m30F1A4F80278E1007A93305ACD0298DE08FBDEAE (void);
// 0x00000096 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::.ctor()
extern void UnsupportedBanner__ctor_m438F1E085E534BD57AB519D449A6EC8F9D274246 (void);
// 0x00000097 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
extern void UnsupportedPlatform_SetupPlatform_mAE4F436A677F0018A54275C2D5D15D49A3E67AD3 (void);
// 0x00000098 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Initialize(System.String,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void UnsupportedPlatform_Initialize_m440B506B6A40EAF3E382609B962D3D157507A33F (void);
// 0x00000099 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void UnsupportedPlatform_Load_mEDE3F80D4B91A89692222769E61EC21AD39D9364 (void);
// 0x0000009A System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void UnsupportedPlatform_Show_mC3BCF27080E48DB7D0EDC50A806C1037B1396ED5 (void);
// 0x0000009B System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void UnsupportedPlatform_SetMetaData_mC58DD5B86F8973F79411046EF74DA64AB5EEA2DE (void);
// 0x0000009C System.String UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetVersion()
extern void UnsupportedPlatform_GetVersion_m09FED2D3C4FDD5E041AEB3C2A4D6709742ADE38C (void);
// 0x0000009D System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::IsInitialized()
extern void UnsupportedPlatform_IsInitialized_mCBCC729DAD4D4DFE3D208C3BB7007C4D3DF48036 (void);
// 0x0000009E System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::.ctor()
extern void UnsupportedPlatform__ctor_mD2AB31CC9591F51340AE7DFFDDFC53AF23D35ED6 (void);
// 0x0000009F System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::Awake()
extern void BannerPlaceholder_Awake_mC91F24FD02F2EBA65D54E8203896D524D8C39E08 (void);
// 0x000000A0 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::OnGUI()
extern void BannerPlaceholder_OnGUI_mCA8AD18DFEB7FB97C42600B64E273673F9C8F890 (void);
// 0x000000A1 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::ShowBanner(UnityEngine.Advertisements.BannerPosition,UnityEngine.Advertisements.BannerOptions)
extern void BannerPlaceholder_ShowBanner_m10FF4E0DBC0E2B1852589371C3B31737E3ACB05C (void);
// 0x000000A2 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::HideBanner()
extern void BannerPlaceholder_HideBanner_m39E19D11C89B1ECDC7161FAACC7D0A668BAAF5CC (void);
// 0x000000A3 UnityEngine.Texture2D UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::BackgroundTexture(System.Int32,System.Int32,UnityEngine.Color)
extern void BannerPlaceholder_BackgroundTexture_mA1C83D534C3005BCE9A50A66E48EE53654DFEE1E (void);
// 0x000000A4 UnityEngine.Rect UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::GetBannerRect(UnityEngine.Advertisements.BannerPosition)
extern void BannerPlaceholder_GetBannerRect_mE3DD03B1E8CF6EA072A70F995D7D01BF2942C94F (void);
// 0x000000A5 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::.ctor()
extern void BannerPlaceholder__ctor_m924CEA544C2BB6BB1FB1B5F720EC76081D508828 (void);
// 0x000000A6 System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidBanner::get_IsLoaded()
extern void AndroidBanner_get_IsLoaded_mA90E42EA6594C68EFDD0AFFDC94A74A80C902B09 (void);
// 0x000000A7 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::.ctor()
extern void AndroidBanner__ctor_m8E723E99310838DADEF2F9B71484400282829E59 (void);
// 0x000000A8 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
extern void AndroidBanner_SetupBanner_m84E942C2FDB8B3C4E8077CCFFC05335A8C0516FB (void);
// 0x000000A9 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void AndroidBanner_Load_m8423A122726BD342EAEC2C8E312C24F00E9613C4 (void);
// 0x000000AA System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void AndroidBanner_Show_m06CF344B822E045EFA05982F3A10774C3A3EE132 (void);
// 0x000000AB System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::Hide(System.Boolean)
extern void AndroidBanner_Hide_mFFC62E2BF6ED3E27135D830DD56F8213625AAECE (void);
// 0x000000AC System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void AndroidBanner_SetPosition_m1370055994F0B01C4EC807BACB09C66CA6636560 (void);
// 0x000000AD System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerShow(System.String)
extern void AndroidBanner_onUnityBannerShow_mCB47141A40C6E6EFC046BB4023F272DAA716C54B (void);
// 0x000000AE System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerHide(System.String)
extern void AndroidBanner_onUnityBannerHide_m79062BDF8A0C2EF344B74827BD2EB3232DF73663 (void);
// 0x000000AF System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerLoaded(System.String,UnityEngine.AndroidJavaObject)
extern void AndroidBanner_onUnityBannerLoaded_m77D16A54777CCF59CA929166B1A6D33B01533763 (void);
// 0x000000B0 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerUnloaded(System.String)
extern void AndroidBanner_onUnityBannerUnloaded_mCF7398E2DF04D901135C06930007BBE942A57C6C (void);
// 0x000000B1 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerClick(System.String)
extern void AndroidBanner_onUnityBannerClick_mBC9DA7FEAB2FAC2FC61BDFA1D930536130110FF0 (void);
// 0x000000B2 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerError(System.String)
extern void AndroidBanner_onUnityBannerError_m4379E165441326ACEB9CC6509E736DDACDBDEBF5 (void);
// 0x000000B3 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<Hide>b__13_0()
extern void AndroidBanner_U3CHideU3Eb__13_0_mA234986168275EA7EA56F38B9EBD4CE4F0533D22 (void);
// 0x000000B4 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<Hide>b__13_1()
extern void AndroidBanner_U3CHideU3Eb__13_1_m1B3C913F757872839F338243CC43EDF2CE3FE9AC (void);
// 0x000000B5 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<onUnityBannerLoaded>b__17_1()
extern void AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_1_m915883D9CAE36E2821426AC509039ACE1C84CAEC (void);
// 0x000000B6 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<onUnityBannerLoaded>b__17_0()
extern void AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_0_mD37D62D5FB74CA7BA2259162802D8DC0C5649988 (void);
// 0x000000B7 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<onUnityBannerClick>b__19_0()
extern void AndroidBanner_U3ConUnityBannerClickU3Eb__19_0_m3E31000AD8114381FF258F7B217EB1FBB3D35CD6 (void);
// 0x000000B8 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mB4749FC4347BDA5F4935149B71E84CA043C5B7EE (void);
// 0x000000B9 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass11_0::<Load>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CLoadU3Eb__0_m7EF8C1F9F5714B3B9D1B80347365F02DB275BAE2 (void);
// 0x000000BA System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m7BB527763F003875E638B7256DD3BD3788435943 (void);
// 0x000000BB System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass12_0::<Show>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__0_mE626FA2BC0E938A69C142BBFA43DB8861832567A (void);
// 0x000000BC System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass12_0::<Show>b__1()
extern void U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__1_m6C46CDF641CEE4172AEDF5695048ACC3F890364F (void);
// 0x000000BD System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_mF43AD6D592B49CA9F9FCF85EA3AEDAAAD7252731 (void);
// 0x000000BE System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass20_0::<onUnityBannerError>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3ConUnityBannerErrorU3Eb__0_m0DD98663AF155A5809120293E2B451CF67DF47DC (void);
// 0x000000BF System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
extern void AndroidPlatform_SetupPlatform_m39E6D013E149BED8ABB45D6B35D568B37E508466 (void);
// 0x000000C0 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::Initialize(System.String,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void AndroidPlatform_Initialize_m72FBCA1EDCF1542425028D983DE6ABE95FD85585 (void);
// 0x000000C1 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void AndroidPlatform_Load_m44B5EF9945174C5681BEAD595361D433EDADE3AF (void);
// 0x000000C2 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void AndroidPlatform_Show_m21C060B4C4A142EC3F017E4F546C8D623E467BF4 (void);
// 0x000000C3 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void AndroidPlatform_SetMetaData_m188743603BA4C26155853CC315E917148711C15C (void);
// 0x000000C4 System.String UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetVersion()
extern void AndroidPlatform_GetVersion_m3BA0BBDBF5FAEA9684782B8F6BF5F3A7B1B1BDD8 (void);
// 0x000000C5 System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidPlatform::IsInitialized()
extern void AndroidPlatform_IsInitialized_m442E73B0DC39E3F0D718CFBD58CFE8AA60973A6B (void);
// 0x000000C6 UnityEngine.AndroidJavaObject UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetCurrentAndroidActivity()
extern void AndroidPlatform_GetCurrentAndroidActivity_mCD9F343253ECB4F4D2515733504948D596EFC1D1 (void);
// 0x000000C7 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::.ctor()
extern void AndroidPlatform__ctor_mAD4B3465710A8E295F30F1872045D1DEF696A2EC (void);
// 0x000000C8 UnityEngine.AndroidJavaObject UnityEngine.Advertisements.Platform.Android.BannerBundle::get_bannerView()
extern void BannerBundle_get_bannerView_m981F7CB1C3C1CA95AF72D95108A42C797AEAF9D3 (void);
// 0x000000C9 System.String UnityEngine.Advertisements.Platform.Android.BannerBundle::get_bannerPlacementId()
extern void BannerBundle_get_bannerPlacementId_mA4916C42EA50108B4D553776443460B1C664C26E (void);
// 0x000000CA System.Void UnityEngine.Advertisements.Platform.Android.BannerBundle::.ctor(System.String,UnityEngine.AndroidJavaObject)
extern void BannerBundle__ctor_mA358F4E6ED2021BD4D86AE8C55BD6B13C4DD3322 (void);
static Il2CppMethodPointer s_methodPointers[202] = 
{
	Advertisement__cctor_mCC6272BAAF27A9D700E13D1DE3EF7CCA19A6BA48,
	Advertisement_Initialize_mA83C553537C87F2C2B12FCBAE4B9EB75EC9DA343,
	Advertisement_Load_m57620941EB371D6E9EFC283698511946CA9BA0A5,
	Advertisement_Show_m8F5AB716E9FB227B3FCD267DBCA7BCCE63C06C6B,
	Advertisement_Show_mD97A736E31BC882EAA93655679EFFFA9482F591F,
	Advertisement_SetMetaData_m9BF33875888AB1DAB282065CE1563F0C1F78EF71,
	Advertisement_CreatePlatform_mFFCC7E5D24C35AF45F2C3FE61A20D60102FF5293,
	Banner_Load_mC55582B1B0C8277EDCCBCCF14627435D8354DB0E,
	Banner_Show_m89B35F72104AF5747936DD1FC58E5C13704075D3,
	Banner_Hide_mE40A4CE81B2F66D874A0995CEB0BF595D9BE09DC,
	Banner_SetPosition_m69680FE1B23F22DC2212F1C37B0890C6833645E8,
	Banner_get_UnityLifecycleManager_m5BB7A4F09AB21EDDF9E77A499D91C03A68B7E82E,
	Banner_get_ShowAfterLoad_m0F5758A87EC97CB00326193263FA91BAF27DB9A4,
	Banner_set_ShowAfterLoad_m4748C54A24F9F34CDCFA582DC64D5D539DCD83CF,
	Banner__ctor_m55A8737759701A4D4E16CC7667CEA599A1C39D39,
	Banner_Load_mF9AF0805B1312D1430FF8C9B034C81068F453289,
	Banner_Show_m661CD517BDB0ED56DA8E07145C09877DE6C4E2BD,
	Banner_Hide_mA4CAE84741A6D00B02A270661712FF2AF11C79E3,
	Banner_SetPosition_m4FDEDF04AF2BE1D18B2943B3F790DE780E65510A,
	BannerLoadOptions_get_loadCallback_mECEEB150276C0E8C1744CB508B6AFF72A0796077,
	BannerLoadOptions_set_loadCallback_m219703CC2E16DAFA77481374046BD19298B27BF0,
	BannerLoadOptions_get_errorCallback_mB9B595EB059D86CA406DCC7F7D4B9EF59A8AB589,
	BannerLoadOptions_set_errorCallback_m73A7C9AEA0F79A4963538CE38D3AAE5AB69AF6E2,
	BannerLoadOptions__ctor_m866C2FA6DEA20A35F7FBE25536D96BD563D1CFE7,
	LoadCallback__ctor_m82C20B862B58A068187B9E090BA53E0F12C2E3F1,
	LoadCallback_Invoke_mFEB8CEEF9C2B011B55E8B61D80B7FC98AA000420,
	ErrorCallback__ctor_m6C3D9F85150EFC4700A624E3946C061BA25849A9,
	ErrorCallback_Invoke_m8D78A1ACBA0018E30AC198C4C179E74F1F5E0377,
	BannerOptions_get_showCallback_m0367F7A8058FA64DD2C114DC2423481F33FF531B,
	BannerOptions_set_showCallback_m14C693B20F26D364ECB7B9B90F3C455D0B361357,
	BannerOptions_get_hideCallback_mBED58861170D9007A1F6C4883ED906C151AD852A,
	BannerOptions_set_hideCallback_m9877994E49979C868E5331BA9708D52DE2F18424,
	BannerOptions_get_clickCallback_m7E35A638D6D69CE003CEB93C25DB33BA194F3AF0,
	BannerOptions__ctor_m5D0194C2660940617F25552DB827D41D10878A60,
	BannerCallback__ctor_m54F3DA4DFF8463122BC222D9B3F49A6A2EF26A46,
	BannerCallback_Invoke_m1826BD07878E285004632455BF5C2226B26C4236,
	UnityAdsInitializationListenerMainDispatch__ctor_mF2ABB02A80CBE4A3172B19B9EC0B5DE8884E1B37,
	UnityAdsInitializationListenerMainDispatch_OnInitializationComplete_m3F574F08BDCC7C37F77E7A23DBD4A87ABFEBD516,
	UnityAdsInitializationListenerMainDispatch_OnInitializationFailed_m85CA948CB74E5EA43C6DD91BD292F2926D8455F6,
	UnityAdsInitializationListenerMainDispatch_U3COnInitializationCompleteU3Eb__3_0_mFF9220DE776EC5910A9D7AFB00A7CFD322DB6E51,
	U3CU3Ec__DisplayClass4_0__ctor_m5E055A48B47F27C369B62EDD5AA6C4CB38B5C5D4,
	U3CU3Ec__DisplayClass4_0_U3COnInitializationFailedU3Eb__0_m98AB7A0529A5CCE24EEAE6BF06BF2BF2FC7A7659,
	UnityAdsLoadListenerMainDispatch__ctor_mB24396B2BAFBB8B8D1CD308623A7FEA521118ACB,
	UnityAdsLoadListenerMainDispatch_OnUnityAdsAdLoaded_mF2B5A49C9AA3F474864CFED5053F2A2B482962C9,
	UnityAdsLoadListenerMainDispatch_OnUnityAdsFailedToLoad_m99373259D9E6FFA7A00FA79966F71FA66A776823,
	U3CU3Ec__DisplayClass3_0__ctor_mB906CFF2F656859346B52B729E63E6DE39E12B85,
	U3CU3Ec__DisplayClass3_0_U3COnUnityAdsAdLoadedU3Eb__0_m5EE4BF9DAEC86927DA84958582315885D55FC6D0,
	U3CU3Ec__DisplayClass4_0__ctor_mE5FF6BC01880890F5D28D80BDCDCD5FCB4A03E2B,
	U3CU3Ec__DisplayClass4_0_U3COnUnityAdsFailedToLoadU3Eb__0_m4CA70EC534C1864180AB34F897EAE2F7072E8646,
	UnityAdsShowListenerMainDispatch__ctor_mC91CA102BE0AFAF3B37F48FF57FA4506CE076E62,
	UnityAdsShowListenerMainDispatch_OnUnityAdsShowFailure_mA172EEDE4E56C03CDD44C9B8B89EE310E65C6787,
	UnityAdsShowListenerMainDispatch_OnUnityAdsShowStart_m50D0D64B5AAC5633641700C4B87234C271DEB6AE,
	UnityAdsShowListenerMainDispatch_OnUnityAdsShowClick_m8AB08D3F969664C46E7DBDADDD2E11253C0E5585,
	UnityAdsShowListenerMainDispatch_OnUnityAdsShowComplete_m4AC0B79B50A55D077DF28C759DE051DE591D10CB,
	U3CU3Ec__DisplayClass3_0__ctor_m3A7281913C593B0472B7BE84390ADC5522C63117,
	U3CU3Ec__DisplayClass3_0_U3COnUnityAdsShowFailureU3Eb__0_m962272B8C13209EB182591F825BBFA897E7C45D9,
	U3CU3Ec__DisplayClass4_0__ctor_mEF502DD172075C7FECF90811DE82E7C4D98EF4BE,
	U3CU3Ec__DisplayClass4_0_U3COnUnityAdsShowStartU3Eb__0_m4628C13D4C4EDB481680D18FFE87F81B2E293528,
	U3CU3Ec__DisplayClass5_0__ctor_m4628F07EAEB71AD9CB29B08AA7AE4A569ECA2D51,
	U3CU3Ec__DisplayClass5_0_U3COnUnityAdsShowClickU3Eb__0_m20AA6CCE43A0C8735DABE6C74EABD69E5E6C15E5,
	U3CU3Ec__DisplayClass6_0__ctor_m05C92017BD1C1B0579C2D74A1169320776321F37,
	U3CU3Ec__DisplayClass6_0_U3COnUnityAdsShowCompleteU3Eb__0_m4702F0FAB42972493A239E6987533D29F6F0C0B1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MetaData_get_category_mE48DB3D38C741ECA2787261D7FDA8AF99D78680A,
	MetaData_set_category_mC0C94CD8085C0D11051DC82BBC6C897428954DF6,
	MetaData__ctor_mA3E0768F48492D2C006E7C19F8D182D71EA57034,
	MetaData_Set_m25CF8E1FC4E0775019FCD58C43ED471CA65BD539,
	MetaData_Values_mC60E8FEDBF17C5ACA56B847F3ACFD75208F9A5DD,
	AndroidInitializationListener__ctor_mD8397CD48024E1B7377D72AA2D10D36AD4A3352F,
	AndroidInitializationListener_onInitializationComplete_mEA1FF2313E39C2428AB8927EC20BE16A54354224,
	AndroidInitializationListener_onInitializationFailed_mC67E18394023D0EC62A082EBA31F50092549FF1C,
	AndroidLoadListener__ctor_mE4A5C2D2C504D65DEE14F507DB6E9C830E39D4FF,
	AndroidLoadListener_onUnityAdsAdLoaded_m773EFB625F840DF3DF34A6D2E5501756C8499171,
	AndroidLoadListener_onUnityAdsFailedToLoad_mEF9AC0598A47727AF499E0B9BFA186A268383668,
	AndroidShowListener__ctor_mDA962C9A0AE9098F61C0FB811C4B5F3475F69C3C,
	AndroidShowListener_onUnityAdsShowFailure_mBB96E70EFC1A7C4462C52CFA3FB5DCC3C78EF8CD,
	AndroidShowListener_onUnityAdsShowStart_mBFF0BF90241AE06FF430321832475231271F4B49,
	AndroidShowListener_onUnityAdsShowClick_m9F36659AF386DBAF68A19A3690241F20262573F6,
	AndroidShowListener_onUnityAdsShowComplete_mE8F9996F082B81F7CD29D3420A1EBE5890A690C5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ShowOptions_get_gamerSid_m99E60DFA556B5CE71165C06A54D7BA9039E44918,
	ApplicationQuit_add_OnApplicationQuitEventHandler_m39398EF751CC1915AE7EBBD6ED2FFF87570B998B,
	ApplicationQuit_remove_OnApplicationQuitEventHandler_m4CE5EDB7A1EFB57CB6FEE7AC659836ACAE5A430E,
	ApplicationQuit_OnApplicationQuit_m67876A4AA945EB02636BE04B62A095514FD6B6EA,
	ApplicationQuit__ctor_m9871D183A5361C34A864EC6B2D64C057F9A4A0DD,
	CoroutineExecutor_Update_mFFBB1E15F39D09DB1C71B318E56FE477E49C9492,
	CoroutineExecutor__ctor_m82424D7D43C52BB4FA3EE7284AFB4002BB7E74F6,
	NULL,
	NULL,
	UnityLifecycleManager__ctor_m7F3CBFB7673A8D646BF6913FBC851C7769037EBB,
	UnityLifecycleManager_Initialize_m164E5816365E0F68B093C3EE54A47C314A3C3468,
	UnityLifecycleManager_Post_m2FB7AC24D84CAB9C180BD9A753721A816FF75A89,
	UnityLifecycleManager_Dispose_m7BFA140FE6714EB97AC5EED19B6B8531073A8E81,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Platform_get_Banner_m44590C390B95E49FADD385B2C832A5FDE64CCC03,
	Platform_get_UnityLifecycleManager_mCDB6D41026E6FDC7A32DE457E2464BB32D40F5FA,
	Platform_get_NativePlatform_mFD8A6BC4C9E4B1A10B2144E968785BE6855A15CE,
	Platform_get_IsInitialized_m32F3455A6A3158FE972AC503E41D07121773F216,
	Platform_get_IsShowing_m6E94928BB3D7E2720B6CDF2C25801158586BCDD7,
	Platform_set_IsShowing_m6FBC75C2EC6907BAC638CCAA2E339998DC05100C,
	Platform_get_Version_mF1750D1013A32B8297AE7EC8DDB442CF979E310C,
	Platform__ctor_mA912D44D83BDA67AA6F469E6138BCB9816F33EB0,
	Platform_Initialize_m4B16C65F2EB6A9E82233A728C04DA46361CE6024,
	Platform_Load_m92B551A85073A8F911E5C60DA0D37CC603EAE938,
	Platform_Show_m222476A3E1855E064835A59D39725D53C924A335,
	Platform_SetMetaData_mFC5274052CA90924B6F13C607E3322535750F9D1,
	Platform_OnUnityAdsShowFailure_mDCCA09E6801CF0A7651FC9DDB16D9C242F1EDB04,
	Platform_OnUnityAdsShowStart_mF1EDD719E846D414DE7058AB3648B4EDC1E80598,
	Platform_OnUnityAdsShowClick_m9604A0686C3D0061371A9E12262B640CE2E0A95C,
	Platform_OnUnityAdsShowComplete_m0FD304BDBCA98D10B1E8E65A00D260AE94854CCB,
	UnsupportedBanner_SetupBanner_mABEF7CC1B545E9B38BAAC23F746BDDD3E7CB1CB9,
	UnsupportedBanner_Load_mF977D0B894335D5B47A78BD0AF35503D7BA9083B,
	UnsupportedBanner_Show_m5E4EBD678692FF29482D48FBF98A95F9CB116295,
	UnsupportedBanner_Hide_m84DC685029818C61BB2D3B74AF562BF96CB1A146,
	UnsupportedBanner_SetPosition_m30F1A4F80278E1007A93305ACD0298DE08FBDEAE,
	UnsupportedBanner__ctor_m438F1E085E534BD57AB519D449A6EC8F9D274246,
	UnsupportedPlatform_SetupPlatform_mAE4F436A677F0018A54275C2D5D15D49A3E67AD3,
	UnsupportedPlatform_Initialize_m440B506B6A40EAF3E382609B962D3D157507A33F,
	UnsupportedPlatform_Load_mEDE3F80D4B91A89692222769E61EC21AD39D9364,
	UnsupportedPlatform_Show_mC3BCF27080E48DB7D0EDC50A806C1037B1396ED5,
	UnsupportedPlatform_SetMetaData_mC58DD5B86F8973F79411046EF74DA64AB5EEA2DE,
	UnsupportedPlatform_GetVersion_m09FED2D3C4FDD5E041AEB3C2A4D6709742ADE38C,
	UnsupportedPlatform_IsInitialized_mCBCC729DAD4D4DFE3D208C3BB7007C4D3DF48036,
	UnsupportedPlatform__ctor_mD2AB31CC9591F51340AE7DFFDDFC53AF23D35ED6,
	BannerPlaceholder_Awake_mC91F24FD02F2EBA65D54E8203896D524D8C39E08,
	BannerPlaceholder_OnGUI_mCA8AD18DFEB7FB97C42600B64E273673F9C8F890,
	BannerPlaceholder_ShowBanner_m10FF4E0DBC0E2B1852589371C3B31737E3ACB05C,
	BannerPlaceholder_HideBanner_m39E19D11C89B1ECDC7161FAACC7D0A668BAAF5CC,
	BannerPlaceholder_BackgroundTexture_mA1C83D534C3005BCE9A50A66E48EE53654DFEE1E,
	BannerPlaceholder_GetBannerRect_mE3DD03B1E8CF6EA072A70F995D7D01BF2942C94F,
	BannerPlaceholder__ctor_m924CEA544C2BB6BB1FB1B5F720EC76081D508828,
	AndroidBanner_get_IsLoaded_mA90E42EA6594C68EFDD0AFFDC94A74A80C902B09,
	AndroidBanner__ctor_m8E723E99310838DADEF2F9B71484400282829E59,
	AndroidBanner_SetupBanner_m84E942C2FDB8B3C4E8077CCFFC05335A8C0516FB,
	AndroidBanner_Load_m8423A122726BD342EAEC2C8E312C24F00E9613C4,
	AndroidBanner_Show_m06CF344B822E045EFA05982F3A10774C3A3EE132,
	AndroidBanner_Hide_mFFC62E2BF6ED3E27135D830DD56F8213625AAECE,
	AndroidBanner_SetPosition_m1370055994F0B01C4EC807BACB09C66CA6636560,
	AndroidBanner_onUnityBannerShow_mCB47141A40C6E6EFC046BB4023F272DAA716C54B,
	AndroidBanner_onUnityBannerHide_m79062BDF8A0C2EF344B74827BD2EB3232DF73663,
	AndroidBanner_onUnityBannerLoaded_m77D16A54777CCF59CA929166B1A6D33B01533763,
	AndroidBanner_onUnityBannerUnloaded_mCF7398E2DF04D901135C06930007BBE942A57C6C,
	AndroidBanner_onUnityBannerClick_mBC9DA7FEAB2FAC2FC61BDFA1D930536130110FF0,
	AndroidBanner_onUnityBannerError_m4379E165441326ACEB9CC6509E736DDACDBDEBF5,
	AndroidBanner_U3CHideU3Eb__13_0_mA234986168275EA7EA56F38B9EBD4CE4F0533D22,
	AndroidBanner_U3CHideU3Eb__13_1_m1B3C913F757872839F338243CC43EDF2CE3FE9AC,
	AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_1_m915883D9CAE36E2821426AC509039ACE1C84CAEC,
	AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_0_mD37D62D5FB74CA7BA2259162802D8DC0C5649988,
	AndroidBanner_U3ConUnityBannerClickU3Eb__19_0_m3E31000AD8114381FF258F7B217EB1FBB3D35CD6,
	U3CU3Ec__DisplayClass11_0__ctor_mB4749FC4347BDA5F4935149B71E84CA043C5B7EE,
	U3CU3Ec__DisplayClass11_0_U3CLoadU3Eb__0_m7EF8C1F9F5714B3B9D1B80347365F02DB275BAE2,
	U3CU3Ec__DisplayClass12_0__ctor_m7BB527763F003875E638B7256DD3BD3788435943,
	U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__0_mE626FA2BC0E938A69C142BBFA43DB8861832567A,
	U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__1_m6C46CDF641CEE4172AEDF5695048ACC3F890364F,
	U3CU3Ec__DisplayClass20_0__ctor_mF43AD6D592B49CA9F9FCF85EA3AEDAAAD7252731,
	U3CU3Ec__DisplayClass20_0_U3ConUnityBannerErrorU3Eb__0_m0DD98663AF155A5809120293E2B451CF67DF47DC,
	AndroidPlatform_SetupPlatform_m39E6D013E149BED8ABB45D6B35D568B37E508466,
	AndroidPlatform_Initialize_m72FBCA1EDCF1542425028D983DE6ABE95FD85585,
	AndroidPlatform_Load_m44B5EF9945174C5681BEAD595361D433EDADE3AF,
	AndroidPlatform_Show_m21C060B4C4A142EC3F017E4F546C8D623E467BF4,
	AndroidPlatform_SetMetaData_m188743603BA4C26155853CC315E917148711C15C,
	AndroidPlatform_GetVersion_m3BA0BBDBF5FAEA9684782B8F6BF5F3A7B1B1BDD8,
	AndroidPlatform_IsInitialized_m442E73B0DC39E3F0D718CFBD58CFE8AA60973A6B,
	AndroidPlatform_GetCurrentAndroidActivity_mCD9F343253ECB4F4D2515733504948D596EFC1D1,
	AndroidPlatform__ctor_mAD4B3465710A8E295F30F1872045D1DEF696A2EC,
	BannerBundle_get_bannerView_m981F7CB1C3C1CA95AF72D95108A42C797AEAF9D3,
	BannerBundle_get_bannerPlacementId_mA4916C42EA50108B4D553776443460B1C664C26E,
	BannerBundle__ctor_mA358F4E6ED2021BD4D86AE8C55BD6B13C4DD3322,
};
static const int32_t s_InvokerIndices[202] = 
{
	7334,
	6106,
	6645,
	6645,
	6121,
	7212,
	7307,
	6645,
	6645,
	7204,
	7208,
	4691,
	4616,
	3837,
	2219,
	2219,
	2219,
	3837,
	3889,
	4691,
	3912,
	4691,
	3912,
	4803,
	2216,
	4803,
	2216,
	3912,
	4691,
	3912,
	4691,
	3912,
	4691,
	4803,
	2216,
	4803,
	2219,
	4803,
	2041,
	4803,
	4803,
	4803,
	2219,
	3912,
	1238,
	4803,
	4803,
	4803,
	4803,
	2219,
	1238,
	3912,
	3912,
	2214,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4691,
	3912,
	3912,
	2219,
	4691,
	3912,
	4803,
	2219,
	3912,
	3912,
	1256,
	2219,
	1256,
	3912,
	3912,
	2219,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4691,
	3912,
	3912,
	4803,
	4803,
	4803,
	4803,
	0,
	0,
	4803,
	4803,
	3912,
	4803,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4691,
	4691,
	4691,
	4616,
	4616,
	3837,
	4691,
	1256,
	1227,
	2219,
	1256,
	3912,
	1238,
	3912,
	3912,
	2214,
	3912,
	2219,
	2219,
	3837,
	3889,
	4803,
	3912,
	1227,
	2219,
	2219,
	3912,
	4691,
	4616,
	4803,
	4803,
	4803,
	2041,
	4803,
	5916,
	7009,
	4803,
	4616,
	4803,
	3912,
	2219,
	2219,
	3837,
	3889,
	3912,
	3912,
	2219,
	3912,
	3912,
	3912,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	4803,
	3912,
	1227,
	2219,
	2219,
	3912,
	4691,
	4616,
	7307,
	4803,
	4691,
	4691,
	2219,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000071, { 0, 2 } },
};
extern const uint32_t g_rgctx_T_tC5B95C0925127FF9C53496314C50BF7DE8AD2DEB;
extern const uint32_t g_rgctx_T_tC5B95C0925127FF9C53496314C50BF7DE8AD2DEB;
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tC5B95C0925127FF9C53496314C50BF7DE8AD2DEB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tC5B95C0925127FF9C53496314C50BF7DE8AD2DEB },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_Advertisements_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_Advertisements_CodeGenModule = 
{
	"UnityEngine.Advertisements.dll",
	202,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
